#AlemHealth
AlemHealth provide medical imaging services

Prerequisites:

[TOC]


## MySQL installation
    Download and install from http://dev.mysql.com/downloads/mysql/
   

##Node Installation:( Using NVM )
   1. curl https://raw.githubusercontent.com/creationix/nvm/v0.10.0/install.sh | sh
   2. in command line run
        `echo ". ~/.nvm/nvm.sh" >> ~/.bash_profile`
      and restart your terminal

   3. Install node using `nvm install 0.10`
   4. Enable the node installation using `nvm use 0.10`
   5. Make the node version default using `nvm alias default 0.10`

##Django Installation
    1. Install VirtualEnv
    2. Create a new virtualenv
    3. Activate the virtualenv
    4. Clone alemhealth repository for github:
       Go to alemhealth directory
    5. Run pip command to install all dependencies.
       pip install -r alemhealth/requirements.txt

    6. Create a database called 'alemhealth' sql command 'create database alemhealth CHARACTER SET utf8 COLLATE utf8_general_ci;'
    7. Run syncdb to create tables
        python manage.py syncdb

##RabbitMQ Install
   1. in Mac https://www.rabbitmq.com/install-homebrew.html
run 'rabbitmq-server'

##DCMTK Install
   1. apt-get install dcmtk

##ELK for Logging
    1. Need to create a 'alemhealth' directory in /var/log/ 
    2. logstash server for track logging 
    3. Need to isntall "logstash-forwarder" in the app machine , which forward the logs to log server 


##Project Setup
    1. clone the project and run `npm install`
    2. Install grunt-cli if it is not there using `npm install -g grunt-cli`
    3. See list of available tasks typing `grunt --help` in project dir
    4. run `grunt dev`
    5. run 'grunt watch' to continuously update.
    

### Please check the [wiki page](https://bitbucket.org/inovio/alemhealth/wiki/Home) for details    
    