#!/usr/bin/env bash

echo "Removing media file .... \c"
rm -rf /alemhealth/media/*
echo "Done"

echo "Clean database table ..."
mysql -u root -p'!qweqwe@#' -Bse "use alemhealth; SET FOREIGN_KEY_CHECKS=0; TRUNCATE dicom_images; TRUNCATE order_metas; TRUNCATE orders; TRUNCATE files; TRUNCATE hospitals; TRUNCATE hospitals_user; UPDATE providers SET logo_id = null; UPDATE hospitals SET logo_id = null; SET FOREIGN_KEY_CHECKS=1;"
echo "Done"