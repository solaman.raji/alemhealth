#!/bin/bash

echo "Removing all alemhealth media file .... \c"

rm -rf /alemhealth/media/*

echo "Done"


echo "Dropping  mysql .... \c"
mysql -u root -p'!qweqwe@#' -Bse "drop database alemhealth; create database alemhealth;"
echo "Done"

echo "Create migration file .... \c"
/alemhealth/manage.py makemigrations
echo "Done"

echo "Create database table ...."
/alemhealth/manage.py migrate
echo "Done"

echo "Create admin user .... \c"
/alemhealth/manage.py runscript initial_data
echo "Done"

echo "Create sample provider .... \c"
/alemhealth/manage.py runscript create_provider
echo "Done"

echo "Create sample facility .... \c"
/alemhealth/manage.py runscript create_facility
echo "Done"

echo "All setup completed. Happy Coding"