from django.contrib.auth.models import User, Group


# Initial Data
def initial_data():
    # Create superuser - admin
    user = User.objects.create_superuser('admin', 'admin@example.com', 'qweqwe')

    # Add admin group
    group = Group(name="admin")
    group.save()

    # Add admin user to admin group
    user.groups.add(group)

    # Add doctor group
    group = Group(name="doctor")
    group.save()

    # Add hospital group
    group = Group(name="hospital")
    group.save()

    # Add operator group
    group = Group(name="operator")
    group.save()

    # Add provider group
    group = Group(name="provider")
    group.save()


def run():
    initial_data()
