import json
from django.test import RequestFactory
from report.views import ReporterView


# Create Report
def create_report(order_guid):
    url = "api/report/" + order_guid
    data = {
        "order_guid": order_guid,
        "report_data": {
            "procedure": "<p>Procedure 1</p>",
            "clinical_information": "<p>Clinical Information 1</p>",
            "comparison": "<p>Comparison 1</p>",
            "findings": "<p>Findings 1</p>",
            "impression": "<p>Impression 1</p>",
        }
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    ReporterView.as_view()(request)


def run():
    # Order GUID
    order_guid = "fa52a4de-c067-42b7-be31-7e4122f64e0e"
    create_report(order_guid)
