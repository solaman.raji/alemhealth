import json
from django.test import RequestFactory
from report.views import ReporterView


# Submit Report
def submit_report(order_guid):
    # Default Data
    url = "api/report/" + order_guid
    data = {
        "order_guid": order_guid,
        "report": "<div style='text-align: center;'><u><b>Procedure 1</b></u></div><div style='text-align: left;'><b>Clinical Information:</b></br>Clinical Information 1</br></div><div style='text-align: left;'><b>Comparison:</b></br>Comparison 1</br></div><div style='text-align: left;'><b>Findings:</b></br>Findings 1</br></div><div style='text-align: left;'><b>Impression:</b></br>Impression 1</br></div>"
    }

    # Submit Report Using Request Factory
    request_factory_object = RequestFactory()
    request = request_factory_object.put(url, content_type="application/json")
    request._body = json.dumps(data)
    ReporterView.as_view()(request)


def run():
    # Order GUID
    _order_guid = "d66b45ee38be41478aa8544e16f8de6a"
    submit_report(_order_guid)
