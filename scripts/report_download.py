from django.test import RequestFactory
from report.views import ReportDownloadView
import logging

logger = logging.getLogger("alemhealth")


# Report Download With Last Download Time
def report_download_with_last_download_time():
    # Default Data
    url = "api/reports"
    data = {
        "provider_guid": "9cd7ea43-fe64-4a42-b815-b804804ca6c3",
        "last_download_time": "2017-08-28 07:49:04"
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.get(url, data, content_type="application/json")
    response = ReportDownloadView.as_view()(request)
    assert response.status_code == 200


# Report Download Without Last Download Time
def report_download_without_last_download_time():
    # Default Data
    url = "api/reports"
    data = {
        "provider_guid": "9cd7ea43-fe64-4a42-b815-b804804ca6c3",
        "last_download_time": ""
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.get(url, data, content_type="application/json")
    response = ReportDownloadView.as_view()(request)
    assert response.status_code == 200


# Report Download Without Provider GUID
def report_download_without_provider_guid():
    # Default Data
    url = "api/reports"
    data = {
        "provider_guid": "",
        "last_download_time": ""
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.get(url, data, content_type="application/json")
    response = ReportDownloadView.as_view()(request)
    assert response.status_code == 200


# Report Download Without Provider GUID parameter
def report_download_without_provider_guid_parameter():
    # Default Data
    url = "api/reports"
    data = {
        "last_download_time": ""
    }

    request_factory_object = RequestFactory()
    request = request_factory_object.get(url, data, content_type="application/json")
    response = ReportDownloadView.as_view()(request)
    logger.info("report download script >>> response: " + str(response))
    logger.info("report download script >>> response as dict: " + str(response.__dict__))
    assert response.status_code == 400


def run():
    # report_download_with_last_download_time()
    # report_download_without_last_download_time()
    # report_download_without_provider_guid()
    report_download_without_provider_guid_parameter()
