import json
from django.test import RequestFactory
from report.views import ReportTemplateView

from logging import getLogger

logger = getLogger("alemhealth")

# Create Report Template
def create_report_template():
    # Default Data
    url = "api/reporttemplate/"
    data = {
        "provider_guid": "a46d32bfa7794af18621435a05487793",
        "template_name": "Report 1",
        "procedure": "Precedure 1",
        "clinical_information": "<p>Clinical Information 1</p>",
        "comparison": "<p>Comparison 1</p>",
        "findings": "<p>Findings 1</p>",
        "impression": "<p>Impression 1</p>"
    }

    # Create Report Template Using Request Factory
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request._body = json.dumps(data)
    response = ReportTemplateView.as_view()(request)
    assert response.data["status"] == 200


def run():
    create_report_template()
