import os
import json
from django.test import RequestFactory
from alemhealth import settings
from django.contrib.auth.models import User
from alemhealth.api import HospitalsView, HospitalsLogoView
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File


# Upload Logo
def upload_logo(url, file_path):
    filename = os.path.basename(file_path)
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url)
    file_obj = File(open(file_path, 'rb'))
    uploaded_file = SimpleUploadedFile(filename, file_obj.read(), content_type='multipart/form-data')
    request.FILES['file'] = uploaded_file
    return HospitalsLogoView.as_view()(request)


# Create Facility
def create_facility(url, data):
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request.user = User.objects.get(username='admin')
    request._body = json.dumps(data)
    HospitalsView.as_view()(request)


def run():
    # Logo Upload URL
    url = "/api/providers/logo/?secret_key=" + settings.CLIENT_SECRET_KEY

    # Logo File Path
    filename = "dummy_alemhealth_logo.png"
    file_path = os.path.join(settings.BASE_DIR, "static", "dashboard", "images", filename)

    response = upload_logo(url, file_path)
    response = json.loads(response.content)

    # Facility Default Data
    url = "/api/hospitals?secret_key=" + settings.CLIENT_SECRET_KEY
    data = [{
        "user": {
            "first_name": "First",
            "last_name": "Facility",
            "email": "f1@alemhealth.com",
            "password": "qweqwe"
        },
        "sendEmail": "false",
        "name": "Facility One",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "short_code": "f1",
        "hospital_type": "3",
        "address": "Facility1 Address",
        "phone": "1234567890",
        "general_email": "facility1@alemhealth.com",
        "city": "Facility1 City",
        "country": "BD",
        "primary_name": "Facility1 Contact",
        "primary_email": "f1_contact@alemhealth.com",
        "primary_physician_contact": "F1 Contact Physician"
    }, {
        "user": {
            "first_name": "Second",
            "last_name": "Facility",
            "email": "f2@alemhealth.com",
            "password": "qweqwe"
        },
        "sendEmail": "false",
        "name": "Facility Two",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "short_code": "f2",
        "hospital_type": "3",
        "address": "Facility2 Address",
        "phone": "1234567890",
        "general_email": "facility2@alemhealth.com",
        "city": "Facility2 City",
        "country": "BD",
        "primary_name": "Facility2 Contact",
        "primary_email": "f2_contact@alemhealth.com",
        "primary_physician_contact": "F2 Contact Physician"
    }, {
        "user": {
            "first_name": "Third",
            "last_name": "Facility",
            "email": "f3@alemhealth.com",
            "password": "qweqwe"
        },
        "sendEmail": "false",
        "name": "Facility Three",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "short_code": "f3",
        "hospital_type": "3",
        "address": "Facility3 Address",
        "phone": "1234567890",
        "general_email": "facility3@alemhealth.com",
        "city": "Facility1 City",
        "country": "BD",
        "primary_name": "Facility3 Contact",
        "primary_email": "f3_contact@alemhealth.com",
        "primary_physician_contact": "F3 Contact Physician"
    }, {
        "user": {
            "first_name": "Fourth",
            "last_name": "Facility",
            "email": "f4@alemhealth.com",
            "password": "qweqwe"
        },
        "sendEmail": "false",
        "name": "Facility Four",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "short_code": "f4",
        "hospital_type": "3",
        "address": "Facility4 Address",
        "phone": "1234567890",
        "general_email": "facility4@alemhealth.com",
        "city": "Facility1 City",
        "country": "BD",
        "primary_name": "Facility4 Contact",
        "primary_email": "f4_contact@alemhealth.com",
        "primary_physician_contact": "F4 Contact Physician"
    }, {
        "user": {
            "first_name": "Fifth",
            "last_name": "Facility",
            "email": "f5@alemhealth.com",
            "password": "qweqwe"
        },
        "sendEmail": "false",
        "name": "Facility Five",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "short_code": "f5",
        "hospital_type": "3",
        "address": "Facility5 Address",
        "phone": "1234567890",
        "general_email": "facility1@alemhealth.com",
        "city": "Facility5 City",
        "country": "BD",
        "primary_name": "Facility5 Contact",
        "primary_email": "f5_contact@alemhealth.com",
        "primary_physician_contact": "F5 Contact Physician"
    }]

    for singleFacility in data:
        create_facility(url, singleFacility)
