import os
import json
from django.test import RequestFactory
from alemhealth import settings
from django.contrib.auth.models import User
from alemhealth.api import ProvidersView, ProviderLogoView
from django.core.files.uploadedfile import SimpleUploadedFile
from django.core.files import File


# Upload Logo
def upload_logo(url, file_path):
    filename = os.path.basename(file_path)
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url)
    file_obj = File(open(file_path, 'rb'))
    uploaded_file = SimpleUploadedFile(filename, file_obj.read(), content_type='multipart/form-data')
    request.FILES['file'] = uploaded_file
    return ProviderLogoView.as_view()(request)


# Create Provider
def create_provider(url, data):
    request_factory_object = RequestFactory()
    request = request_factory_object.post(url, content_type="application/json")
    request.user = User.objects.get(username='admin')
    request._body = json.dumps(data)
    ProvidersView.as_view()(request)


def run():
    # Logo Upload URL
    url = "/api/providers/logo/?secret_key=" + settings.CLIENT_SECRET_KEY

    # Logo File Path
    filename = "dummy_alemhealth_logo.png"
    file_path = os.path.join(settings.BASE_DIR, "static", "dashboard", "images", filename)

    response = upload_logo(url, file_path)
    response = json.loads(response.content)

    # Provider Default Data
    url = "/api/providers?secret_key=" + settings.CLIENT_SECRET_KEY
    data = [{
        "user": {
            "first_name": "First",
            "last_name": "Provider",
            "email": "p1@alemhealth.com",
            "password": "qweqwe"
        },
        "is_csend": "false",
        "sendEmail": "false",
        "name": "Provider One",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "phone": "1234567890",
        "address": "Provider1 Address",
        "email": "provider1@alemhealth.com",
        "city": "Provider1 City",
        "country": "BD",
        "primary_name": "Provider1 Contact",
        "primary_email": "contact1@alemhealth.com",
        "primary_phone": "1234567890",
        "routine_turnaround": 1,
        "stat_turnaround": 1
    }, {
        "user": {
            "first_name": "Second",
            "last_name": "Provider",
            "email": "p2@alemhealth.com",
            "password": "qweqwe"
        },
        "is_csend": "false",
        "sendEmail": "false",
        "name": "Provider Two",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "phone": "1234567890",
        "address": "Provider2 Address",
        "email": "provider2@alemhealth.com",
        "city": "Provider2 City",
        "country": "BD",
        "primary_name": "Provider2 Contact",
        "primary_email": "contact2@alemhealth.com",
        "primary_phone": "1234567890",
        "routine_turnaround": 1,
        "stat_turnaround": 1
    }, {
        "user": {
            "first_name": "Third",
            "last_name": "Provider",
            "email": "p3@alemhealth.com",
            "password": "qweqwe"
        },
        "is_csend": "false",
        "sendEmail": "false",
        "name": "Provider Three",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "phone": "1234567890",
        "address": "Provider3 Address",
        "email": "provider3@alemhealth.com",
        "city": "Provider3 City",
        "country": "BD",
        "primary_name": "Provider3 Contact",
        "primary_email": "contact3@alemhealth.com",
        "primary_phone": "1234567890",
        "routine_turnaround": 1,
        "stat_turnaround": 1
    }, {
        "user": {
            "first_name": "Fourth",
            "last_name": "Provider",
            "email": "p4@alemhealth.com",
            "password": "qweqwe"
        },
        "is_csend": "false",
        "sendEmail": "false",
        "name": "Provider Four",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "phone": "1234567890",
        "address": "Provider4 Address",
        "email": "provider4@alemhealth.com",
        "city": "Provider4 City",
        "country": "BD",
        "primary_name": "Provider4 Contact",
        "primary_email": "contact4@alemhealth.com",
        "primary_phone": "1234567890",
        "routine_turnaround": 1,
        "stat_turnaround": 1
    }, {
        "user": {
            "first_name": "Fifth",
            "last_name": "Provider",
            "email": "p5@alemhealth.com",
            "password": "qweqwe"
        },
        "is_csend": "false",
        "sendEmail": "false",
        "name": "Provider Five",
        "logo": response["logo"],
        "logo_file": response["logo_file"],
        "phone": "1234567890",
        "address": "Provider5 Address",
        "email": "provider5@alemhealth.com",
        "city": "Provider5 City",
        "country": "BD",
        "primary_name": "Provider5 Contact",
        "primary_email": "contact5@alemhealth.com",
        "primary_phone": "1234567890",
        "routine_turnaround": 1,
        "stat_turnaround": 1
    }]

    for singleProvider in data:
        create_provider(url, singleProvider)
