from fabric.api import *
from fabric.operations import *
from fabric.contrib import django
from fabric.contrib.project import rsync_project
from fabric.contrib.console import confirm


import sys, os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
os.environ['DJANGO_SETTINGS_MODULE'] = 'alemhealth.settings'
abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)
from alemhealth import settings

# --------------------------------------------
# AWS Machines
# --------------------------------------------


def dev():

    print "Connecting to alemhealth AWS dev server"

    env.setup = False
    env.user = 'ubuntu'
    env.key_filename = abspath('alemhealth.pem')
    env.hosts = [
        '52.74.28.43',
        ]
    env.serverpath = '/home/ubuntu/alemhealth'
    env.virtualenvpath = '/home/ubuntu/virtual-env/alemhealth'
    env.scriptpath = '/home/ubuntu/alemhealth/script/'

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    # env.mysql_user = 'root'
    # env.mysql_password = '!qweqwe@#'
    # env.mysql_db = "alemhealth"
    # env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        '--exclude alemhealth/migrations',
        '--exclude order/migrations',
        '--exclude media',
        '--exclude node_modules',
        '--exclude alemhealth/local_settings.py',
        '--exclude storescp',
        '--exclude *.pyc',
        '--exclude log',
        '--exclude celerybeat-schedule.db',
        '--exclude celerybeat.pid',
        '--exclude celeryev.pid',
        '--exclude static/bower_components',
        '--exclude static/client/build',
    ]

    env.is_grunt = True
    
    return


def prod_nginx():

    print "Connecting to alemhealth AWS nginx Production server"
    print 'Need to install , bower. need to export dr CSV etc ..'
    env.setup = False
    env.user = 'ubuntu'
    env.key_filename = abspath('alemhealth.pem')
    env.hosts = [
        '54.251.168.110',
        ]
    env.serverpath = '/home/ubuntu/alemhealth'
    env.virtualenvpath = '/home/ubuntu/virtual-env/alemhealth'
    env.scriptpath = '/home/ubuntu/alemhealth/script/'

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.nginx_site_config = 'dev-ops/dev/site.nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    # env.mysql_user = 'root'
    # env.mysql_password = '!qweqwe@#'
    # env.mysql_db = "alemhealth"
    # env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        '--exclude alemhealth/migrations',
        '--exclude order/migrations',
        '--exclude media',
        '--exclude node_modules',
        '--exclude alemhealth/local_settings.py',
        '--exclude storescp',
        '--exclude *.pyc',
        '--exclude log',
        '--exclude celerybeat-schedule.db',
        '--exclude celerybeat.pid',
        '--exclude celeryev.pid',
        '--exclude static/bower_components',
        '--exclude static/client/build',
    ]

    env.is_grunt = True
    return


def prod_celery():

    print "Connecting to alemhealth AWS celery Production server"

    env.setup = False
    env.user = 'ubuntu'
    env.key_filename = abspath('alemhealth.pem')
    env.hosts = [
        '52.74.27.28',
        ]
    env.serverpath = '/home/ubuntu/alemhealth'
    env.virtualenvpath = '/home/ubuntu/virtual-env/alemhealth'
    env.scriptpath = '/home/ubuntu/alemhealth/script/'

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.nginx_site_config = 'dev-ops/dev/site.nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    env.mysql_user = 'root'
    env.mysql_password = '!qweqwe@#'
    env.mysql_db = "alemhealth"
    env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        '--exclude alemhealth/migrations',
        '--exclude order/migrations',
        '--exclude media',
        '--exclude node_modules',
        '--exclude alemhealth/local_settings.py',
        '--exclude storescp',
        '--exclude *.pyc',
        '--exclude log',
        '--exclude celerybeat-schedule.db',
        '--exclude celerybeat.pid',
        '--exclude celeryev.pid',
        '--exclude static/bower_components',
        '--exclude static/client/build',
    ]

    env.is_grunt = False
    return

# --------------------------------------------
# Installing Alemhealth Platform
# --------------------------------------------

# install nginx instance
def install_nginx_instance():
    print "Connecting nginx instance " + env.host
    install_python_dependency()
    install_nodejs()
    install_nginx()
    install_supervisor()
    create_uwsgi_supervisor()
    sync_code_base()
    pip_requirements()
    grunt()

# install nginx instance
def install_celery_instance():
    print "Connecting celery instance " + env.host
    install_python_dependency()
    install_dcmtk()
    install_supervisor()
    sync_code_base()
    pip_requirements()
    create_celery_supervisor()



def install_ah():
    if env.setup:
        # install_python_dependency()
        # install_nodejs()
        # install_nginx()
        install_rabbitmq()
        install_supervisor()
        create_supervisor_services()


def install_python_dependency():
    """
    install python dependency packages
    """
    print "Installing python dependency packages"
    sudo(
        'apt-get -y install '
        'python-setuptools libmysqlclient-dev '
        'python-mysqldb libmysqlclient-dev '
        'python-dev make automake gcc '
        'libxml2 libxml2-dev libxslt-dev '
        'python-dev libtidy-dev python-lxml '
        'htop iftop'
    )

    sudo(
        'apt-get install -y '
        'libjpeg62 zlib1g-dev libjpeg8-dev '
        'libjpeg-dev libfreetype6-dev '
        'libpng-dev libgif-dev'
    )
    sudo('apt-get -y install libcurl4-gnutls-dev librtmp-dev')
    sudo('apt-get install -y libjpeg-turbo8-dev')
    sudo('apt-get install -y libjpeg62-dev')

    # set up virtualenv
    print "Installing python virtualenv"
    sudo('apt-get -y install python-virtualenv')
    sudo('mkdir -p %s/virtual-env' % env.home)
    print "Create alemhealth virtualenv"
    sudo('cd %s/virtual-env/; virtualenv  alemhealth' % env.home)

def install_mysql():
    """
    install mysql with
    setup with passwords
    """
    print "Installing mysql"
    sudo('apt-get install debconf-utils')
    run('echo "mysql-server-5.1 mysql-server/root_password password !qweqwe@#" > ~/mysql.preseed')
    run('echo "mysql-server-5.1 mysql-server/root_password_again password !qweqwe@#" >> ~/mysql.preseed')
    run('echo "mysql-server-5.1 mysql-server/start_on_boot boolean true" >> ~/mysql.preseed')
    run('cat ~/mysql.preseed | sudo debconf-set-selections')
    sudo("apt-get install mysql-server -y")
    run('mysql -u root -h localhost -p"!qweqwe@#" -e "create database alemhealth CHARACTER SET utf8 COLLATE utf8_general_ci";')


def install_nodejs():
    print 'Installing nodejs 0.10 with nvm'
    run(
        'curl https://raw.githubusercontent.com/'
        'creationix/nvm/v0.10.0/install.sh | sh'
    )
    run('echo ". ~/.nvm/nvm.sh" >> ~/.bash_profile')
    run('nvm install 0.10')
    run('nvm use 0.10')
    run('nvm alias default 0.10')


def install_nginx():

    print 'Installing uwsgi with PIP'
    sudo('pip install uwsgi')
    print 'Installing Nginx'
    sudo('apt-get -y install nginx')
    put('%s/%s' % (settings.BASE_DIR, env.nginx_config),
        '/etc/nginx/',
        use_sudo=True)

    put('%s/%s' % (settings.BASE_DIR, env.nginx_site_config),
        '/etc/nginx/sites-enabled/',
        use_sudo=True)

    sudo(
        'mv /etc/nginx/sites-enabled/site.nginx.conf '
        '/etc/nginx/sites-enabled/nginx.conf'
    )

    sudo("service nginx restart")
    # sudo nano /etc/nginx/nginx.conf  change the root


def install_rabbitmq():
    print 'Installing Rabbitmq'
    sudo('apt-get install rabbitmq-server')

def install_dcmtk():
    print 'Installing dcmtk'
    sudo('apt-get install dcmtk')


def install_supervisor():
    print 'Installing Supervisor'
    sudo('apt-get install supervisor')


def create_uwsgi_supervisor():
    print 'Store supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.uwsgi_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    sudo('supervisorctl reread all')

def create_celery_supervisor():

    print 'Store celery supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.celery_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    print 'Store celerybeat supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.celerybeat_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    print 'Store dcmtk listener supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.storescp_config),
        '/etc/supervisor/conf.d/', use_sudo=True)


    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    sudo('supervisorctl reread all')

def create_supervisor_services():
    print 'Store supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.uwsgi_config),
        '/etc/supervisor/conf.d/', use_sudo=True)
    put('%s/%s' % (settings.BASE_DIR, env.celery_config),
        '/etc/supervisor/conf.d/', use_sudo=True)
    put('%s/%s' % (settings.BASE_DIR, env.celerybeat_config),
        '/etc/supervisor/conf.d/', use_sudo=True)
    put('%s/%s' % (settings.BASE_DIR, env.storescp_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    sudo('supervisorctl reread all')


# --------------------------------------------
# Deploying Alemhealth Platform
# --------------------------------------------


def deploy():
    sync_code_base()
    pip_requirements()
    #syncdb()

    if env.is_grunt:
        grunt()
    restart_services()


def sync_code_base():
    print 'rsync Alemhealth code base'
    local('rsync -rave "ssh -i %s" %s  %s/* %s@%s:%s' % (
        settings.BASE_DIR + '/alemhealth.pem',
        ' '.join(env.rsync_exclude),
        settings.BASE_DIR, env.user,
        env.hosts[0],
        env.serverpath
        )
    )

def pip_requirements():
    sudo('source %s/bin/activate; cd %s; pip install -r alemhealth/requirements.txt;' % (env.virtualenvpath, env.serverpath))

def syncdb():
    run('source %s/bin/activate; cd %s; python manage.py syncdb;' % (env.virtualenvpath, env.serverpath))

def grunt(tasks='dev'):
    if env.setup:
        sudo("cd %s;nvm use 0.10; npm install; npm install -g grunt-cli; grunt %s;npm install -g bower;" % (env.serverpath, tasks))
    else:
        run("cd %s;nvm use 0.10;cd %s/static/;bower install;grunt %s;" % (env.serverpath, env.serverpath, tasks)) #;npm install -g bower


def restart_services():

    # print 'Restart nginx ...'
    # sudo("service nginx restart")

    print 'Restart all supervisor services ...'
    sudo('supervisorctl restart all')

    # print('Restart logstash-forwared')
    # sudo('service logstash-forwarder restart')

def migrate(app_name):
    sudo('source %s/bin/activate; cd %s; python manage.py schemamigration %s --auto;python manage.py migrate %s --ignore-ghost-migrations;' % (env.virtualenvpath, env.serverpath, app_name, app_name))

