module.exports = function(grunt) {
    grunt.initConfig({
        concat: {
            dashboard_js: {
                src: [

                    // need to be bower file 
                    'static/new-dashboard/js/jquery-2.1.0.min.js',
                    'static/new-dashboard/js/bootstrap.min.js',
                    'static/client/app/libs/pdf.js',
                    // 'static/client/app/libs/cornerstone.min.js',
                    'static/client/app/libs/cornerstone_latest.min.js',
                    'static/client/app/libs/jquery.hammer-full.js',
                    'static/client/app/libs/cornerstoneMath.min.js',
                    'static/client/app/libs/cornerstoneTools.min.js',
                    'static/client/app/libs/dicomParser.min.js',
                    'static/client/app/libs/jpx.min.js',
                    'static/client/app/libs/cornerstoneWADOImageLoader.min.js',

                    'static/client/app/libs/angular.min.js',
                    'static/client/app/libs/angular.resource.min.js',
                    'static/client/app/libs/angular-ui-router.min.js',
                    'static/client/app/libs/angular.cookies.min.js',
                    'static/client/app/libs/peke.upload.js',
                    'static/client/app/libs/ui-bootstrap-tpls-0.10.0.min.js',
                    'static/client/app/libs/angular-flash.js',
                    'static/client/app/libs/bootstrap3-wysihtml5.js',
                    'static/client/app/libs/ng-infinite-scroll.min.js',
                    


                    'static/client/app/libs/ng-tags-input.min.js',
                    'static/client/app/libs/textAngular-sanitize.min.js',
                    'static/client/app/libs/textAngular.min.js',


                    'static/client/app/libs/ng-google-chart.js',

                    'static/client/app/libs/angular-file-upload.js',

                    'static/client/app/libs/ng-flow-standalone.min.js',

                    'static/bower_components/angular-bootstrap-checkbox/angular-bootstrap-checkbox.js',
                    
                    // multi select 
                    'static/bower_components/lodash/dist/lodash.min.js',
                    'static/bower_components/angularjs-dropdown-multiselect/dist/angularjs-dropdown-multiselect.min.js',
                    
                    // moment js 

                    'static/bower_components/moment/min/moment.min.js',
                    
                    //  ngInfiniteScroll

                    'static/bower_components/ngInfiniteScroll/build/ng-infinite-scroll.min.js',

                    // Angular loading bar

                    'static/bower_components/angular-loading-bar/build/loading-bar.min.js',

                    // nanobar

                    'static/bower_components/nanobar/nanobar.min.js',

                    //jrange

                    'static/bower_components/jRange/jquery.range.js',

                    // datepicker range 
                    'static/bower_components/bootstrap-daterangepicker/daterangepicker.js',
                    'static/bower_components/angular-daterangepicker/js/angular-daterangepicker.js',
                    // jquery ui
                    'static/bower_components/jquery-ui/jquery-ui.min.js',

                    'static/client/app/app.js',
                    'static/client/app/modules/*/app.js',
                    'static/client/app/modules/*/*.js',
                    'static/client/app/common/*/*.js'


                ],
                dest: 'static/client/build/script.js'
            },
            
            dashboard_css: {
               src: [
                    'static/new-dashboard/css/bootstrap.min.css',
                    'static/new-dashboard/font-awesome-4.3.0/css/font-awesome.min.css',
                    'static/bower_components/angular-loading-bar/build/loading-bar.min.css',
                    'static/bower_components/jRange/jquery.range.css',
                    'static/new-dashboard/css/ah-admin.css',
                    'static/new-dashboard/css/dicom-viewer.css',
                    'static/new-dashboard/css/cornerstone.min.css',
                    // date range picker 
                    'static/bower_components/bootstrap-daterangepicker/daterangepicker-bs3.css',
                    // drag n drop                     
                    'static/bower_components/dragula.js/dist/dragula.min.css',

               ],
               dest: 'static/client/build/style.css'
            }


        },
        sass: {                              
            dist: {                            
                
                files: {                         
                    'static/client/build/new_style.css': 'static/new-dashboard/scss/main.scss'
                },
                options: {
                    "sourcemap=none": '',
                    'style' : 'compact'
                }
            }
        },
        sass_globbing: {
            your_target: {
                files: {
                    'static/new-dashboard/scss/main.scss': [
                        'static/new-dashboard/scss/vendor/jeet/index.scss',
                        'static/new-dashboard/scss/all/**/*.scss'
                    ]
                }
            }
        },
        watch: {
            files: [
                    'static/client/app/app.js',
                    'static/client/app/modules/*/app.js',
                    'static/client/app/modules/*/*.js',
                    'static/client/app/common/*/*.js',
                    'static/client/app/libs/*.js',
                    'static/new-dashboard/css/ah-admin.css',
                    'static/new-dashboard/css/dicom-viewer.css',
                    'static/new-dashboard/scss/all/*/*.scss',
                    'static/new-dashboard/scss/main.scss'
            ],
            tasks: ['dev:alemhealth']
        }
    });
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-sass-globbing');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    

    //register tasks
    grunt.registerTask('dev:alemhealth',
        [
            'concat:dashboard_js',
            'concat:dashboard_css',
            'sass_globbing',
            'sass'

        ]);


    grunt.registerTask('dev', ['dev:alemhealth']);

};