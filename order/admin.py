from django.contrib import admin

# Register your models here.
from order.models import *

admin.site.register(Price)



class OrderDetailInline(admin.TabularInline):
    model = Order

class OrderAdmin(admin.ModelAdmin):
    list_display = ("id", 'patient',"created_at",)
    list_filter = ("created_at",)


admin.site.register(Order, OrderAdmin)


#admin.site.register(Order)