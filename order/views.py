from django.http import HttpResponse
from order.tasks import *
from django.views.decorators.csrf import csrf_exempt
import json


@csrf_exempt
def dicom_s3_sync_complete(request):

	json_dict = json.loads(request.body)
	key = json_dict.get('key')
	hospital = json_dict.get('hospital')

	if not key:
		return HttpResponse(json.dumps({'error' :  's3 key not found'}) , content_type="application/json", status= 500)

	try:
		#import pdb; pdb.set_trace()

		task = store_dicom_from_s3_bucket.delay(key, hospital)
		return HttpResponse(json.dumps({ 'message' : 'Task successfully created : ' + str(task)}) , content_type="application/json", status= 200)
		#return HttpResponse({'Task successfully created : ' + str(task)})
	except Exception as e:
		#return HttpResponse(status=500)
		return HttpResponse(json.dumps({'error' :  str(e)}) , content_type="application/json", status= 500)


@csrf_exempt
def mirth(request):
	#import pdb; pdb.set_trace()
	print request.body
	return HttpResponse({}, status=200)
