from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
from django.utils.timezone import utc
# Create your models here.

import uuid
from alemhealth.models import *
from order.tasks import *
from order.emails import *
from alemhealth.jsonfield import JSONField
from alemhealth.utils import app_storage_url

import dicom
import os
import shutil
import datetime

from crum import get_current_request


def doctor_signature_file_upload_dir(instance, filename):

        t = filename.split(" ")
        # there is space in filename need to replace with '_'
        if len(t) > 1:
            filename = filename.replace(" ", "_")

        return 'doctor_signatures/' + str(instance.guid) + '/' + filename


def doctor_signatures_file_upload_dir(instance, filename):

        t = filename.split(" ")
        # there is space in filename need to replace with '_'
        if len(t) > 1:
            filename = filename.replace(" ", "_")

        return 'doctor_signatures/' + str(instance.order.guid) + '/' + filename


def receipt_file_upload_dir(instance, filename):
        return 'receipts/' + str(instance.guid) + '/' + filename


def report_file_upload_dir(instance, filename):
        return 'reports/' + str(instance.guid) + '/' + filename


def normal_report_file_upload_dir(instance, filename):
    return 'reports/' + str(instance.guid) + '/' + filename


def zipfile_upload_dir(instance, filename):
        return 'zipfiles/' + str(instance.guid) + '/' + filename


# hospital study price
class Price(models.Model):

    priority_list = (
        ('Routine', 'Routine'),
        ('STAT', 'STAT')
    )
    modality_list = (
        ('CT', 'CT'),
        ('NM', 'NM'),
        ('MR', 'MR'),
        ('DS', 'DS'),
        ('DR', 'DR'),
        ('US', 'US'),
        ('OT', 'OT'),
        ('HSG', 'HSG'),
        ('CR', 'CR'),
        ('MG', 'MG'),
        ('OP', 'OP'),
    )

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    provider = models.ForeignKey(
        Provider, related_name='prices')
    priority = models.CharField(
        max_length=12, choices=priority_list)
    modality = models.CharField(
        max_length=10, choices=modality_list)
    unit_cost = models.DecimalField(max_digits=8, decimal_places=2)
    unit_price = models.DecimalField(max_digits=8, decimal_places=2)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'prices'
        unique_together = (("provider", "priority", 'modality'),)

    def __unicode__(self):
        return self.provider.name + '-' + self.priority + '-' + self.modality + ' : ' + str(self.unit_cost)
# http://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.xchange%20where%20pair%20in%20(%22USDAFN%22)&env=store://datatables.org/alltableswithkeys&format=json


class Receipt(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    price = models.ForeignKey(
        Price, related_name='receipts')
    total_price = models.DecimalField(max_digits=8, decimal_places=2)
    tax_price = models.DecimalField(max_digits=8, decimal_places=2, blank=True, null=True)
    total_price_with_tax = models.DecimalField(max_digits=8, decimal_places=2)

    receipt_pdf = models.FileField(
        upload_to=receipt_file_upload_dir, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'receipts'

    def __unicode__(self):
        return str(self.guid)


class OrderManager(models.Manager):

    def get_updated_reports(self, facility_guid, after_datetime, base_url=''):
        facility_id = Hospital.objects.get(guid=facility_guid).id
        synced_time = datetime.datetime.now()
        updated_studies = self.filter(
            report_file_updated_at__gte=after_datetime
        ).filter(
            report__isnull=False
        ).filter(
            hospital_id=facility_id
        ).values(
            'guid', 'report', 'report_file_updated_at', 'report_filename'
        )

        for study in updated_studies:
            study['guid'] = str(study['guid'])
            study['report_file_updated_at'] = study['report_file_updated_at'].strftime("%Y-%m-%d %H:%M:%S")
            study['report_file_url'] = app_storage_url(base_url, study['report_filename'])
            study['report_filename'] = study['report_filename'].split('/')[-1]

        result = {
            'synced_time': synced_time.strftime("%Y-%m-%d %H:%M:%S"),
            'updated_studies': list(updated_studies)
        }

        return result


class Order(models.Model):

    modality_list = (
        ('CT', 1),
        ('NM', 2),
        ('MR', 3),
        ('DS', 4),
        ('DR', 5),
        ('US', 6),
        ('OT', 7),
        ('HSG', 8),
        ('CR', 9),
        ('OP', 10),
    )

    priority_list = (
        ('Routine', 'Routine'),
        ('STAT', 'STAT')
    )

    status_list = (
        (0, 'Draft study'),
        (1, 'Study stored'),
        (2, 'Study syncing'),
        (3, 'Retry routing'),
        (4, 'Report pending'),
        (5, 'Report ready'),
        (6, 'Report delivered'),
    )

    meta_status_list = (
        (0, 'Not extracted'),
        (1, 'Extracted'),
        (2, 'Meta write'),
    )

    billing_status_list = (
        (0, 'Not billed'),
        (1, 'Free Study'),
        (2, 'Invoice Pending'),
        (3, 'Paid'),
    )

    active_status_list = (
        (0, 'Not Active'),
        (1, 'Active'),
    )

    read_status_list = (
        (0, 'Not read'),
        (1, 'Read'),
    )

    hub_sync_list = (
        (0,'not synchronized'),
        (1,'started synchronizing'),
        (2,'synchronization completed'),
    )

    objects = OrderManager()

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    dicom_uid = models.TextField(null=True, blank=True)

    # associations
    operator = models.ForeignKey(
        User,
        related_name='orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    patient = models.ForeignKey(
        Patient,
        related_name='orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    doctor = models.ForeignKey(
        Doctor, related_name='orders', blank=True, null=True)
    dr_code = models.IntegerField(null=True, blank=True)
    referral_code = models.IntegerField(null=True, blank=True)

    hospital = models.ForeignKey(
        Hospital, related_name='orders', blank=True, null=True)
    provider = models.ForeignKey(
        Provider,
        related_name='orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    second_provider = models.ForeignKey(
        Provider,
        related_name='second_orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )


    radiologist = models.ForeignKey(
        Doctor,
        related_name='radiologist_orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )

    provider_radiologist = models.ForeignKey(
        Doctor,
        related_name='provider_radiologist_orders',
        blank=True,
        null=True,
        on_delete=models.SET_NULL
    )


    accession_id = models.CharField(max_length=128, null=True, blank=True)
    modality = models.CharField(
        max_length=10, null=True, blank=True, choices=modality_list)
    speciality = models.TextField(null=True, blank=True)
    history = models.TextField(null=True, blank=True)

    priority = models.CharField(
        max_length=12, choices=priority_list, blank=True, null=True)
    status = models.IntegerField(choices=status_list, default=0)

    meta_status = models.IntegerField(choices=meta_status_list, default=0)
    billing_status = models.IntegerField(choices=billing_status_list, default=0)
    active_status = models.IntegerField(choices=active_status_list, default=1)

    # reports
    report = models.TextField(null=True, blank=True)
    doctor_signature = models.FileField(
        upload_to=doctor_signature_file_upload_dir, null=True, blank=True)
    report_filename = models.FileField(
        upload_to=report_file_upload_dir, null=True, blank=True)
    normal_report_filename = models.FileField(
        upload_to=normal_report_file_upload_dir, null=True, blank=True)
    report_file_updated_at = models.DateTimeField(null=True, blank=True, default=None)
    # 2nd opinion report
    second_report = models.TextField(null=True, blank=True)
    second_report_filename = models.FileField(
        upload_to=report_file_upload_dir, null=True, blank=True)


    zipfile = models.FileField(
        upload_to=zipfile_upload_dir, null=True, blank=True)

    receipt = models.OneToOneField(
        Receipt, related_name='order', null=True, blank=True)

    report_time = models.DecimalField(
        max_digits=18, decimal_places=5, blank=True, null=True)

    read_status = models.IntegerField(choices=read_status_list, default=0)

    hub_sync = models.IntegerField(choices=hub_sync_list, default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    op_status = models.CharField(max_length=128, null=True, blank=True)

    is_storage = models.BooleanField(default=False)


    class Meta:
        db_table = 'orders'

    def __unicode__(self):
        return str(self.guid)

    def save(self, *args, **kwargs):

        request = get_current_request()

        # update some dicom for crestView
        if request and request.user.is_superuser and request.user.username == 'master' and self.status == 0:
            dicom_meta_write.delay(self.guid, request.user)
        if self.status == 5:
            # change the report time while report ready
            now = datetime.datetime.utcnow().replace(tzinfo=utc)
            report_time_dif = now - self.created_at
            self.report_time = round(
                report_time_dif.total_seconds() / 60 / 60, 2)
            # select DATE_FORMAT(created_at,'%Y-%b') , avg(report_time) from orders  where report_time !='' GROUP BY  DATE_FORMAT(created_at,'%Y-%m')
            # select created_at , avg(report_time) from orders  where report_time !='' GROUP BY  week(DATE_SUB(created_at, INTERVAL 1 DAY))
        super(Order, self).save(**kwargs)
        # start dicom routing to Provider
        if self.status == 2:
            send_dicom_to_provider.delay(guid=self.guid)
            # send_dicom_to_provider(guid=self.guid)
        if self.status == 5:
            send_operator_report_ready(self)


# delete dicom files when order delete
def delete_dicom_files(sender, instance, **kwargs):

    if settings.USE_S3:
        for dicom_obj in instance.dicoms.all():
            dicom_obj.file.delete()

    else:
        folder = settings.BASE_DIR + '/media/dicoms/' + str(instance.guid)
        if(os.path.isdir(folder)):
            for the_file in os.listdir(folder):
                file_path = os.path.join(folder, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception, e:
                    print e
            try:
                shutil.rmtree(folder)
            except Exception, e:
                print e


models.signals.pre_delete.connect(delete_dicom_files, sender=Order)


class DoctorSignature(models.Model):
    order = models.ForeignKey(Order, related_name='doctor_signatures')
    signature = models.FileField(
        upload_to=doctor_signatures_file_upload_dir)

    class Meta:
        db_table = 'doctor_signatures'

    def __unicode__(self):
        return str(self.signature)


class SecondDoctorSignature(models.Model):
    order = models.ForeignKey(Order, related_name='second_doctor_signatures')
    signature = models.FileField(
        upload_to=doctor_signatures_file_upload_dir)

    class Meta:
        db_table = 'second_doctor_signatures'

    def __unicode__(self):
        return str(self.signature)


class OrderMeta(models.Model):
    order = models.ForeignKey(Order, related_name='metas')
    key = models.TextField()
    value = models.TextField(null=True, blank=True)

    class Meta:
        db_table = 'order_metas'

    def __unicode__(self):
        return self.key


def dicom_file_upload_dir(instance, filename):

        t = filename.split(" ")
        # there is space in filename need to replace with '_'
        if len(t) > 1:
            filename = filename.replace(" ", "_")


        # if it is using the tmp dir for zpaq compression
        if filename.find('tmp') >= 0:
            file_array = filename.split('/')
            return 'dicoms/' + str(instance.order.guid.hex) + '/' + file_array[3]
        else:
            return 'dicoms/' + str(instance.order.guid.hex) + '/' + filename


class DicomFile(models.Model):

    edit_meta_list = (
        (0, 'Not edit'),
        (1, 'Edit'),
        (2, 'Send'),
    )

    transfer_syntax_list = (
        ('LittleEndianImplicit', ''),
        ('LittleEndianExplicit', ''),
        ('DeflatedExplicitVRLittleEndian', ''),
        ('BigEndianExplicit', ''),
        ('JPEGProcess1', ''),
        ('JPEGProcess2_4', ''),
        ('JPEGProcess14SV1', ''),
        ('JPEGLSLossless', '--propose-jls-lossless'),
        ('JPEGLSLossy', '--propose-jls-lossy'),
        ('JPEG2000LosslessOnly', '--propose-j2k-lossless'),
        ('JPEG2000' , '--propose-j2k-lossy'),
        ('MPEG2MainProfileAtMainLevel', '--propose-mpeg2'),
        ('MPEG2MainProfileAtHighLevel', '--propose-mpeg2-high'),
        ('RLELossless', '--propose-rle'),
    )
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    order = models.ForeignKey(Order, related_name='dicoms')
    file = models.FileField(upload_to=dicom_file_upload_dir, max_length=320)

    edit_meta = models.IntegerField(choices=edit_meta_list, default=0)

    transfer_syntax = models.CharField(
        max_length=128, blank=True, null=True, choices=transfer_syntax_list)
    annotation_data = JSONField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'dicom_images'

    def __unicode__(self):
        return str(self.guid)

    def save(self, *args, **kwargs):
        # check dicom filename  has space or not
        super(DicomFile, self).save(**kwargs)

        # if self.file.name:
        #     t = self.file.name.split(" ")
        #     # there is space in filename need to replace with '_'
        #     if len(t) > 1:
        #         filename = settings.BASE_DIR + '/media/' + self.file.name
        #         os.rename(filename, filename.replace(" ", "_"))
        #         self.file.name = self.file.name.replace(" ", "_")
        #         super(DicomFile, self).save(**kwargs)

        # extract meta data for first time
        if self.order.meta_status == 0:
            try:
                dicom_file = dicom.read_file(self.file)
            except Exception as e:
                print str(e)
                return

            # add new patient
            # todo: get or create patient
            patient, created = Patient.objects.get_or_create(
                name=str(dicom_file.get('PatientName')),
                gender=str(dicom_file.get('PatientSex')),
                phone=str(dicom_file.get('PatientTelephoneNumbers'))
            )

            patient.date_of_birth = str(dicom_file.get('PatientBirthDate'))
            patient.patient_id = 'P-' + str(patient.pk)
            patient.save()

            self.order.patient = patient
            self.order.modality = str(dicom_file.get('Modality'))
            self.order.dicom_uid = str(dicom_file.get('StudyInstanceUID'))
            # self.order.accession_id = str(dicom_file.get('AccessionNumber'))
            self.order.save()

            # add all metas
            OrderMeta.objects.bulk_create([
                OrderMeta(
                    order=self.order, key='BodyPartExamined',
                    value=str(dicom_file.get('BodyPartExamined'))),
                OrderMeta(
                    order=self.order, key='DateofSecondaryCapture',
                    value=str(dicom_file.get('DateofSecondaryCapture'))),
                OrderMeta(
                    order=self.order, key='TimeofSecondaryCapture',
                    value=str(dicom_file.get('TimeofSecondaryCapture'))),
                OrderMeta(
                    order=self.order, key='StudyDate',
                    value=str(dicom_file.get('StudyDate'))),
                OrderMeta(
                    order=self.order, key='StudyTime',
                    value=str(dicom_file.get('StudyTime'))),
                OrderMeta(
                    order=self.order, key='InstitutionName',
                    value='Alem Health'),
                OrderMeta(
                    order=self.order, key='InstitutionAddress',
                    value="Block B, Core F Unit 304 Downtown, Dubai, UAE"),
                # OrderMeta(order=self.order, key='ReferringPhysician',
                #    value=str(dicom_file.get('ReferringPhysician'))),
                OrderMeta(
                    order=self.order, key='StudyDescription',
                    value=str(dicom_file.get('StudyDescription'))),
                OrderMeta(
                    order=self.order, key='SeriesDescription',
                    value=str(dicom_file.get('SeriesDescription'))),
                OrderMeta(
                    order=self.order, key='OperatorsName',
                    value=str(dicom_file.get('OperatorsName'))),
                OrderMeta(
                    order=self.order, key='TransferSyntax',
                    value=str(dicom_file.get('TransferSyntax'))),
                OrderMeta(
                    order=self.order, key='AdditionalPatientHistory',
                    value=str(dicom_file.get('AdditionalPatientHistory'))),
            ])
