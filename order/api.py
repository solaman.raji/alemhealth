from django.views.generic import View
from django.utils.decorators import method_decorator
from django.template.loader import get_template
from django.template import Context
from django.db.models import Q
from django.shortcuts import redirect
from django.core.files import File
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Count
from django.db import connection
from wsgiref.util import FileWrapper
from django.http.response import HttpResponseRedirect
from django.http.response import HttpResponseNotFound

import json
import os.path
import datetime
import xhtml2pdf.pisa as pisa
import twilio
import mandrill
import base64
import os
import requests

from twilio.rest import TwilioRestClient

from alemhealth.utils import *
from alemhealth.decorators import *
from order.models import *
from report.models import ReportModel
from order.tasks import *
from activity.models import *

from django.core.exceptions import ObjectDoesNotExist

logger = logging.getLogger('alemhealth')


def upload_dicom(request):
    guid = request.POST.get('guid')

    try:
        order = Order.objects.get(guid=guid)
    except Order.DoesNotExist:
        return api_error_message("DoesNotExist", "Order")

    try:
        dcm_file = DicomFile()
        dcm_file.order = order
        dcm_file.file = request.FILES.get('file')
        dcm_file.save()

        data = {"code": "ok"}
        if order.meta_status == 0:
            order.meta_status = 1
            order.save()
            data = {
                'status': 'first'
            }
        logger.info(
            request.user.email +
            ' uploaded a new dicom file ' + dcm_file.file.filename +
            ' for order ' + str(order.pk)
        )
        return dict_to_json(data, 200)
    except Exception as e:
        logger.error(str(e))


def get_updated_reports(request):
    facility_guid = request.GET.get('facility_guid')
    after_datetime = request.GET.get('after', False)

    if not after_datetime:
        return api_error_message("ParameterMissing", "after")
    if not facility_guid:
        return api_error_message("ParameterMissing", "facility_guid")

    try:
        # todo check authorization
        datetime.datetime.strptime(after_datetime, "%Y-%m-%d %H:%M:%S")
        result = Order.objects.get_updated_reports(facility_guid, after_datetime, request.build_absolute_uri('/'))
    except ValueError:
        return api_error_message("InvalidParameter", "after")
    except Hospital.DoesNotExist:
        return api_error_message("DoesNotExist", "Facility")

    return HttpResponse(json.dumps(result), status=200, content_type="text/json")


def get_study_for_hub(request):
    result = {}

    provider_guid = request.GET.get('provider_guid')

    if not provider_guid:
        return api_error_message('ParameterMissing', 'provider_guid')

    return HttpResponse(json.dumps(result), status=200, content_type='text/json')


class PatientResources(View):
    def patient_format(self, patient):
        data = {
            "age": patient.age,
            "blood_group": patient.blood_group,
            "city": patient.city,
            "country": patient.country,
            "date_of_birth": patient.date_of_birth,
            "created_at": patient.created_at.strftime("%d %b, %Y :  %H.%M"),
            "gender": patient.gender,
            "guid": str(patient.guid),
            "name": patient.name,
            "orders": [self.order_format(order) for order in patient.orders.all()],
            "phone": patient.phone
        }

        return data

    def order_format(self, order):
        data = {
            'id': order.pk,
            'guid': str(order.guid),
            'modality': order.modality,
            'accession_id': order.accession_id,
            'name': order.patient.name if order.patient else '',
            'phone': order.patient.phone if order.patient else '',
            'age': order.patient.age if order.patient else '',
            'patient_id': order.patient.patient_id if order.patient else '',
            'patient_name': order.patient.name if order.patient else '',
            'created_at': order.created_at.strftime("%d %b, %Y :  %H.%M"),
            'updated_at': order.updated_at.strftime("%a %w %b, %Y :  %H.%M %p"),
            'hospital': order.hospital.name if order.hospital else '',
            'hospital_country': str(order.hospital.country) if order.hospital else '',
            'doctor': order.doctor.name if order.doctor else '',
            'status': order.status,
            'priority': order.priority,
            'status_text': order.get_status_display(),
            'billing_status': order.billing_status,
            'billing_status_text': order.get_billing_status_display(),
            'zipfile': str(order.zipfile) if order.zipfile else None,
            'report_filename': str(order.report_filename) if order.report_filename else None,
            'provider': order.provider.name if order.provider else None,
            'second_provider': order.second_provider.name if order.second_provider else None,
            'price': str(order.receipt.price.unit_price) if order.receipt else None,
            'radiologist': order.radiologist.name if order.radiologist else None,
            'provider_radiologist_name': order.provider_radiologist.name if order.provider_radiologist else None,
            'is_storage': order.is_storage if order.is_storage else None,
        }

        return data


class OrderResource(View):
    def format(self, order):
        data = {
            'id': order.pk,
            'guid': str(order.guid),
            'dicom_uid': str(order.dicom_uid),
            'modality': order.modality,
            'accession_id': order.accession_id,
            'name': order.patient.name if order.patient else '',
            'phone': order.patient.phone if order.patient else '',
            'age': order.patient.age if order.patient else '',
            'patient_id': order.patient.patient_id if order.patient else '',
            'patient_name': order.patient.name if order.patient else '',
            'created_at': order.created_at.strftime("%d %b, %Y :  %H.%M"),
            'updated_at': order.updated_at.strftime("%a %w %b, %Y :  %H.%M %p"),
            'hospital': order.hospital.name if order.hospital else '',
            'hospital_country': str(order.hospital.country) if order.hospital else '',
            'doctor': order.doctor.name if order.doctor else '',
            'status': order.status,
            'priority': order.priority,
            'status_text': order.get_status_display(),
            'billing_status': order.billing_status,
            'billing_status_text': order.get_billing_status_display(),
            'zipfile': str(order.zipfile) if order.zipfile else None,
            'report_filename': str(order.report_filename) if order.report_filename else None,
            'provider': order.provider.name if order.provider else None,
            'second_provider': order.second_provider.name if order.second_provider else None,
            'price': str(order.receipt.price.unit_price) if order.receipt else None,
            'radiologist': order.radiologist.name if order.radiologist else None,
            'provider_radiologist_name': order.provider_radiologist.name if order.provider_radiologist else None,
            'is_storage': order.is_storage if order.is_storage else None,
            'number_of_dicom_images': order.dicoms.count(),
            'metas': {meta.key: meta.value for meta in order.metas.all()},
        }
        return data

    def detail_format(self, order):
        data = {
            'id': order.pk,
            'guid': str(order.guid),
            'dicom_uid': str(order.dicom_uid),
            'accession_id': order.accession_id,
            'modality': order.modality,
            'priority': order.priority,
            'status': order.status,
            'status_text': order.get_status_display(),
            'meta_status': order.meta_status,
            'op_status': order.op_status,
            'billing_status': order.billing_status,
            'zipfile': str(order.zipfile) if order.zipfile else None,
            'metas': {a.key: a.value for a in order.metas.all()},
            'dicoms': [self.dicom_format(a) for a in order.dicoms.all()],
            'report': order.report,
            'report_guid': self.get_report_guid_by_order_guid(order.guid),
            'report_filename': str(order.report_filename) if order.report_filename else None,
            'second_report': order.second_report,
            'second_report_filename': str(order.second_report_filename) if order.second_report_filename else None,
            'hospital': order.hospital_id if order.hospital else None,
            'hospital_guid': order.hospital.guid.hex if order.hospital else None,
            'hospital_type': order.hospital.hospital_type if order.hospital else None,
            'hospital_logo': str(order.hospital.logo.file) if order.hospital and order.hospital.logo else None,
            'provider': order.provider_id if order.provider else None,
            'provider_name': order.provider.name if order.provider else None,
            'provider_logo': str(order.provider.logo.file) if order.provider and order.provider.logo else None,
            'provider_email': order.provider.user.all()[0].email if order.provider else None,
            'provider_radiologist': order.provider_radiologist_id,
            'provider_radiologist_name': order.provider_radiologist.name if order.provider_radiologist else None,
            'second_provider': order.second_provider_id if order.second_provider else None,
            'second_provider_email': order.second_provider.user.all()[0].email if order.second_provider else None,
            'doctor': order.doctor_id if order.doctor else None,
            'doctor_name': order.doctor.name if order.doctor else None,
            'doctor_phone': order.doctor.phone if order.doctor else None,
            'doctor_signature': str(order.doctor_signature) if order.doctor_signature else None,
            'doctor_signatures': [self.dr_signature_format(a) for a in order.doctor_signatures.all()],
            'second_doctor_signatures': [self.dr_signature_format(a) for a in order.second_doctor_signatures.all()],
            'radiologist': order.radiologist_id if order.radiologist else None,
            'radiologist_name': order.radiologist.name if order.radiologist else None,
            'radiologist_signature': str(order.radiologist.signature.file) if order.radiologist and order.radiologist.signature else None,
            'activities': [self.activity_format(a) for a in order.activities.all().order_by('-created_at')],
            'receipt': str(order.receipt.receipt_pdf) if order.receipt else None,
            'dr_code': str(order.dr_code) if order.dr_code else None,
            'referral_code': str(order.referral_code) if order.referral_code else None,
            'created_at': str(order.created_at),
        }

        if order.patient:
            data['patient'] = {
                'name': order.patient.name,
                'patient_id': order.patient.patient_id,
                'date_of_birth': order.patient.date_of_birth,
                'gender': order.patient.gender,
                'phone': order.patient.phone,
                'age': order.patient.age
            }

        return data

    # Get report guid by order guid
    # If order doesn't have a report then return blank report_guid
    def get_report_guid_by_order_guid(self, order_guid):
        report_guid = ""

        try:
            report = ReportModel.objects.get(order_guid=order_guid)
            report_guid = report.guid
        except ObjectDoesNotExist:
            logger.error("get_report_guid_by_order_guid >>> order_guid {0} doesn't have a report".format(
                order_guid
            ))

        return str(report_guid)

    def meta_format(self, meta):
        return {
            meta.key: meta.value
            # 'value': meta.value
        }

    def dicom_format(self, dicom):
        return {
            'file': str(dicom.file),
            'name': os.path.basename(dicom.file.name),
            'guid': str(dicom.guid),
            'annotation_data': dicom.annotation_data
            # 'size' : dicom.file.size if dicom.file else None
        }

    def dr_signature_format(self, signature):
        return {
            'id': signature.pk,
            'signature': str(signature.signature),
        }

    def activity_format(self, activity):
        data = {
            'id': str(activity.pk),
            'guid': str(activity.guid),
            'actor': activity.actor,
            'context': activity.context,
            'target': activity.target,
            'obj': activity.obj,
            'created_at': str(activity.created_at),
            'mark_as_read': 1
        }
        return data

    def receipt_format(self, data):
        if data:
            return {
                'total_price': str(data.total_price),
                'tax_price': str(data.tax_price),
                'total_price_with_tax': str(data.total_price_with_tax)
            }


class OrdersView(OrderResource):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrdersView, self).dispatch(*args, **kwargs)

    @method_decorator(check_accesskey)
    def post(self, request):
        logger.info('Create inactive order for hospital Box with data: {}'.format(request.body))

        json_dict = json.loads(request.body)

        _hospital_id = json_dict.get('hospital_id')
        if _hospital_id:
            try:
                data = {}

                _ah_order_guid = json_dict.get('ah_order_guid')
                if _ah_order_guid:
                    logger.info('BOX-{} POST to update order guid: {}'.format(str(_hospital_id), _ah_order_guid))
                    try:
                        order = Order.objects.get(guid=_ah_order_guid)

                        order.hospital_id = _hospital_id
                        order.priority = json_dict.get('priority')
                        order.doctor_id = json_dict.get('doctor_id')
                        order.is_storage = True if json_dict.get('is_storage') else False

                        provider_id = json_dict.get('provider_id')
                        provider_guid = json_dict.get('provider_guid')

                        if provider_id:
                            order.provider = Provider.objects.get(pk=provider_id)

                        if provider_guid:
                            order.provider = Provider.objects.get(guid=provider_guid)

                        if json_dict.get('active_status'):
                            order.active_status = json_dict.get('active_status')
                        order.accession_id = 'AH-' + str(order.get_modality_display()).zfill(2) + '-' + str(
                            order.hospital.pk).zfill(2) + '-' + str(order.pk).zfill(2)
                        logger.info('Accession id {} is set for order {}'.format(order.accession_id, order.guid))

                        try:

                            if hasattr(order, 'patient') and order.patient.date_of_birth:
                                patient_dob = datetime.datetime.strptime(order.patient.date_of_birth, '%Y%m%d')
                                order.patient.age = str(age(patient_dob)).zfill(3) + 'Y'
                                order.patient.save()

                            if order.provider.default_radiologist:
                                order.provider_radiologist_id = order.provider.default_radiologist

                        except Exception as e:
                            logger.error("AGE-ERROR" + str(e))
                            pass

                        try:
                            order.save()
                            result = send_dicom_to_dcm4che.delay(guid=order.guid)
                            logger.info("{log_prefix} task_id: {task_id}, order_id: {order_id}, order_guid: {order_guid}".format(
                                log_prefix="send dicom to dcm4che >>>",
                                task_id=result.task_id,
                                order_id=order.id,
                                order_guid=order.guid
                            ))
                        except Exception as e:
                            logger.exception('Exception occured during saving order')

                        dicom_count = order.dicoms.count()
                        if dicom_count:
                            logger.info('Trigger Dicom meta write for order guid: {} with {} dicoms'.format(
                                order.guid,
                                str(dicom_count)))
                            dicom_meta_write.delay(guid=order.guid, user=request.user)
                            # dicom_meta_write.delay(guid=order.guid, user=None)
                            logger.info('BOX-{} all {} dicom sync successfully'.format(
                                str(_hospital_id), str(dicom_count)))
                        else:
                            logger.warning('Skip dicom meta-write as dicom count is {} for order {}'.format(
                                str(dicom_count), order.guid
                            ))
                    except Order.DoesNotExist:
                        logger.error('Order not found for guid: {}'.format(_ah_order_guid))
                        return dict_to_json(data, 200)
                else:
                    logger.info('Create a new order from BOX-{}'.format(str(_hospital_id)))
                    order = Order()
                    order.active_status = 0
                    order.hospital_id = _hospital_id
                    order.priority = json_dict.get('priority')
                    order.doctor_id = json_dict.get('doctor_id')
                    order.is_storage = True if json_dict.get('is_storage') else False

                    provider_id = json_dict.get('provider_id')
                    provider_guid = json_dict.get('provider_guid')

                    if provider_id:
                        order.provider = Provider.objects.get(pk=provider_id)

                    if provider_guid:
                        order.provider = Provider.objects.get(guid=provider_guid)

                    order.save()
                    logger.info('BOX-{} has added new order: {}'.format(str(_hospital_id), str(order.guid)))

                data = {
                    'guid': str(order.guid),
                    'active_status': order.active_status,
                    'id': order.pk
                }
            except Exception as e:
                logger.error("exception occurred. reason: " + str(e))
                return dict_to_json({}, 500)
            return dict_to_json(data, 200)

        group_name_list = [a.name for a in request.user.groups.all()]

        order = Order()
        order.operator = request.user

        if 'hospital' in group_name_list:
            order.hospital = request.user.hospitals.get(user=request.user)
            if order.hospital.operators.all().count():
                order.operator = order.hospital.operators.all()[0].user

        order.save()
        logger.info(request.user.email + ' created a new order ' + str(order.pk))

        # add activity
        actor = {
            'type': 'user',
            'name': request.user.first_name + ' ' + request.user.last_name,
            'guid': str(request.user.profile.guid)
        }

        context = 'initiated'

        obj = {
            'type': 'study',
            'name': 'Draft study #' + str(order.pk),
            'guid': str(order.guid)
        }
        listeners = [request.user]

        if order.hospital:
            listeners.append(order.operator)

        create_activity(actor, context, listeners, order=order, obj=obj, user=request.user)

        data = {
            'guid': str(order.guid),
            'status': order.status,
            'id': order.pk
        }
        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def get(self, request):

        page = request.GET.get('page')
        per_page = request.GET.get('per_page')

        q = request.GET.get('q')
        status = request.GET.get('status')
        hospital = request.GET.get('hospital')
        hospital_country = request.GET.get('hospital_country')
        read_status = request.GET.get('read_status')
        priority = request.GET.get('priority')
        froms = request.GET.get('from')
        to = request.GET.get('to')
        modality_search = request.GET.get('modality_search')

        if not per_page:
            post_per_page = 50
        else:
            post_per_page = int(per_page)

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        # order list access wise
        group_name_list = [a.name for a in request.user.groups.all()]

        if 'operator' in group_name_list:
            order_query = Order.objects.filter(
                operator=request.user).order_by('-created_at')
        elif 'hospital' in group_name_list:
            hospital = request.user.hospitals.all()[0]
            order_query = Order.objects.filter(
                hospital=hospital).order_by('-created_at')
        elif 'provider' in group_name_list:
            # hospital = request.user.hospitals.all()[0]
            order_query = Order.objects.filter(
                provider=request.user.providers.all()[0], status__gte=4).order_by('-created_at') | Order.objects.filter(
                second_provider=request.user.providers.all()[0], status__gte=4).order_by('-created_at')

        elif 'doctor' in group_name_list:
            if request.user.doctor.provider:
                #  provider Dr order list
                order_query = Order.objects.filter(
                    provider_radiologist=request.user.doctor).order_by('-created_at')
            elif request.user.doctor.radiologist:
                order_query = Order.objects.filter(
                    radiologist__user=request.user).order_by('-created_at')
            else:
                order_query = Order.objects.filter(
                    doctor__user=request.user).order_by('-created_at')
        else:
            order_query = Order.objects.order_by('-created_at')

        if hospital:
            order_query = order_query.filter(hospital_id=hospital)

        if hospital_country:
            order_query = order_query.filter(hospital__country=hospital_country)

        if froms and to:
            order_query = order_query.filter(created_at__range=[froms, to])

        if modality_search:
            order_query = order_query.filter(modality=modality_search)

        if q:
            order_query = order_query.filter(
                Q(patient__name__icontains=q) |
                Q(patient__gender__icontains=q) |
                Q(patient__age__icontains=q) |
                Q(patient__phone__icontains=q) |
                Q(patient__blood_group__icontains=q) |
                Q(patient__city__icontains=q) |

                Q(doctor__name__icontains=q) |
                Q(doctor__specialty__icontains=q) |
                Q(doctor__email__icontains=q) |
                Q(doctor__phone__icontains=q) |
                Q(doctor__city__icontains=q) |

                Q(hospital__name__icontains=q) |
                Q(hospital__phone__icontains=q) |
                Q(hospital__city__icontains=q) |
                Q(hospital__address__icontains=q) |
                Q(hospital__general_email__icontains=q) |
                Q(hospital__primary_name__icontains=q) |
                Q(hospital__primary_email__icontains=q) |
                Q(hospital__primary_physician_contact__icontains=q) |

                Q(accession_id__icontains=q) |
                Q(modality__icontains=q) |
                Q(speciality__icontains=q) |
                Q(history__icontains=q)
            )
        if status:
            order_query = order_query.filter(status=status)
        # return all active draft
        order_query = order_query.filter(active_status=True)

        # check read status
        if read_status:
            order_query = order_query.filter(read_status=read_status)

        # check priority
        if priority:
            order_query = order_query.filter(priority=priority)

        count = order_query.count()
        data = {'meta': {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }}

        orders = order_query[offset:pagesize]
        data['orders'] = [self.format(a) for a in orders]

        return dict_to_json(data, 200)

class PatientOrders(PatientResources):
    @csrf_exempt
    def dispatch(self, request, *args, **kwargs):
        return super(PatientOrders, self).dispatch(request, *args, **kwargs)

    def post(self, request):

        json_dict = json.loads(request.body)
        print request.body
        phone = str(json_dict.get("phone_no"))

        # return dict_to_json({"phone": phone}, 200)
        data = {}
        # import pdb; pdb.set_trace()

        try:
            patient = Patient.objects.get(phone=phone)
        except Patient.DoesNotExist:
            data['error'] = "Patient Does not exist"
            return dict_to_json(data, 400)

        data = self.patient_format(patient)
        return dict_to_json(data, 200)

    def get(self, phone_no):
        patient_phone = str(phone_no)
        try:
            patient = Patient.objects.get(phone=patient_phone)
            try:
                order = Order.objects.get(patient)
            except Order.DoesNotExist:
                return api_error_message("DoesNotExist", "order")
        except Patient.DoesNotExist:
            return api_error_message("DoesNotExist", "patient")

        data = self.detail_format(order)
        return dict_to_json(data, 200)


class OrderView(OrderResource):
    @method_decorator(check_accesskey)
    def put(self, request, guid):
        json_dict = json.loads(request.body)

        # Add  bulk provider_radiologist
        if json_dict.get('type') == 'bulk_provider_radiologist':
            provider_radiologist_id = json_dict.get('provider_radiologist')
            Order.objects.filter(id__in=json_dict.get('orders')).update(provider_radiologist=provider_radiologist_id)

            data = {}
            return dict_to_json(data, 200)

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message('DoesNotExist', "Order")

        # add second opinion provider
        if json_dict.get('type') == 'second_provider':
            order.second_provider_id = json_dict.get('second_provider')
            order.save()
            data = {}
            data = self.detail_format(order)

            send_dicom_to_2nd_provider.delay(guid=order.guid)

            logger.info(
                request.user.email +
                ' add second provider'
            )

            return dict_to_json(data, 200)

        # Add  provider_radiologist
        if json_dict.get('type') == 'provider_radiologist':

            order.provider_radiologist_id = json_dict.get('provider_radiologist')
            order.save()
            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                ' add provider_radiologist'
            )

            # add activity
            actor = {
                'type': 'user',
                'name': request.user.first_name + ' ' + request.user.last_name,
                'guid': str(request.user.profile.guid)
            }

            obj = {
                'type': 'study',
                'name': str(order.accession_id),
                'guid': str(order.guid)
            }

            if order.accession_id:
                context = "" + order.accession_id + " has been assigned to " + order.provider_radiologist.name
            else:
                context = "" + order.pk + " has been assigned to " + order.provider_radiologist.name

            listeners = [order.provider.user.all()[0], order.provider_radiologist.user, order.operator,
                         order.hospital.user.all()[0]]

            create_activity(
                actor,
                context,
                listeners,
                obj=obj,
                order=order,
                user=request.user
            )

            return dict_to_json(data, 200)



            # update bill status

        if json_dict.get('type') == 'billing_status':
            order.billing_status = json_dict.get('billing_status')
            order.save()
            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                ' updated billing status : '
                + order.get_billing_status_display()
            )

            # add activity
            actor = {
                'type': 'user',
                'name': request.user.first_name + ' ' + request.user.last_name,
                'guid': str(request.user.profile.guid)
            }

            obj = {
                'type': 'study',
                'name': str(order.accession_id),
                'guid': str(order.guid)
            }

            context = 'updated billing to "' + order.get_billing_status_display() + '" for'

            listeners = [order.operator, order.hospital.user.all()[0]]

            create_activity(
                actor,
                context,
                listeners,
                obj=obj,
                order=order,
                user=request.user
            )

            return dict_to_json(data, 200)

        # update report from Viewer
        if json_dict.get('type') == 'viewer_report_update':
            order.report = json_dict.get('report')
            order.save()
            data = {}
            data = self.detail_format(order)

            return dict_to_json(data, 200)

        # update report only
        if json_dict.get('type') == 'report_update':
            order.report = json_dict.get('report')
            order.status = json_dict.get('status')
            order.op_status = json_dict.get('op_status')

            # order.save()

            logger.info(
                request.user.email +
                ' generating PDF report for order: '
                + str(order.pk)
            )

            if not order.receipt and order.hospital.currency:
                # create receipt
                try:
                    price = Price.objects.get(
                        provider=order.provider,
                        priority=order.priority,
                        modality=order.modality
                    )
                    receipt = Receipt()
                    receipt.price = price
                    if order.hospital.currency.code != 'USD':
                        receipt.total_price = price.unit_price * order.hospital.currency.rate
                    else:
                        receipt.total_price = price.unit_price
                    receipt.tax_price = (order.hospital.currency.tax * receipt.total_price) / 100
                    receipt.total_price_with_tax = receipt.tax_price + receipt.total_price

                    receipt.save()

                    order.receipt = receipt
                except Price.DoesNotExist:
                    pass

            if order.receipt:
                create_receipt(order)

            pdf_generate(order)

            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                ' updated report '
            )

            # add activity
            actor = {
                'type': 'user',
                'name': request.user.first_name + ' ' + request.user.last_name,
                'guid': str(request.user.profile.guid)
            }

            obj = {
                'type': 'study',
                'name': str(order.accession_id),
                'guid': str(order.guid)
            }

            context = 'created report for'

            listeners = [order.operator, order.hospital.user.all()[0]]
            if order.radiologist:
                listeners.append(order.radiologist.user)

            create_activity(
                actor,
                context,
                listeners,
                obj=obj,
                order=order,
                user=request.user
            )

            return dict_to_json(data, 200)

        # second update report only
        if json_dict.get('type') == 'second_report_update':
            order.second_report = json_dict.get('second_report')
            order.status = json_dict.get('status')
            order.op_status = json_dict.get('op_status')

            # order.save()

            logger.info(
                request.user.email +
                ' generating PDF report for order: '
                + str(order.pk)
            )

            # if not order.receipt and order.hospital.currency:
            #     # create receipt
            #     try:
            #         price = Price.objects.get(
            #             provider=order.provider,
            #             priority=order.priority,
            #             modality=order.modality
            #         )
            #         receipt = Receipt()
            #         receipt.price = price
            #         if order.hospital.currency.code != 'USD':
            #             receipt.total_price = price.unit_price * order.hospital.currency.rate
            #         else:
            #             receipt.total_price = price.unit_price
            #         receipt.tax_price = (order.hospital.currency.tax * receipt.total_price) / 100
            #         receipt.total_price_with_tax = receipt.tax_price + receipt.total_price
            #
            #         receipt.save()
            #
            #         order.receipt = receipt
            #     except Price.DoesNotExist:
            #         pass
            #
            # if order.receipt:
            #     create_receipt(order)

            second_report_pdf_generate(order)

            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                ' updated report '
            )

            # add activity
            actor = {
                'type': 'user',
                'name': request.user.first_name + ' ' + request.user.last_name,
                'guid': str(request.user.profile.guid)
            }

            obj = {
                'type': 'study',
                'name': str(order.accession_id),
                'guid': str(order.guid)
            }

            context = 'created report for'

            listeners = [order.operator, order.hospital.user.all()[0]]
            if order.radiologist:
                listeners.append(order.radiologist.user)

            create_activity(
                actor,
                context,
                listeners,
                obj=obj,
                order=order,
                user=request.user
            )

            return dict_to_json(data, 200)

        # update report delivered
        if json_dict.get('type') == 'report_delivered':
            order.status = json_dict.get('status')
            order.save()
            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                ' updated report delivered status '
            )

            return dict_to_json(data, 200)

        # start dicom routing
        if json_dict.get('type') == 'start_routing' and order.provider:
            order.status = json_dict.get('status')
            order.save()
            data = {}
            data = self.detail_format(order)

            logger.info(
                request.user.email +
                'routed'
                + order.provider.name
            )

            # add activity
            actor = {
                'type': 'user',
                'name': request.user.first_name + ' ' + request.user.last_name,
                'guid': str(request.user.profile.guid)
            }

            obj = {
                'type': 'study',
                'name': str(order.accession_id),
                'guid': str(order.guid)
            }

            context = 'start routing'

            target = {
                'prefix': 'to',
                'type': 'provider',
                'name': order.provider.name,
                # 'guid': str(order.provider.user.profile.guid)
            }

            listeners = [order.operator, order.hospital.user.all()[0]]

            create_activity(
                actor,
                context,
                listeners,
                obj=obj,
                order=order,
                user=request.user,
                target=target
            )

            return dict_to_json(data, 200)

        if json_dict.get('hospital'):
            try:
                hospital = Hospital.objects.get(pk=json_dict.get('hospital'))
                order.hospital = hospital
            except Hospital.DoesNotExist:
                pass

        if json_dict.get('doctor'):
            try:
                doctor = Doctor.objects.get(pk=json_dict.get('doctor'))
                order.doctor = doctor
            except Doctor.DoesNotExist:
                pass

        if json_dict.get('provider'):
            try:
                provider = Provider.objects.get(pk=json_dict.get('provider'))
                if provider.default_radiologist:
                    order.provider_radiologist_id = provider.default_radiologist
                order.provider = provider
            except Provider.DoesNotExist:
                pass
        else:
            order.provider = None

        if json_dict.get('radiologist'):
            try:
                radiologist = Doctor.objects.get(pk=json_dict.get('radiologist'))
                order.radiologist = radiologist
            except Doctor.DoesNotExist:
                pass
        else:
            order.radiologist = None

        # add dr code and referral code
        if json_dict.get('dr_code'):
            order.dr_code = json_dict.get('dr_code')
        if json_dict.get('referral_code'):
            order.referral_code = json_dict.get('referral_code')

        # add a doctor if not assigned with Dr code
        if order.dr_code and not order.doctor:
            try:
                doctor = Doctor.objects.get(referral_code=order.dr_code)
                order.doctor = doctor
            except Doctor.DoesNotExist:
                pass

        order.modality = json_dict.get('modality')
        order.priority = json_dict.get('priority')
        order.status = 1
        order.meta_status = 1

        accession_id_template = '{prefix}-{modality_display}-{hospital_id}-{order_id}'
        order.accession_id = accession_id_template.format(prefix='AH',
                                                          modality_display=str(order.get_modality_display()).zfill(2),
                                                          hospital_id=str(hospital.pk).zfill(2),
                                                          order_id=str(order.pk).zfill(2))
        order.save()
        result = send_dicom_to_dcm4che.delay(guid=order.guid)
        logger.info("{log_prefix} task_id: {task_id}, order_id: {order_id}, order_guid: {order_guid}".format(
            log_prefix="send dicom to dcm4che >>>",
            task_id=result.task_id,
            order_id=order.id,
            order_guid=order.guid
        ))

        if json_dict.get('metas'):
            for meta in json_dict.get('metas'):
                try:
                    order_meta = OrderMeta.objects.get(
                        order=order,
                        key=meta
                    )

                    order_meta.value = json_dict.get('metas').get(meta)
                    order_meta.save()
                except OrderMeta.DoesNotExist:
                    pass

        data = self.detail_format(order)

        # save patient

        if json_dict.get('patient'):
            order.patient.name = json_dict.get('patient')['name']
            # order.patient.patient_id = json_dict.get('patient')['patient_id']
            order.patient.gender = json_dict.get('patient')['gender']
            order.patient.date_of_birth = json_dict.get('patient')['date_of_birth']
            order.patient.phone = json_dict.get('patient')['phone']

            patient_dob = datetime.datetime.strptime(order.patient.date_of_birth, '%Y%m%d')
            order.patient.age = str(age(patient_dob)).zfill(3) + 'Y'
            order.patient.save()

        # add celery task for edit meta
        count = str(order.dicoms.count())
        logger.info('Trigger Dicom meta write for order guid: {} for {} dicoms in Order PUT request'.format(order.guid, count))
        dicom_meta_write.delay(guid=order.guid, user=request.user)
        # logger.error('Order : ' + str(order.pk) + ' Can\'t start task ' + str(e) )
        # dicom_meta_write(order.guid)

        # logger

        logger.info(
            request.user.email +
            ' edited order '
            + str(order.pk)
        )

        # add activity

        actor = {
            'type': 'user',
            'name': request.user.first_name + ' ' + request.user.last_name,
            'guid': str(request.user.profile.guid)
        }

        obj = {
            'type': 'study',
            'name': str(order.accession_id),
            'guid': str(order.guid)
        }

        context = 'updated Study Details of'
        listeners = [order.operator, order.hospital.user.all()[0]]

        if order.provider:
            listeners.append(order.provider.user.all()[0])

        create_activity(
            actor,
            context,
            listeners,
            obj=obj,
            order=order,
            user=request.user
        )

        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def get(self, request, guid):

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "order")

        data = self.detail_format(order)
        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def delete(self, request, guid):

        try:
            order = Order.objects.get(guid=guid)
            id = order.pk
            order.delete()
            logger.info(
                request.user.email +
                ' deleted order : '
                + str(id)
            )

        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", 'Order')

        return api_error_message("DeleteSuccessfully", "Order")


# for testing the reports
class testView(OrderResource):
    def get(self, request):

        try:
            id = request.GET.get("id")
            order = Order.objects.get(id=id)

            pdf_generate(order)

        except:
            return api_error_message("DoesNotExist", 'Order')
        return dict_to_json({}, 200)


def pdf_generate(order):
    print("Order to pdf", order)
    data = {}
    data['order'] = order
    data['settings'] = settings
    data['create_time'] = str(datetime.date.today())

    # Render html content through html template with context
    # check if there is a template for a particular hospital
    if settings.AH_ENV == 'dev':
        folder_name = 'dev'
    else:
        folder_name = 'pro'

    try:
        template = get_template('report/' + folder_name + '/' + order.hospital.short_code.lower() + '.html')
    except:
        template = get_template('report/' + folder_name + '/order.html')

    html = template.render(data)

    filename = str(order.pk) + '_' + slugify(order.accession_id) + '_' + '_' + slugify(str(datetime.date.today()))
    filename = filename[:120]
    filename = filename + '.pdf'

    tmp_filename = '/tmp/alemhealth/' + filename
    # Write PDF to file
    tmp_file = open(tmp_filename, "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

    if pisaStatus:
        order.report_file_updated_at = datetime.datetime.utcnow().replace(tzinfo=utc)
        order.report_filename.save(filename, File(tmp_file))
        # order.save()

        if os.path.isfile(tmp_filename):
            os.unlink(tmp_filename)

    tmp_file.close()

    return pisaStatus
    # # Return PDF document through a Django HTTP response
    # file.seek(0)
    # pdf = file.read()
    # file.close()            # Don't forget to close the file handle
    # return HttpResponse(pdf, mimetype='application/pdf')


def second_report_pdf_generate(order):
    print("Order to pdf", order)
    data = {}
    data['order'] = order
    data['settings'] = settings
    data['create_time'] = str(datetime.date.today())
    # Render html content through html template with context
    if settings.AH_ENV == 'dev':
        template = get_template('report/second-order.html')
    else:
        template = get_template('report/prod-second-order.html')
    html = template.render(data)

    filename = str(order.pk) + '_' + slugify(order.accession_id) + '_' + '_' + slugify(str(datetime.date.today()))
    filename = filename[:120]
    filename = filename + '.pdf'

    tmp_filename = '/tmp/alemhealth/' + filename
    # Write PDF to file
    tmp_file = open(tmp_filename, "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

    if pisaStatus:
        order.second_report_filename.save(filename, File(tmp_file))
        # order.save()

        if os.path.isfile(tmp_filename):
            os.unlink(tmp_filename)

    tmp_file.close()

    return pisaStatus


def create_receipt(order):
    # import pdb; pdb.set_trace()
    data = {}
    data['order'] = order
    data['settings'] = settings
    data['create_time'] = str(datetime.date.today())
    # Render html content through html template with context
    template = get_template('report/receipt.html')
    html = template.render(data)

    filename = 'receipt-' + slugify(order.accession_id)
    filename = filename[:120]
    filename = filename + '.pdf'

    tmp_filename = '/tmp/alemhealth/' + filename
    # Write PDF to file
    tmp_file = open(tmp_filename, "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

    if pisaStatus:
        order.receipt.receipt_pdf.save(filename, File(tmp_file))
        # order.save()

        if os.path.isfile(tmp_filename):
            os.unlink(tmp_filename)

    tmp_file.close()

    return pisaStatus


class DicomResource(View):
    def format(self, dicom):
        return {
            'id': dicom.id,
            'guid': str(dicom.guid),
            'filename': dicom.file.name
        }


class DicomsView(DicomResource):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DicomsView, self).dispatch(*args, **kwargs)

    @method_decorator(check_accesskey)
    def post(self, request):
        from django.core.files import File

        guid = request.GET.get('guid')

        try:
            order = Order.objects.get(guid=guid)
            logger.info('Dicom file is uploading for order guid: {}, current patient: {}, status: {}'.format(
                order.guid, order.patient.name if order.patient else 'XXXX', order.meta_status))
            data = {}

            if (request.GET.get('compress')):  # getting compressed file

                requested_file = request.FILES['file']

                path = '/tmp/' + str(guid) + '/'

                os.mkdir(path)

                logger.info("path of the file to decompress :" + path)

                try:
                    with open(path + str(requested_file), 'wb+') as destination:
                        for chunk in requested_file.chunks():
                            destination.write(chunk)
                except Exception as e:
                    print str(e)
                    pass

                """  decompression starts   """
                logger.info(os.system("cd " + path + ";pwd;zpaq x " + str(requested_file)))

                os.remove(path + str(requested_file))
                """  decompression ends """

                logger.info("Done decompressing")

                try:

                    dcm_file = DicomFile()
                    dcm_file.order = order

                    destination_filename = path + os.path.splitext(str(requested_file))[0] + '.dcm'

                    reopen = open(destination_filename)
                    destination_file = File(reopen)

                    # dcm_file.file.save('test.dcm', file_content)

                    dcm_file.file = destination_file
                    dcm_file.save()

                    # changing the meta status :)
                    if order.meta_status == 0:
                        order.meta_status = 1
                        order.save()

                    os.remove(destination_filename)
                    os.removedirs(path)

                    logger.info("Successfully Recived zpaq file")


                except Exception as e:
                    logger.info("Error :    " + str(e))
                    pass
            else:  # without compressed
                dcm_file = DicomFile()
                dcm_file.order = order
                dcm_file.file = request.FILES.get('file')
                dcm_file.save()

                logger.info("Successfully recived dicom files")
                logger.info('Dicom file is uploading for order guid: {}, current patient: {}, status: {}'.format(
                    order.guid, order.patient.name if order.patient else 'XXXX', order.meta_status))


            # os.remove(destination_filename)
            # os.removedirs(path)


            if order.meta_status == 0:
                order.meta_status = 1
                order.save()

            data = self.format(dcm_file)
            return dict_to_json(data, 200)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")
        except Exception as e:
            print str(e)
            return api_error_message('IntegrityError', str(e))

    @method_decorator(check_accesskey)
    def delete(self, request):
        data = {}
        guid = request.GET.get('ah_order_guid')
        logger.info('Delete dicoms request for order guid: {}'.format(guid))
        if guid:
            try:
                order = Order.objects.get(guid=guid)
                order.active_status = 0
                order.meta_status = 0
                order.zipfile.delete()
                logger.info("Order: " + str(order.guid) + " - Zipfile deleted")
                order.save()
                logger.info('Delete dicoms request for order guid: {} will clean up dicoms'.format(guid))
                try:
                    if settings.USE_S3:
                        for dicoms_obj in order.dicoms.all():
                            dicoms_obj.file.delete()
                        logger.info("dicoms delete from s3 bucket for order: " + str(order.id))
                        DicomFile.objects.filter(order=order.id).delete()
                        logger.info("dicoms info for order: " + str(order.id) + " deleted from DB")
                        OrderMeta.objects.filter(order=order.id).delete()
                    data["success"] = "Order: " + str(order.id) + " deleted"
                    return dict_to_json(data, 200)
                except Exception as err:
                    return api_error_message('IntegrityError', str(err))
                finally:
                    logger.info('Delete dicoms for order guid: {} has {} dicoms and meta_status {}'.format(
                        guid, order.dicoms.count(), order.meta_status))
            except Order.DoesNotExist:
                return api_error_message("DoesNotExist", "Order")


class DicomView(DicomResource):
    def put(self, request, guid):

        json_dict = json.loads(request.body)

        try:
            dicom = DicomFile.objects.get(guid=guid)
            dicom.annotation_data = json_dict.get('annotation_data')
            dicom.save()

            data = self.format(dicom)

        except DicomFile.DoesNotExist:
            return api_error_message("DoesNotExist", "Dicom")

        return dict_to_json(data, 200)


class DicomFileUpload(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):
        guid = request.POST.get('guid')
        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")
        # if request.FILES.get('file').content_type == 'application/dicom':
        dcm_file = DicomFile()
        dcm_file.order = order
        dcm_file.file = request.FILES.get('file')

        if dcm_file.file.name.endswith('.dcm'):
            dcm_file.save()
        else:
            return api_error_message("IntegrityError", "invalid file format")

        if order.meta_status == 0 and order.dicoms.all().count() > 0:
            order.meta_status = 1
            order.save()
            data = self.detail_format(order)
        else:
            data = {}

        return dict_to_json(data, 200)


class DoctorSinatureResource(View):
    def format(self, signature):
        return {
            'id': signature.pk,
            'signature': str(signature.signature)
        }


class DoctorSignaturesView(DoctorSinatureResource):
    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DoctorSignaturesView, self).dispatch(*args, **kwargs)

    @method_decorator(check_accesskey)
    def post(self, request):
        guid = request.POST.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        doctor_signature = DoctorSignature()
        doctor_signature.signature = request.FILES.get('file')
        doctor_signature.order = order
        doctor_signature.save()

        data = {}
        data = self.format(doctor_signature)

        return dict_to_json(data, 200)


class DoctorSignatureView(DoctorSinatureResource):
    @method_decorator(check_accesskey)
    def delete(self, request, id):

        guid = request.GET.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")
        try:
            doctor_signature = DoctorSignature.objects.get(order=order, pk=id)
            doctor_signature.signature.delete()
            doctor_signature.delete()
        except DoctorSignature.DoesNotExist:
            return api_error_message("DoesNotExist", "Doctor Signature")

        data = {}
        return dict_to_json(data, 200)


class SecondDoctorSignaturesView(DoctorSinatureResource):
    @method_decorator(check_accesskey)
    def post(self, request):
        guid = request.POST.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        doctor_signature = SecondDoctorSignature()
        doctor_signature.signature = request.FILES.get('file')
        doctor_signature.order = order
        doctor_signature.save()

        data = {}
        data = self.format(doctor_signature)

        return dict_to_json(data, 200)


class SecondDoctorSignatureView(DoctorSinatureResource):
    @method_decorator(check_accesskey)
    def delete(self, request, id):

        guid = request.GET.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")
        try:
            doctor_signature = SecondDoctorSignature.objects.get(order=order, pk=id)
            doctor_signature.signature.delete()
            doctor_signature.delete()
        except DoctorSignature.DoesNotExist:
            return api_error_message("DoesNotExist", "Second Doctor Signature")

        data = {}
        return dict_to_json(data, 200)


class OrderReportResource(View):
    def modality_format(self, report):
        return {
            'modality': report['modality'],
            'count': report['modality__count']
        }

    def hospital_format(self, report):
        return {
            'hospital': report['hospital__name'],
            'count': report['hospital__count']
        }

    def turn_around_time_format(self, report):
        return {
            'date': report[0],
            'avg': float(report[1])
        }

    def total_order_format(self, report):
        return {
            'date': report[0],
            'count': float(report[1])
        }


class OrdersReportView(OrderReportResource):
    @method_decorator(check_accesskey)
    def get(self, request):
        report_type = request.GET.get('type')
        start_date = request.GET.get('start_date')
        end_date = request.GET.get('end_date')

        group_name_list = [a.name for a in request.user.groups.all()]

        data = {}

        # for provider
        if 'provider' in group_name_list:
            provider_id = str(request.user.providers.all()[0].id)

            if report_type == 'modality':
                if start_date:
                    orders = Order.objects.exclude(
                        modality=None).filter(provider=request.user.providers.all()[0], status__gte=4,
                                              created_at__range=[start_date, end_date]).values('modality').annotate(
                        Count('modality'))
                else:
                    orders = Order.objects.exclude(
                        modality=None).values('modality').annotate(Count('modality'))

                data['reports'] = [self.modality_format(a) for a in orders]

            if report_type == 'hospital':
                if start_date:
                    orders = Order.objects.exclude(
                        hospital=None).filter(provider=request.user.providers.all()[0],
                                              created_at__range=[start_date, end_date]).values(
                        'hospital__name').annotate(Count('hospital'))
                else:
                    orders = Order.objects.exclude(
                        hospital=None).filter(provider=request.user.providers.all()[0]).values(
                        'hospital__name').annotate(Count('hospital'))

                data['reports'] = [self.hospital_format(a) for a in orders]

            if report_type == 'turn_around_time':
                cursor = connection.cursor()

                if start_date:
                    sql = 'select DATE_FORMAT(created_at, "%Y-%b-%d") , avg(report_time) from orders where provider_id = ' + provider_id + ' and report_time !="" and created_at between "' + start_date + '" and "' + end_date + '" GROUP BY  DATE_FORMAT(created_at, "%Y%m%d")'
                else:
                    sql = "select DATE_FORMAT(created_at, '%Y-%b-%d') , avg(report_time) from orders where provider_id = " + provider_id + " and report_time !='' and created_at >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY  DATE_FORMAT(created_at, '%Y%m%d')"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data['reports'] = [self.turn_around_time_format(a) for a in orders]

            if report_type == 'total_order':
                cursor = connection.cursor()

                if start_date:
                    sql = 'select DATE_FORMAT(created_at, "%Y-%b-%d") , count(*) from orders  where provider_id = ' + provider_id + ' and created_at between "' + start_date + '" and "' + end_date + '" and provider_id = ' + provider_id + ' GROUP BY  DATE_FORMAT(created_at, "%Y%m%d")'
                else:
                    sql = "select DATE_FORMAT(created_at, '%Y-%b-%d') , count(*) from orders  where provider_id = " + provider_id + " and created_at >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY  DATE_FORMAT(created_at, '%Y%m%d')"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data['reports'] = [self.total_order_format(a) for a in orders]



        # for admin
        elif 'admin' in group_name_list:
            if report_type == 'modality':
                if start_date:
                    orders = Order.objects.exclude(
                        modality=None).filter(created_at__range=[start_date, end_date]).values('modality').annotate(
                        Count('modality'))
                else:
                    orders = Order.objects.exclude(
                        modality=None).values('modality').annotate(Count('modality'))

                data['reports'] = [self.modality_format(a) for a in orders]

            if report_type == 'hospital':
                if start_date:
                    orders = Order.objects.exclude(
                        hospital=None).filter(created_at__range=[start_date, end_date]).values(
                        'hospital__name').annotate(Count('hospital'))
                else:
                    orders = Order.objects.exclude(
                        hospital=None).values('hospital__name').annotate(Count('hospital'))

                data['reports'] = [self.hospital_format(a) for a in orders]

            if report_type == 'turn_around_time':
                cursor = connection.cursor()
                if start_date:
                    sql = 'select DATE_FORMAT(created_at, "%Y-%b-%d") , avg(report_time) from orders  where report_time !="" and created_at between "' + start_date + '" and "' + end_date + '" GROUP BY  DATE_FORMAT(created_at, "%Y%m%d")'
                else:
                    sql = "select DATE_FORMAT(created_at, '%Y-%b-%d') , avg(report_time) from orders  where report_time !='' and created_at >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY  DATE_FORMAT(created_at, '%Y%m%d')"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data['reports'] = [self.turn_around_time_format(a) for a in orders]

            if report_type == 'total_order':
                cursor = connection.cursor()
                if start_date:
                    sql = 'select DATE_FORMAT(created_at, "%Y-%b-%d") , count(*) from orders  where  created_at between "' + start_date + '" and "' + end_date + '" GROUP BY  DATE_FORMAT(created_at, "%Y%m%d")'
                else:
                    sql = "select DATE_FORMAT(created_at, '%Y-%b-%d') , count(*) from orders  where  created_at >= DATE(NOW()) - INTERVAL 7 DAY GROUP BY  DATE_FORMAT(created_at, '%Y%m%d')"
                cursor.execute(sql)
                orders = cursor.fetchall()
                data['reports'] = [self.total_order_format(a) for a in orders]
                # update orders set report_time = TIMESTAMPDIFF(MINUTE, created_at, updated_at) / 60

        return dict_to_json(data, 200)


class DoctorFileUpload(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        guid = request.POST.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        if order.doctor_signature:
            order.doctor_signature.delete()
            # if settings.USE_S3:

            # else:
            #     file_path = str(order.doctor_signature.file)
            #     if os.path.isfile(file_path):
            #             os.unlink(file_path)

        order.doctor_signature = request.FILES.get('file')
        order.save()

        data = {}
        data = self.detail_format(order)

        return dict_to_json(data, 200)


class SendSMS(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):

        json_dict = json.loads(request.body)
        client = TwilioRestClient(settings.TWILIO_ACCOUNT_SID, settings.TWILIO_AUTH_TOKEN)

        try:
            message = client.messages.create(to=json_dict.get('phone'), from_="+13232444448",
                                             body=json_dict.get('message'))

        except twilio.TwilioRestException as e:
            return dict_to_json({'error': e.msg}, 501)

        return dict_to_json({}, 200)


class SendEmail(OrderResource):
    @method_decorator(check_accesskey)
    def post(self, request):
        json_dict = json.loads(request.body)
        guid = json_dict.get('guid')

        try:
            order = Order.objects.get(guid=guid)
        except Order.DoesNotExist:
            return api_error_message("DoesNotExist", "Order")

        if json_dict.get('email'):
            email = json_dict.get('email')
        else:
            email = order.hospital.general_email

        try:
            # import pdb; pdb.set_trace()
            mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)
            # report_file = settings.MEDIA_ROOT + '/' + 'report-' + str(order.pk) + '.pdf'
            report_file = order.report_filename.read()
            filename = 'Report-' + str(order.pk)
            # send email to sign up user
            message = {
                'from_email': 'team@alemhealth.com',
                'from_name': 'Alemhealth team',
                'headers': {'Reply-To': 'message.reply@example.com'},
                'html': "Dear " + order.hospital.name + ",<br/>"
                                                        "Attached is a report of " + order.modality + " of " + order.patient.name + " submitted on " + order.created_at.strftime(
                    "%d %b, %Y :  %H.%M") + ".<br/> "
                                            "Alemhealth Office.",

                'important': True,
                'subject': 'Report - ' + str(order.pk),
                'tags': ['password-resets'],
                'to': [{'email': email, 'name': order.hospital.name}],
                'attachments': [{
                    'type': 'application/pdf',
                    'name': filename,
                    'content': base64.b64encode(report_file)
                }]
            }
            result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool', send_at='')

            if result[0]['status'] != "sent" and result[0]['status'] != "queued":
                code = 501
                return dict_to_json({'error': 'Somethings goes wrong, Please try again later.'}, 501)

        except mandrill.Error, e:
            return dict_to_json({'error': e.msg}, 501)

        return dict_to_json({}, 200)


from collections import namedtuple


def namedtuplefetchall(cursor):
    "Return all rows from a cursor as a namedtuple"
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]


class ReportResourceFormat(View):
    def order_format(self, report):
        return {
            # 'report' : report['report_filename'],
            'doctor': report['doctor__name'],
            # 'speaciality' : report['doctor__speciality'],
            'city': report['doctor__city'],
            'country': report['doctor__country'],
            'phone': report['doctor__phone'],
            'count': report['id__count']

        }

    def gender_breakdown_format(self, report):
        return {
            'month': str(report.month),
            'male': int(report.Male),
            'female': int(report.Female)
        }

    def hospital_stduy_format(self, report):
        return {
            'hospital': report['hospital__name'],
            'city': report['hospital__city'],
            'country': report['hospital__country'],
            'phone': report['hospital__phone'],
            'count': report['id__count']
        }

    def modality_study_format(self, report):
        return {
            'modality': report['modality'],
            'count': report['id__count']
        }


class AllReportsView(ReportResourceFormat):
    @method_decorator(check_accesskey)
    def get(self, request):

        today = datetime.date.today()
        weekago = today - datetime.timedelta(days=7)

        if request.GET.get('type'):

            report_type = request.GET.get('type')
            start_date = request.GET.get('start_date')
            end_date = request.GET.get('end_date')
            year_select = request.GET.get('year_select')
            print(year_select);
            # doctor_id = request.GET.get('reffered_doctor')
            # print("req data", request)
            # import pdb; pdb.set_trace()
            data = {}
            if report_type == 'reportlist' and start_date == '':
                orders = Order.objects.filter(created_at__range=[weekago, today]).values('doctor__name', 'doctor__city',
                                                                                         'doctor__country',
                                                                                         'doctor__phone').annotate(
                    Count('id')).exclude(doctor_id=None).select_related()
                data['reports'] = [self.order_format(a) for a in orders]
            elif report_type == 'reportlist':
                orders = Order.objects.filter(created_at__range=[start_date, end_date]).values('doctor__name',
                                                                                               'doctor__city',
                                                                                               'doctor__country',
                                                                                               'doctor__phone').annotate(
                    Count('id')).exclude(doctor_id=None).select_related()
                data['reports'] = [self.order_format(a) for a in orders]


            elif report_type == 'hospitalstudy' and start_date == '':
                orders = Order.objects.filter(created_at__range=[weekago, today]).values('hospital__name',
                                                                                         'hospital__city',
                                                                                         'hospital__country',
                                                                                         'hospital__phone').annotate(
                    Count('id')).exclude(hospital_id=None).select_related()
                data['reports'] = [self.hospital_stduy_format(a) for a in orders]

            elif report_type == 'hospitalstudy':
                orders = Order.objects.filter(created_at__range=[start_date, end_date]).values('hospital__name',
                                                                                               'hospital__city',
                                                                                               'hospital__country',
                                                                                               'hospital__phone').annotate(
                    Count('id')).exclude(hospital_id=None).select_related()
                data['reports'] = [self.hospital_stduy_format(a) for a in orders]

            elif report_type == 'modalitystudy':
                orders = Order.objects.filter(created_at__range=[start_date, end_date]).values('modality').annotate(
                    Count('id')).exclude(modality=None).select_related()
                data['reports'] = [self.modality_study_format(a) for a in orders]

            elif report_type == 'modalitystudy' and start_date == '':
                orders = Order.objects.filter(created_at__range=[weekago, today]).values('modality').annotate(
                    Count('id')).exclude(modality=None).select_related()
                data['reports'] = [self.modality_study_format(a) for a in orders]


            elif report_type == 'gender_breakdown':

                # cursor = connection.cursor()
                if year_select:
                    # truncate_date = connection[Order.objects.db].ops.date_trunc_sql('month', 'created_at')
                    # orders = Order.objects.extra({'month':truncate_date})
                    # report = orders.filter(created_at__year=year_select).values('month', 'patient__gender')

                    query = 'SELECT MONTHNAME(created_at) AS month, sum(CASE WHEN gender="M" THEN 1 ELSE 0 END) AS Male, sum(CASE WHEN gender="F" THEN 1 ELSE 0 END) AS Female FROM patients WHERE YEAR(created_at)=' + str(
                        year_select) + ' GROUP BY MONTH(created_at), MONTHNAME(created_at)'
                    # orders = Order.objects.filter(created_at__year=year_select).raw(query)

                    cursor = connection.cursor()
                    cursor.execute(query)
                    results = namedtuplefetchall(cursor)
                    data['reports'] = [self.gender_breakdown_format(a) for a in results]

                    #    sum(CASE WHEN gender='M' THEN 1 ELSE 0 END) AS Male,
                    #    sum(CASE WHEN gender='F' THEN 1 ELSE 0 END) AS Female
                    #    FROM patients
                    #    WHERE YEAR(created_at) = %s
                    #    GROUP BY MONTH(created_at)''',[year])

                    # print data



            else:
                orders = Order.objects.select_related().exclude(doctor_id=None).values('doctor_id').annotate(
                    Count('id'))

            # data['reports'] = [self.gender_breakdown_format(a) for a in orders]
            # print data

            return dict_to_json(data, 200)

        return api_error_message("IntegrityError", "Report")


class StudyForHubResource(View):
    def order_details(self, order):
        return {
            'order_id': order.pk,
            'guid': str(order.guid),
            'modality': order.modality,
            'accession_id': order.accession_id,
            'metas': {a.key: a.value for a in order.metas.all()},

            'hospital_id': order.hospital_id if order.hospital else None,
            'hospital_type': order.hospital.hospital_type if order.hospital else None,
            'hospital_name': order.hospital.name if order.hospital else None,
            'hospital_logo': str(order.hospital.logo.file) if order.hospital else None,

            'doctor_name': order.doctor.name if order.doctor else None,
            'doctor': order.doctor_id if order.doctor else None,
            'doctor_phone': order.doctor.phone if order.doctor else None,
            'status': order.status,
            'meta_status': order.meta_status,
            'status_text': order.get_status_display(),
            'report': order.report,
            'created_at': str(order.created_at),

            'patient_phone': order.patient.phone if order.patient else '',
            'patient_name': order.patient.name if order.patient else '',
            'patient_gender': order.patient.gender if order.patient else '',
            'patient_id': order.patient.id if order.patient else '',

            'op_status': order.op_status,

            'priority': order.priority,
            'zipfile': str(order.zipfile) if order.zipfile else None,
            'billing_status': order.billing_status,

            'dr_code': str(order.dr_code) if order.dr_code else None,
            'referral_code': str(order.referral_code) if order.referral_code else None,
            'provider_radiologist': order.provider_radiologist_id,
        }

    def sort_dicom(self, dicom):
        return {
            'dicom_guid': str(dicom.guid),
            'dicom_filename': str(dicom.file)
        }


class StudyForHubView(StudyForHubResource):
    def get(self, request):
        data = {}

        try:
            provider_guid = request.GET.get('provider_guid')

            logger.info("Recving request from provider hub")

            if (provider_guid):
                try:
                    orders = Order.objects.filter(
                        provider__guid=provider_guid, status=4, hub_sync=0).order_by('-created_at')

                    if len(orders) > 0:
                        data['orders'] = {}

                        for order in orders:
                            res = DicomFile.objects.filter(order__guid=order.guid)

                            if len(res):
                                details = {}
                                details['dicoms'] = [self.sort_dicom(dicom) for dicom in res]

                                details['order_detail'] = self.order_details(order)

                                data['orders'][str(order.guid)] = details

                        logging.info("Querying study for hub")
                    else:
                        data['error'] = "Currently No Order"
                except Provider.DoesNotExist:
                    data['user_error'] = "Invalid Provider guid"


            else:
                raise ValueError

        except ValueError:
            return api_error_message('ParameterMissing', 'Invalid GET request')

        logging.info("Sending Studies to the hub")

        return dict_to_json(data, 200)

    def post(self, request):
        data = {}
        # status flag
        status_flag = {'1': 'study synced to hub', '2': 'dicom started sync', '3': 'dicom sync is complete'}

        logging.info("Changing flag for order")

        try:
            order_guid = request.POST.get('order_guid')
            flag = request.POST.get('hubsync_flag')

            try:
                if (flag in status_flag):
                    logging.info(status_flag[flag])

                    order = Order.objects.get(guid=order_guid)
                    order.hub_sync = flag
                    order.save()
                    data['success'] = "flag successfully changed"

                    logging.info("hubsync flag changed successfully")
            except Order.DoesNotExist:
                data['error'] = "Invalid Order Guid"


        except ValueError as e:
            return api_error_message('ParameterMissing', 'Invalid POST request')

        return dict_to_json(data, 200)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(StudyForHubView, self).dispatch(*args, **kwargs)


class DicomForHubView(View):
    def get(self, request):
        response = None
        try:
            dicom_guid = request.GET.get('dicom_guid')
            order_guid = request.GET.get('order_guid')

            # getting dicom by order guid
            dicom = DicomFile.objects.get(order__guid=order_guid, guid=dicom_guid)

            logger.info("request for dicom")

            target_dicom = None

            # if compression is requested
            if request.GET.get('compress'):
                # if s3 is enabled
                if (settings.USE_S3):
                    conn = S3Connection(settings.AWS_ACCESS_KEY_ID,
                                        settings.AWS_SECRET_ACCESS_KEY,
                                        host='s3-ap-southeast-1.amazonaws.com')

                    bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)
                    k = Key(bucket)

                    k.key = 'media/' + str(dicom.file)
                    # sample
                    # k.key = 'media/dicoms/f87b287955a04a7bb8316fbacf267dcb/1.3.51.0.1.0.20140827012345.19.dcm'

                    file_path = str(dicom.file).split('/')

                    k.get_contents_to_filename('/tmp/' + file_path[len(file_path) - 1])
                    logger.info("downloaded from s3 for compression")
                else:
                    logger.info("getting dicom file from local memory")
                    path = str(settings.MEDIA_ROOT) + '/' + str(dicom.file)

                    try:
                        os.system('cp ' + path + ' /tmp')
                    except OSError as e:
                        logging.info(str(e))

                file_split = str(dicom.file).split('/')
                file_name = file_split[len(file_split) - 1]

                new_path = '/tmp/' + file_name
                # extract_file_name = (os.path.splitext(new_path)[0]).split('/')[2]
                extract_file_name, file_extension = os.path.splitext(new_path)
                extract_file_name = str(extract_file_name).split('/')[2]

                zpaq_file = extract_file_name + '.zpaq'
                dcm_file = extract_file_name

                if file_extension:
                    dcm_file += file_extension

                try:
                    logger.info("Started compression")

                    logger.info(os.system('cd /tmp;zpaq a ' + str(zpaq_file) + ' ' + str(dcm_file) + ';rm ' + dcm_file))
                    target_dicom = '/tmp/' + str(zpaq_file)

                    logger.info("Ending compression")

                except OSError as e:
                    logger.info(e)

            # whithout compression
            else:

                if (settings.USE_S3):
                    s3_url = str(settings.MEDIA_URL) + str(dicom.file)
                    # s3_url = 'https://s3-ap-southeast-1.amazonaws.com/alemhealth-local/media/dicoms/e5a4e44f24ad4bfca7f3d19d37b1a12e/CR.1.2.392.200036.9125.9.0.386907393.25760512.2155019192.dcm'
                    return redirect(s3_url)
                else:
                    target_dicom = str(settings.MEDIA_ROOT) + '/' + str(dicom.file)

            file_name = target_dicom.split('/')[len(target_dicom.split('/')) - 1]

            logger.info("dicom is sending to the hub")
            file = open(target_dicom, 'rb')
            response = HttpResponse(FileWrapper(file), content_type='application/force-download')
            response['Content-Disposition'] = 'attachment; filename="%s"' % file_name


        except ValueError:
            return api_error_message('ParameterMissing', 'Invalid GET request')

        return response

    def post(self, request):
        data = {}

        try:
            order_guid = request.POST.get('order_guid')
            dicom_guid = request.POST.get('dicom_guid')

            try:
                dicom = DicomFile.objects.get(order__guid=order_guid, guid=dicom_guid)

                logging.info("Deleting dicom from tmp file")

                file_path = str(dicom.file).split('/')
                file_path = file_path[len(file_path) - 1]

                if (os.path.isfile('/tmp/' + file_path)):
                    os.remove('/tmp/' + file_path)

                    data['success'] = "dicom deleted"
                else:
                    path = os.path.splitext('/tmp/' + file_path)

                    if (os.path.isfile(path[0] + '.zpaq')):
                        os.remove(path[0] + '.zpaq')
                        data['success'] = "zpaq deleted"


            except DicomFile.DoesNotExist:
                data['error'] = "DIcom doesn't exist"



        except ValueError:
            return api_error_message('ParameterMissing', 'Invalid POST request')

        return dict_to_json(data, 200)

    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(DicomForHubView, self).dispatch(*args, **kwargs)


class ReportFromHubView(View):
    @csrf_exempt
    def dispatch(self, *args, **kwargs):
        return super(ReportFromHubView, self).dispatch(*args, **kwargs)

    @classmethod
    def _add_reporting_activity(cls, order, provider_guid):
        actor = {
            'type': 'provider',
            'name': 'Hub',
            'guid': str(provider_guid)
        }

        context = 'Report Uploaded from Hub'

        obj = {
            'type': 'report',
            'name': str(order.guid),
            'guid': str(order.guid)
        }

        listeners = [order.operator, order.hospital.user.all()[0]]

        create_activity(actor, context, listeners, obj=obj, order=order)

    def post(self, request):
        logger.info("Syncing report from alemhealth device")
        data = {}

        json_dict = json.loads(request.body)
        logger.info("request data: {0}".format(json_dict))

        try:
            guid = str(json_dict.get('order_guid'))
            logger.info("order_guid: {0}".format(guid))

            provider_guid = str(json_dict.get('provider_guid'))
            logger.info("provider_guid: {0}".format(provider_guid))

            try:
                order = Order.objects.get(guid=guid)
                logger.info("order object: {0}".format(order))
                logger.info("order object details: {0}".format(order.__dict__))

                if str(json_dict.get('type')) == 'report_update':
                    logger.info("type is report_update")
                    order.report = str(json_dict.get('report'))
                    order.status = str(json_dict.get('status'))
                    order.op_status = str(json_dict.get('op_status'))
                    order.save()

                    if not order.receipt and order.hospital.currency:
                        # create receipt
                        try:
                            price = Price.objects.get(
                                provider=order.provider,
                                priority=order.priority,
                                modality=order.modality
                            )
                            receipt = Receipt()
                            receipt.price = price

                            if order.hospital.currency.code != 'USD':
                                receipt.total_price = price.unit_price * order.hospital.currency.rate
                            else:
                                receipt.total_price = price.unit_price

                            receipt.tax_price = (order.hospital.currency.tax * receipt.total_price) / 100
                            receipt.total_price_with_tax = receipt.tax_price + receipt.total_price
                            receipt.save()

                            order.receipt = receipt
                        except Price.DoesNotExist:
                            pass

                    if order.receipt:
                        create_receipt(order)

                    logger.info("Generating PDF report for order {0}".format(order.guid))
                    status = pdf_generate(order)
                    logger.info("pdf_generate status: {0}".format(status))

                    self._add_reporting_activity(order, provider_guid)

                    if status is not None:
                        success_msg = "Report successfully generated in platform"
                        logger.info(success_msg)
                        data["success"] = success_msg
            except Order.DoesNotExist:
                err_msg = "Order does not exist"
                logger.error(err_msg)
                data["error"] = err_msg
        except ValueError as e:
            err_msg = "Invalid post request. Reason: {0}".format(e)
            logger.error(err_msg)
            data["error"] = err_msg
        except Exception as e:
            err_msg = "Unexpected error: {0}".format(e)
            logger.error(err_msg)
            data["error"] = err_msg

        return dict_to_json(data, 200)


class DicomViewerProxy(View):
    """This proxy api will access a nginx in front of viewer.

    Nginx server infront of viewer will secure the initial request to viewer url. Please check the following url for
    details:
    *Securing URLs with the Secure Link Module in NGINX and NGINX Plus*
        https://www.nginx.com/blog/securing-urls-secure-link-module-nginx-plus/
    """
    SECRET_KEY = 'AKIAIDT5BGHYRFCGJFSQ'
    session_cookie = "sessionCookie"
    php_session_id = "PHPSESSID"
    viewer_path = '/alemhubviewer/index.php'
    query_param_file_template = 'dicoms/{guid}'

    viewer_redirect_url = "{}/alemhubviewer/md5/index.html".format(settings.VIEWER_INFO['base_url'])

    @method_decorator(check_user_cookie)
    def get(self, request, guid):
        try:
            guid_hex = uuid.UUID(guid).hex
        except:
            return dict_to_json({'msg': '{} is not a valid guid'.format(guid)}, 400)

        if not request.user.is_authenticated():
            return dict_to_json({"msg": "Not found"}, 404)

        remote_address = request.META.get('REMOTE_ADDR')
        logger.info('Dicom viewer request from {} for guid: {}'.format(remote_address, guid_hex))

        viewer_url = self.get_viewer_url(settings.VIEWER_INFO['base_url'], guid_hex, '')
        logger.info('Proxy Dicom viewer reqeust to {}'.format(viewer_url))

        session = requests.Session()
        response = session.get(viewer_url)
        content_type = response.headers['content-type']
        http_response = HttpResponseRedirect(redirect_to=self.viewer_redirect_url, content_type=content_type)
        logger.info('Send redirect response to client: {}'.format(self.viewer_redirect_url))

        try:
            session_cookie_value = session.cookies.get_dict()[self.session_cookie]
            php_session_id_value = session.cookies.get_dict()[self.php_session_id]
        except Exception as e:
            logger.debug('Session Cookies from viewer: {}'.format(session.cookies.get_dict()))
            logger.error("exception occurred in getting cookie data. reason: " + str(e))
            return HttpResponseNotFound('Study not found from viewer.')

        http_response.set_cookie(
            self.session_cookie,
            value=session_cookie_value,
            domain=settings.VIEWER_INFO['domain'],
            path=settings.VIEWER_INFO['path']
        )
        http_response.set_cookie(
            self.php_session_id,
            value=php_session_id_value,
            domain=settings.VIEWER_INFO['domain'],
            path=settings.VIEWER_INFO['path']
        )

        return http_response

    def get_viewer_url(self, viewer_base_url, guid, remote_address):
        expire_datetime = datetime.datetime.utcnow() + datetime.timedelta(days=7)

        query_param_file = self.query_param_file_template.format(guid=guid)
        epoch_seconds, md5_hash = generate_hash(expire_datetime, self.viewer_path, self.query_param_file_template.format(guid=guid), remote_address, self.SECRET_KEY)
        logger.info('Generated hash for viewer URL. expire seconds: {}, md5 hash: {}'.format(epoch_seconds, md5_hash))

        final_url = '{viewer_base_url}{uri}?file={query_param_file}&htmlMode=on&md5={hash}&expires={epoch_seconds}'.format(
            viewer_base_url=viewer_base_url,
            uri=self.viewer_path,
            query_param_file=query_param_file,
            hash=md5_hash,
            epoch_seconds=epoch_seconds)
        return final_url


class DicomViewerThroughDcm4che(View):
    viewer_redirect_url = "{base_url}/alemviewer/md5/index.html?study={dicom_uid}"

    @method_decorator(check_user_cookie)
    def get(self, request, guid):
        logger.info('Dicom viewer request for order: {}'.format(guid))
        if not request.user.is_authenticated():
            return dict_to_json({"msg": "Not found"}, 404)

        try:
            order = Order.objects.get(guid=guid)
            url = self.viewer_redirect_url.format(dicom_uid=order.dicom_uid, base_url=settings.PUBLIC_DCM4CHEE_VIEWER)
            logger.info('Redirect viewer request to {}'.format(url))
            return HttpResponseRedirect(
                redirect_to=url,
            )
        except Order.DoesNotExist:
            logger.error('Order not found with guid: {}'.format(guid))
            return dict_to_json({"msg": "Not found"}, 404)


class OrdersAhConnectView(View):
    log_prefix = "order ah connect >>>"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(OrdersAhConnectView, self).dispatch(*args, **kwargs)

    @method_decorator(check_accesskey)
    def post(self, request):
        logger.info("{0} create order".format(self.log_prefix))

        request_data = json.loads(request.body)
        logger.info("{0} request_data: {1}".format(self.log_prefix, request_data))

        facility_guid = request_data.get('facility_guid')
        logger.info("{0} facility_guid: {1}".format(self.log_prefix, facility_guid))

        if not facility_guid:
            logger.error("{0} facility_guid does not exist in request body".format(self.log_prefix))
            data = {
                "code": 500,
                "message": "Facility guid is required",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 500)

        facility_guid = validate_uuid(facility_guid)
        logger.info("{0} facility_guid: {1}".format(self.log_prefix, facility_guid))

        if not facility_guid:
            logger.error("{0} facility_guid is not valid".format(self.log_prefix))
            data = {
                "code": 500,
                "message": "Facility guid is not valid",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 500)

        try:
            hospital = Hospital.objects.get(guid=facility_guid)
            logger.info("{0} hospital: {1}".format(self.log_prefix, hospital))
        except Hospital.DoesNotExist:
            logger.error("{0} facility does not exist".format(self.log_prefix))
            data = {
                "code": 404,
                "message": "Facility does not exist",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 404)

        try:
            order = Order()
            order.active_status = 0
            order.hospital = hospital
            order.save()
            logger.info("{0} order with id {1} and guid {2} created".format(
                self.log_prefix,
                order.id,
                order.guid
            ))
        except Exception as e:
            logger.error("{0} cannot create order. Error: {1}".format(self.log_prefix, e))
            data = {
                "code": 500,
                "message": "Something went wrong. Cannot create order.",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 500)

        data = {
            'guid': str(order.guid),
            'status': order.status,
            'active_status': order.active_status
        }
        logger.info("{0} data: {1}".format(self.log_prefix, data))
        return dict_to_json(data, 200)

    @method_decorator(check_accesskey)
    def put(self, request, guid):
        logger.info("{0} update order".format(self.log_prefix))

        guid = validate_uuid(guid)
        logger.info("{0} guid: {1}".format(self.log_prefix, guid))

        if not guid:
            logger.error("{0} guid is not valid".format(self.log_prefix))
            data = {
                "code": 500,
                "message": "Guid is not valid",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 500)

        request_data = json.loads(request.body)
        logger.info("{0} request_data: {1}".format(self.log_prefix, request_data))

        try:
            order = Order.objects.get(guid=guid)
            logger.info("{0} order: {1}".format(self.log_prefix, order))
        except Order.DoesNotExist:
            logger.error("{0} order does not exist".format(self.log_prefix))
            data = {
                "code": 404,
                "message": "Order does not exist",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 404)

        order.active_status = request_data.get('active_status', order.active_status)
        order.save()
        logger.info("{0} order {1} active_status updated to {2}".format(
            self.log_prefix,
            order.guid,
            order.active_status
        ))

        result = send_dicom_to_dcm4che.delay(guid=order.guid)
        logger.info("{log_prefix} task_id: {task_id}, order_id: {order_id}, order_guid: {order_guid}".format(
            log_prefix="send dicom to dcm4che >>>",
            task_id=result.task_id,
            order_id=order.id,
            order_guid=order.guid
        ))

        data = {
            'guid': str(order.guid),
            'status': order.status,
            'active_status': order.active_status
        }
        logger.info("{0} data: {1}".format(self.log_prefix, data))
        return dict_to_json(data, 200)


class DicomsAhConnectView(DicomResource):
    log_prefix = "dicom ah connect >>>"

    @method_decorator(csrf_exempt)
    def dispatch(self, *args, **kwargs):
        return super(DicomsAhConnectView, self).dispatch(*args, **kwargs)

    @method_decorator(check_accesskey)
    def post(self, request, guid):
        logger.info("{0} create dicom images".format(self.log_prefix))

        guid = validate_uuid(guid)
        logger.info("{0} guid: {1}".format(self.log_prefix, guid))

        if not guid:
            logger.error("{0} guid is not valid".format(self.log_prefix))
            data = {
                "code": 500,
                "message": "Guid is not valid",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 500)

        try:
            order = Order.objects.get(guid=guid)
            logger.info("{0} order: {1}".format(self.log_prefix, order))
        except Order.DoesNotExist:
            logger.error("{0} order does not exist".format(self.log_prefix))
            data = {
                "code": 404,
                "message": "Order does not exist",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 404)

        try:
            dcm_file = DicomFile()
            dcm_file.order = order
            dcm_file.file = request.FILES.get('file')
            dcm_file.save()
            logger.info("{0} dicom image with id {1} and guid {2} created".format(
                self.log_prefix,
                dcm_file.id,
                dcm_file.guid
            ))
        except Exception as e:
            logger.error("{0} cannot create dicom image. Error: {1}".format(self.log_prefix, e))
            data = {
                "code": 500,
                "message": "Something went wrong. Cannot create dicom image.",
            }
            logger.info("{0} data: {1}".format(self.log_prefix, data))
            return dict_to_json(data, 500)

        if order.meta_status == 0:
            order.meta_status = 1
            order.save()
            logger.info("{0} order {1} meta_status updated to {2}".format(
                self.log_prefix,
                order.guid,
                order.meta_status
            ))

        data = {
            'order_guid': str(order.guid),
            'dicom_file_guid': str(dcm_file.guid)
        }
        logger.info("{0} data: {1}".format(self.log_prefix, data))
        return dict_to_json(data, 200)
