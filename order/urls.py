from django.conf.urls import url
import order
from order import views
from order.api import *

urlpatterns = [

    url(r'^api/upload/?$', order.api.upload_dicom),
    url(r'^api/orders/sms/?$', SendSMS.as_view()),
    url(r'^api/orders/email/?$', SendEmail.as_view()),

    url(r'^api/orders/dicom/?$', DicomFileUpload.as_view()),
    url(r'^api/doctor-signature/?$', DoctorSignaturesView.as_view()),
    url(r'^api/doctor-signature/(?P<id>\d+)?$', DoctorSignatureView.as_view()),

    url(r'^api/second-doctor-signature/?$', SecondDoctorSignaturesView.as_view()),
    url(r'^api/second-doctor-signature/(?P<id>\d+)?$', SecondDoctorSignatureView.as_view()),

    url(r'^api/orders/report?$', OrdersReportView.as_view()),

    url(r'^api/orders/?$', OrdersView.as_view()),
    url(r'^api/orders/(?P<guid>[^/]+)?$', OrderView.as_view()),

    url(r'^api/dicoms/?$', DicomsView.as_view()),
    url(r'^api/dicoms/(?P<guid>[^/]+)?$', DicomView.as_view()),
    url(r'^api/patient-orders/?$', PatientOrders.as_view()),
    url(r'^api/test-pdf/?$', testView.as_view()),

    url(r'^dicom_s3_sync_complete/?$', order.views.dicom_s3_sync_complete),
    url(r'^mirth/?$', order.views.mirth),

    url(r'^api/all-reports/', AllReportsView.as_view()),
    url(r'^api/get-updated-reports', order.api.get_updated_reports),

    url(r'^api/studyforhub/?$', StudyForHubView.as_view()),
    url(r'^api/dicomforhub/?$', DicomForHubView.as_view()),
    url(r'^api/reportfromhub/?$', ReportFromHubView.as_view()),

    url(r'^api/dicom-viewer/(?P<guid>[^/]+)?$', DicomViewerProxy.as_view()),
    url(r'^api/dicom-image-viewer/(?P<guid>[^/]+)?$', DicomViewerThroughDcm4che.as_view()),

    # Alemhealth Connect
    url(r'^api/orders/ah-connect/?$', OrdersAhConnectView.as_view()),
    url(r'^api/orders/ah-connect/(?P<guid>[^/]+)?$', OrdersAhConnectView.as_view()),
    url(r'^api/dicoms/ah-connect/(?P<guid>[^/]+)?$', DicomsAhConnectView.as_view()),
]
