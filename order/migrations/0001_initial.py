# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-08-28 06:17
from __future__ import unicode_literals

import alemhealth.jsonfield
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion
import order.models
import uuid


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('alemhealth', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='DicomFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('file', models.FileField(max_length=320, upload_to=order.models.dicom_file_upload_dir)),
                ('edit_meta', models.IntegerField(choices=[(0, b'Not edit'), (1, b'Edit'), (2, b'Send')], default=0)),
                ('transfer_syntax', models.CharField(blank=True, choices=[(b'LittleEndianImplicit', b''), (b'LittleEndianExplicit', b''), (b'DeflatedExplicitVRLittleEndian', b''), (b'BigEndianExplicit', b''), (b'JPEGProcess1', b''), (b'JPEGProcess2_4', b''), (b'JPEGProcess14SV1', b''), (b'JPEGLSLossless', b'--propose-jls-lossless'), (b'JPEGLSLossy', b'--propose-jls-lossy'), (b'JPEG2000LosslessOnly', b'--propose-j2k-lossless'), (b'JPEG2000', b'--propose-j2k-lossy'), (b'MPEG2MainProfileAtMainLevel', b'--propose-mpeg2'), (b'MPEG2MainProfileAtHighLevel', b'--propose-mpeg2-high'), (b'RLELossless', b'--propose-rle')], max_length=128, null=True)),
                ('annotation_data', alemhealth.jsonfield.JSONField(blank=True, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
            ],
            options={
                'db_table': 'dicom_images',
            },
        ),
        migrations.CreateModel(
            name='DoctorSignature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('signature', models.FileField(upload_to=order.models.doctor_signatures_file_upload_dir)),
            ],
            options={
                'db_table': 'doctor_signatures',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('dicom_uid', models.TextField(blank=True, null=True)),
                ('dr_code', models.IntegerField(blank=True, null=True)),
                ('referral_code', models.IntegerField(blank=True, null=True)),
                ('accession_id', models.CharField(blank=True, max_length=128, null=True)),
                ('modality', models.CharField(blank=True, choices=[(b'CT', 1), (b'NM', 2), (b'MR', 3), (b'DS', 4), (b'DR', 5), (b'US', 6), (b'OT', 7), (b'HSG', 8), (b'CR', 9), (b'OP', 10)], max_length=10, null=True)),
                ('speciality', models.TextField(blank=True, null=True)),
                ('history', models.TextField(blank=True, null=True)),
                ('priority', models.CharField(blank=True, choices=[(b'Routine', b'Routine'), (b'STAT', b'STAT')], max_length=12, null=True)),
                ('status', models.IntegerField(choices=[(0, b'Draft study'), (1, b'Study stored'), (2, b'Study syncing'), (3, b'Retry routing'), (4, b'Report pending'), (5, b'Report ready'), (6, b'Report delivered')], default=0)),
                ('meta_status', models.IntegerField(choices=[(0, b'Not extracted'), (1, b'Extracted'), (2, b'Meta write')], default=0)),
                ('billing_status', models.IntegerField(choices=[(0, b'Not billed'), (1, b'Free Study'), (2, b'Invoice Pending'), (3, b'Paid')], default=0)),
                ('active_status', models.IntegerField(choices=[(0, b'Not Active'), (1, b'Active')], default=1)),
                ('report', models.TextField(blank=True, null=True)),
                ('doctor_signature', models.FileField(blank=True, null=True, upload_to=order.models.doctor_signature_file_upload_dir)),
                ('report_filename', models.FileField(blank=True, null=True, upload_to=order.models.report_file_upload_dir)),
                ('report_file_updated_at', models.DateTimeField(blank=True, default=None, null=True)),
                ('second_report', models.TextField(blank=True, null=True)),
                ('second_report_filename', models.FileField(blank=True, null=True, upload_to=order.models.report_file_upload_dir)),
                ('zipfile', models.FileField(blank=True, null=True, upload_to=order.models.zipfile_upload_dir)),
                ('report_time', models.DecimalField(blank=True, decimal_places=5, max_digits=18, null=True)),
                ('read_status', models.IntegerField(choices=[(0, b'Not read'), (1, b'Read')], default=0)),
                ('hub_sync', models.IntegerField(choices=[(0, b'not synchronized'), (1, b'started synchronizing'), (2, b'synchronization completed')], default=0)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('op_status', models.CharField(blank=True, max_length=128, null=True)),
                ('is_storage', models.BooleanField(default=False)),
                ('doctor', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='alemhealth.Doctor')),
                ('hospital', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='orders', to='alemhealth.Hospital')),
                ('operator', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='orders', to=settings.AUTH_USER_MODEL)),
                ('patient', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='orders', to='alemhealth.Patient')),
                ('provider', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='orders', to='alemhealth.Provider')),
                ('provider_radiologist', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='provider_radiologist_orders', to='alemhealth.Doctor')),
                ('radiologist', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='radiologist_orders', to='alemhealth.Doctor')),
            ],
            options={
                'db_table': 'orders',
            },
        ),
        migrations.CreateModel(
            name='OrderMeta',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('key', models.TextField()),
                ('value', models.TextField(blank=True, null=True)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='metas', to='order.Order')),
            ],
            options={
                'db_table': 'order_metas',
            },
        ),
        migrations.CreateModel(
            name='Price',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('priority', models.CharField(choices=[(b'Routine', b'Routine'), (b'STAT', b'STAT')], max_length=12)),
                ('modality', models.CharField(choices=[(b'CT', b'CT'), (b'NM', b'NM'), (b'MR', b'MR'), (b'DS', b'DS'), (b'DR', b'DR'), (b'US', b'US'), (b'OT', b'OT'), (b'HSG', b'HSG'), (b'CR', b'CR'), (b'MG', b'MG'), (b'OP', b'OP')], max_length=10)),
                ('unit_cost', models.DecimalField(decimal_places=2, max_digits=8)),
                ('unit_price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('provider', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='prices', to='alemhealth.Provider')),
            ],
            options={
                'db_table': 'prices',
            },
        ),
        migrations.CreateModel(
            name='Receipt',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('guid', models.UUIDField(default=uuid.uuid4, editable=False, unique=True)),
                ('total_price', models.DecimalField(decimal_places=2, max_digits=8)),
                ('tax_price', models.DecimalField(blank=True, decimal_places=2, max_digits=8, null=True)),
                ('total_price_with_tax', models.DecimalField(decimal_places=2, max_digits=8)),
                ('receipt_pdf', models.FileField(blank=True, null=True, upload_to=order.models.receipt_file_upload_dir)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
                ('updated_at', models.DateTimeField(auto_now=True)),
                ('price', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='receipts', to='order.Price')),
            ],
            options={
                'db_table': 'receipts',
            },
        ),
        migrations.CreateModel(
            name='SecondDoctorSignature',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('signature', models.FileField(upload_to=order.models.doctor_signatures_file_upload_dir)),
                ('order', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='second_doctor_signatures', to='order.Order')),
            ],
            options={
                'db_table': 'second_doctor_signatures',
            },
        ),
        migrations.AddField(
            model_name='order',
            name='receipt',
            field=models.OneToOneField(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='order', to='order.Receipt'),
        ),
        migrations.AddField(
            model_name='order',
            name='second_provider',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='second_orders', to='alemhealth.Provider'),
        ),
        migrations.AddField(
            model_name='doctorsignature',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='doctor_signatures', to='order.Order'),
        ),
        migrations.AddField(
            model_name='dicomfile',
            name='order',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='dicoms', to='order.Order'),
        ),
        migrations.AlterUniqueTogether(
            name='price',
            unique_together=set([('provider', 'priority', 'modality')]),
        ),
    ]
