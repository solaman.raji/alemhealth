#!/usr/bin/python
"""
Storage SCU example.

This demonstrates a simple application entity that support the RT Plan
Storage SOP Class as SCU. For this example to work, there must be an
SCP listening on the specified host and port.

For help on usage,
python storescu.py -h
"""

import sys
import argparse
from netdicom import AE, StorageSOPClass, VerificationSOPClass
from dicom.UID import ExplicitVRLittleEndian, ImplicitVRLittleEndian, ExplicitVRBigEndian, DeflatedExplicitVRLittleEndian
from dicom import read_file
import os, dicom
from dicom import read_file



remote_host = 'localhost'
remote_port = 11112
remote_aet = 'alemhealth'

ts = [
    ExplicitVRLittleEndian,
    ImplicitVRLittleEndian,
    ExplicitVRBigEndian,
    DeflatedExplicitVRLittleEndian,
]

# call back
def OnAssociateResponse(association):
    print "Association response received"



local_port = 9000


# create application entity
try:
    MyAE = AE('localhost', local_port, [StorageSOPClass,  VerificationSOPClass], [], ts)
except IOError, e:
    print e


MyAE.OnAssociateResponse = OnAssociateResponse

# remote application entity
RemoteAE = dict(Address=remote_host, Port=remote_port, AET=remote_aet)



# create association with remote AE
print "Request association"

assoc = MyAE.RequestAssociation(RemoteAE)

if not assoc:
    print "Could not establish association"
    sys.exit(1)

# perform a DICOM ECHO, just to make sure remote AE is listening
#st = assoc.VerificationSOPClass.SCU(1)
#print 'done with status "%s"' % st


file_name = '/Users/macbookpro/development/python/alemhealth/media/test2.dcm'
file_image = dicom.read_file(file_name)
#import pdb;pdb.set_trace()

print '--------------------'
print file_image
print file_image.SOPClassUID
print '--------------------'
# create some dataset
print "DICOM StoreSCU ... ",
try:
    st = assoc.SCU(file_image, 1)
    print 'done with status "%s"' % st

except Exception as e:
    print e
    print "problem", file_image.SOPClassUID
    print file_image.SOPClassUID




print "Release association"
assoc.Release(0)


# done
MyAE.Quit()

#os.system('storescu localhost 11112 -aet alemhealth /Users/macbookpro/development/python/alemhealth/media/test2.dcm -xs --log-config /Users/macbookpro/development/python/alemhealth/media/log.txt')