from django.conf.urls import url
from activity.api import *

urlpatterns = [

    url(r'^api/activities/?$', ActivitiesView.as_view()),
    url(r'^api/activities/(?P<guid>[^/]+)?$', ActivityView.as_view()),

]