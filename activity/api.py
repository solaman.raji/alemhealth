from django.views.generic import View
from django.utils.decorators import method_decorator

from alemhealth.decorators import *
from alemhealth.utils import *
from activity.models import *


class ActivityResource(View):

    def format(self, item):

        data = {
            'id' : str(item.pk),
            'guid': str(item.activity.guid),
            'actor': item.activity.actor,
            'context': item.activity.context,
            'target': item.activity.target,
            'obj': item.activity.obj,
            'created_at': str(item.activity.created_at),
            'mark_as_read': item.mark_as_read if 1 else 0
        }
        return data

    def custom_format(self, activity):

        data = {
            'id' : str(activity.pk),
            'guid': str(activity.guid),
            'actor': activity.actor,
            'context': activity.context,
            'target': activity.target,
            'obj': activity.obj,
            'created_at': str(activity.created_at),
            'mark_as_read':1
        }
        return data


class ActivitiesView(ActivityResource):

    @method_decorator(check_accesskey)
    def get(self, request):

        post_per_page = 20
        page = request.GET.get('page')

        if not page:
            page = 1
        else:
            page = int(page)

        offset = page * post_per_page - post_per_page
        pagesize = page * post_per_page

        group_name_list = [a.name for a in request.user.groups.all()]

        if 'admin' in group_name_list:
            activities = Activity.objects.all().order_by('-created_at')
            if request.GET.get('notification'):
                count = 0
            else:
                count = activities.count()
                
        else:
            activities = request.user.activities.all().order_by('-activity__created_at')

            if request.GET.get('notification'):
                activities = activities.filter(mark_as_read=False)

            count = activities.count()

        if page != -1:
            activities = activities[offset:pagesize]

        data = {}

        data['meta'] = {
            "offset": offset,
            "pagesize": pagesize,
            "count": count
        }
        if 'admin' in group_name_list:
            data['activities'] = [self.custom_format(a) for a in activities]
        else:
            data['activities'] = [self.format(a) for a in activities]

        return dict_to_json(data, 200)


class ActivityView(ActivityResource):

    @method_decorator(check_accesskey)
    def put(self, request, guid):

        try:
            activity = Activity.objects.get(guid=guid)
        except Activity.DosenotExist:
            return api_error_message('DoesNotExist', "Activity")

        listener = activity.listeners.get(user=request.user)

        listener.mark_as_read = True
        listener.save()

        data = self.format(listener)

        return dict_to_json(data, 200)
