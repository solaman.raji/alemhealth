import uuid
import logging
import boto3
import json

# TODO: Follow right process
# http://boto3.readthedocs.io/en/latest/guide/configuration.html
from alemhealth.settings import AH_ENV

logger = logging.getLogger('alemhealth')

sqs = boto3.client(
    'sqs',
    # Hard coded strings as credentials, not recommended.
    aws_access_key_id='AKIAIDT5BGHYRFCGJFSQ',
    aws_secret_access_key='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'
)

QUEUE_NAME = 'study-events'
STUDY_QUEUE = '-'.join(['https://ap-southeast-1.queue.amazonaws.com/194130111156/study-events', AH_ENV])


class RequestMessage(object):
    def __init__(self, body, attributes):
        super(RequestMessage, self).__init__()
        self.body = body
        self.attributes = attributes
        self.guid = None

    def set_guid(self, guid):
        self.guid = guid


class ResponseMessage(object):
    def __init__(self, guid, body, attributes, receipt_handle):
        super(ResponseMessage, self).__init__()
        self.guid = guid
        self.body = body
        self.attributes = attributes
        self.receipt_handle = receipt_handle

    @classmethod
    def empty_response(cls, message):
        return message.guid == None


def create_queue(name, **kwargs):
    # Create a SQS queue
    response = sqs.create_queue(
        QueueName=name,
        Attributes={
            'DelaySeconds': '60',
            'MessageRetentionPeriod': '86400'
        }
    )

    print(response['QueueUrl'])

    return response['QueueUrl']


def list_queues():
    response = sqs.list_queues()

    print(response['QueueUrls'])
    return response['QueueUrls']


def delete_queue(url):
    # Delete SQS queue
    result = sqs.delete_queue(QueueUrl=url)
    return result


class SQSClient(object):
    queue_url = STUDY_QUEUE
    _client = None

    @classmethod
    def get_client(cls):
        if not cls._client:
            cls._client = SQSClient()
        return cls._client

    def __init__(self, url=None):
        super(SQSClient, self).__init__()
        if url:
            self.queue_url = url

    def send_message(self, message):
        assert type(message) == RequestMessage
        # Send message to SQS queue
        dummy_msg = 'Information about current NY Times fiction bestseller for week of 12/11/2016.'
        response = sqs.send_message(
            QueueUrl=self.queue_url,
            DelaySeconds=10,
            MessageAttributes={
                'Title': {
                    'DataType': 'String',
                    'StringValue': 'Activity'
                },
                'Author': {
                    'DataType': 'String',
                    'StringValue': 'Cloud Platform'
                }
            },
            MessageBody=(json.dumps(message.body) or dummy_msg)
        )

        logger.info('Sent message with ID: %s' % response['MessageId'])
        logger.info('Response: %s' % response)
        return response['MessageId']

    def receive_message(self):
        # Receive message from SQS queue
        response = sqs.receive_message(
            QueueUrl=self.queue_url,
            AttributeNames=[
                'SentTimestamp'
            ],
            MaxNumberOfMessages=1,
            MessageAttributeNames=[
                'All'
            ],
            VisibilityTimeout=0,
            WaitTimeSeconds=20
        )

        print 'response', response
        logger.debug('Response from queue: %s' % response)
        messages_key = 'Messages'

        if messages_key not in response:
            return ResponseMessage(None, '', {}, '')

        message = response['Messages'][0]
        message = ResponseMessage(uuid.UUID(message['MessageId']),
                                  json.loads(message['Body']),
                                  message['MessageAttributes'],
                                  message['ReceiptHandle'])
        return message

    def delete_message(self, receipt_handle):
        # Delete received message from queue
        sqs.delete_message(
            QueueUrl=self.queue_url,
            ReceiptHandle=receipt_handle
        )
        print('Received and deleted message: %s' % receipt_handle)
        logger.debug('Received and deleted message: %s' % receipt_handle)
