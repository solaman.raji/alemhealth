import uuid
from django.db import models
from django.contrib.auth.models import User

from alemhealth.jsonfield import JSONField
from order.models import Order
import logging

logger = logging.getLogger('alemhealth')


class Activity(models.Model):
    """
        Activity types
        actor = {
            'type' : user/study,
            'name' : 'name',
            'guid' : 'guid'
        }

        obj = {
            'type' : provider/study/doctor,
            'name' : 'name',
            'guid' : 'guid'
        }

        target = {
            'type' : study/provider,
            'name' : 'name',
            'guid' : 'guid',
            'prefix': '',
            'postfix': '',
        }
    """

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    actor = JSONField()
    context = models.TextField()
    obj = JSONField(null=True, blank=True)
    target = JSONField(null=True, blank=True)
    order = models.ForeignKey(
        Order, related_name="activities", blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        db_table = 'activities'

    def __unicode__(self):
        return str(self.guid)


class ActivityListener(models.Model):
    activity = models.ForeignKey(Activity, related_name='listeners')
    user = models.ForeignKey(User, related_name='activities')
    mark_as_read = models.BooleanField(default=False)

    class Meta:
        db_table = 'activity_listeners'
        unique_together = (("activity", "user"),)

    def __unicode__(self):
        return str(self.activity.guid)

    # def save(self, *args, **kwargs):
    #     super(ActivityListener, self).save(**kwargs)


def create_activity(actor, context, listeners, order=None, obj=None, target=None, user=None):
    try:
        activity = Activity()
        activity.actor = actor
        activity.context = context
        logger.debug('Activity details: %s, %s' % (activity.order, activity.guid))
        logger.debug('Activity actor: %s' % activity.actor)
        logger.debug('Activity context: %s' % activity.context)
        logger.debug('Activity target: %s' % activity.target)

        if obj:
            activity.obj = obj
            logger.debug('Activity obj: %s' % activity.obj)
        if target:
            activity.target = target
        if order:
            activity.order = order

        activity.save()

        logger.info('Prepare message')
        message = {'order_guid': order.guid.hex if order else None,
                   'actor': actor,
                   'target': target,
                   'obj': obj,
                   'context': context}
        logger.info('Push activity message to queue')
        MessageQueueClient.push(message)

        if listeners:
            for listener in listeners:
                activity_listener = ActivityListener()
                activity_listener.activity = activity
                activity_listener.user = listener
                if user == listener:
                    activity_listener.mark_as_read = True
                activity_listener.save()

        logger.info('Activity ' + str(activity.guid) + ' save successfully')
    except Exception as e:
        logger.error(str(e))


class MessageQueueClient(object):
    @classmethod
    def push(cls, message):
        # FIXME: Deployment fails to load the sqsclient
        from activity.queue import SQSClient, RequestMessage
        # TODO: Use message attributes
        SQSClient.get_client().send_message(RequestMessage(message, {}))

