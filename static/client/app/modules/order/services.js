'use restrict'

angular.module('OrderApp.services').factory('Order', function($resource){
   return $resource('/api/orders/:guid', { guid : '@guid' }, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('Dicom', function($resource){
   return $resource('/api/dicoms/:guid', { guid : '@guid' }, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('SMS', function($resource){
   return $resource('/api/orders/sms/', {}, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});

angular.module('OrderApp.services').factory('OrderEmail', function($resource){
   return $resource('/api/orders/email/', {}, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('DoctorSignature', function($resource){
   return $resource('/api/doctor-signature/:id', { id : '@id' }, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});


angular.module('OrderApp.services').factory('Report', function($resource){
   return $resource('/api/orders/report', {}, {
       query: { method: 'GET' },
       update: {method: 'PUT' }
   });
});

angular.module('OrderApp.services').service('studyDetailService', function($resource) {
    return $resource('api/orders/:guid', {guid:'@guid'}, {
        query : {method : 'GET'},
        save : {method : 'PUT'}
    });
});

angular.module('OrderApp.services').service('reportService', function($resource) {
    return $resource('api/report/:guid/', { guid: '@guid' }, {
        query: {
            method: 'GET'
        },
        create: {
            method: 'POST'
        }
    });
});

angular.module('OrderApp.services').service('reportFormatService', function($resource) {
    return $resource('api/reporttemplate', {}, {
        query: {
            method: 'GET'
        },
        delete: {
            method: 'DELETE'
        },
        save: {
            method: 'PUT'
        },
        create: {
            method: 'POST'
        }
    })
});
