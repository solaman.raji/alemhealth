'use strict'

angular.module('OrderApp.controllers').controller('OrdersCtrl', function ($scope, $rootScope, $state, $timeout, Order, flash, $filter, Hospital, Doctor, Provider, Activity) {

    console.group("Study List Page")
    // Check activity  notifications
    Activity.get({'notification': 1}, function success(data) {
        $rootScope.activities = data.activities
        $rootScope.activities.notification = data.meta.count
    })

    $scope.search_box = null;
    $scope.search_with = {};

    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;
    $scope.report_date = {startDate: null, endDate: null};

    $scope.q = undefined;
    $scope.status = undefined;

    $scope.provider_radiologist = undefined;
    $scope.hospital = undefined;
    //  get all hospital list

    Hospital.query({page: -1},
        function success(response) {
            $scope.hospitals = response.hospitals;

            var country = [];

            for (var i = 0; i < response.hospitals.length; i++) {
                if (country.length > 0) {
                    if (country.indexOf(response.hospitals[i].country) == -1) {
                        country.push(response.hospitals[i].country)
                    }
                }
                else {
                    country.push(response.hospitals[i].country)
                }
            }

            $scope.hospitals_country = country;
        });


    // get doctor list
    Doctor.query({
        page: -1,
    }, function success(response) {
        $scope.doctors = response.doctors;
    });

    // getting default radiologist
    Provider.query({'doctor_list': 1}, function success(response) {
        $timeout(function () {
            $scope.default_radiologist = response.providers.default_radiologist;

            if ($scope.default_radiologist != 0) {
                $scope.disabled = false;
            } else {
                $scope.disabled = true;
            }

        }, 0)
    });

    $scope.search_remove = function (s) {
        $scope[s.modal] = "";
        delete $scope.search_with[s.key];
    };

    $scope.showSelectedFilterBox = function (options) {
        if (options.search_key != undefined && options.search_value != undefined) {
            if (!(options.search_key in $scope.search_with)) {
                $scope.search_with[options.search_key] = {
                    'key': options.search_key,
                    'value': options.search_value,
                    'show_to_user': options.show_to_user,
                    'modal': options.modal
                };
            } else {
                delete $scope.search_with[options.search_key];
                $scope.search_with[options.search_key] = {
                    'key': options.search_key,
                    'value': options.search_value,
                    'show_to_user': options.show_to_user,
                    'modal': options.modal
                };
            }

            $scope[options.modal] = options.search_value;
        }
    };

    // adding from all drop down box
    $scope.$watch('search_box', function () {

        if ($scope.search_box != undefined) {
            var search_key = angular.element("#search_box option").filter(":selected").parent("optgroup").attr("label");
            var search_value = angular.element("#search_box option").filter(":selected").attr("value");
            var show_to_user = angular.element("#search_box option").filter(":selected").attr("label");
            var modal = angular.element("#search_box option").filter(":selected").parent("optgroup").attr("id");

            if (search_key != undefined && search_value != undefined) {
                if (!(search_key in $scope.search_with)) {
                    $scope.search_with[search_key] = {
                        'key': search_key,
                        'value': search_value,
                        'show_to_user': show_to_user,
                        'modal': modal
                    };
                } else {
                    delete $scope.search_with[search_key];
                    $scope.search_with[search_key] = {
                        'key': search_key,
                        'value': search_value,
                        'show_to_user': show_to_user,
                        'modal': modal
                    };
                }

                $scope[modal] = search_value;

            }

            $scope.search_box = undefined;
        }

    });


    $scope.$watch('today', function () {
        if ($scope.today) {

            // disabling the fellow datepickers
            $scope.yesterday = false;
            $scope.report_date = {startDate: null, endDate: null};

            // todays report
            $scope.report_date.from = moment().format('YYYY-MM-DD') + " 0:0:0";
            $scope.report_date.to = moment().format('YYYY-MM-DD') + " 23:0:0";

            $scope.fetchResults();
        } else if (!$scope.yesterday) {
            if ($scope.report_date.startDate == null && $scope.report_date.endDate == null) {
                $scope.report_date.from = null;
                $scope.report_date.to = null;

                $scope.fetchResults();
            }
        }
    });

    $scope.$watch('yesterday', function () {
        if ($scope.yesterday) {

            // disabling the fellow datepickers
            $scope.today = false;
            $scope.report_date = {startDate: null, endDate: null};

            //yesterday's report
            var prev_obj = moment().add(-1, 'days');
            $scope.report_date.from = prev_obj.format('YYYY-MM-DD') + " 0:0:0";
            $scope.report_date.to = prev_obj.format('YYYY-MM-DD') + " 23:0:0";

            $scope.fetchResults();
        }  else if (!$scope.today) {
            if ($scope.report_date.startDate == null && $scope.report_date.endDate == null) {
                $scope.report_date.from = null;
                $scope.report_date.to = null;

                $scope.fetchResults();
            }
        }
    });

    $scope.$watch('report_date', function () {
        if ($scope.report_date.startDate != null && $scope.report_date.endDate != null) {

            // disabling the fellow datepickers
            $scope.yesterday = false;
            $scope.today = false;

            var from = moment($scope.report_date.startDate).format('YYYY-MM-DD');
            var to = moment($scope.report_date.endDate).format('YYYY-MM-DD');

            $scope.report_date.from = from;
            $scope.report_date.to = to;

            $scope.fetchResults();
        }
    });

    $scope.fetchResults = function () {
        console.log('filter modality: %s', $scope.modality);
        flash.info = "Order loading..."
        $scope.results = Order.query({
            page: $scope.bigCurrentPage,
            q: $scope.q,
            status: $scope.status,
            hospital: $scope.hospital,
            hospital_country : $scope.hospital_country,
            priority : $scope.study_type,
            from : $scope.report_date.from,
            to : $scope.report_date.to,
            modality_search : $scope.modality

        }, function success(response) {
            console.log(response);
            window.response = response;
            flash.info = '';
            $scope.bigTotalItems = response.meta.count;
        });
    }

    // $scope.fetchResults();

    $scope.$watch('status', function () {
        if ($scope.status != undefined) {
            $scope.fetchResults();
        }
    });
    $scope.$watch('hospital', function () {
        if ($scope.hospital != undefined) {
            $scope.fetchResults();
        }
    });
    $scope.$watch('hospital_country', function () {
        console.log('hospital country filter changed')
        if ($scope.hospital_country != undefined) {
            $scope.fetchResults();
        }
    });
    $scope.$watch('modality',function() {
        if($scope.modality != undefined) {
            $scope.fetchResults();
        }
    });
    $scope.$watch('report_date',function () {
        if($scope.report_date.startDate != null && $scope.report_date.endDate != null) {
            var from = moment($scope.report_date.startDate).format('YYYY-MM-DD');
            var to = moment($scope.report_date.endDate).format('YYYY-MM-DD');

            $scope.report_date.from = from;
            $scope.report_date.to = to;

            $scope.fetchResults()
        }
    });
    $scope.$watch('modality',function() {
        if($scope.modality != undefined) {

            var options = {
                search_key : 'Modality',
                search_value : angular.element("#modality option").filter(":selected").attr("value"),
                show_to_user : 'modality',
                modal : 'modality'
            };
            // $scope.showSelectedFilterBox(options);
            $scope.fetchResults();
        }
    });


    // watching chane on provider_radiologist
    $scope.$watch('provider_radiologist', function () {
        if ($scope.provider_radiologist != undefined) {
            if ($.trim($scope.provider_radiologist) != 0) {
                $scope.disabled = false;
            } else {
                $scope.disabled = true;
            }
        }
    });
    $scope.$watch('study_type', function () {
        if ($scope.study_type != undefined) {
            $scope.fetchResults();
        }
    });

    $scope.createOrder = function () {
        Order.save({}, function success(data) {
            $state.transitionTo('dashboard.order', {guid: data.guid});
        });
    };

    $scope.addBulkProviderRadiologists = function () {

        var orders = [];


        _.each($scope.results.orders, function (order) {
            if (order.provider_radiologist_check) {
                orders.push(order.id);
            }
        });

        if ($scope.provider_radiologist && orders.length > 0) {
            flash.info = "Adding Provider Radiologists";
            Order.update({
                guid: 'dsfsdfsdfsdfsdfsd',
                type: 'bulk_provider_radiologist',
                provider_radiologist: $scope.provider_radiologist,
                orders: orders
            }, function success(response) {
                flash.info = '';
                flash.success = "Provider Radiologists changed successfully";

                $scope.results = Order.query({
                    page: $scope.bigCurrentPage,
                    q: $scope.q,
                    status: $scope.status,
                    hospital: $scope.hospital
                }, function success(response) {

                    window.response = response;
                    flash.info = '';
                    $scope.bigTotalItems = response.meta.count;
                });


            }, function err(error) {
                flash.info = '';
                if (error.status == 400) {
                    flash.error = error.data['message'];
                } else {
                    flash.error = 'Somethings wrong please try again later';
                }
            });
        } else {
            flash.error = 'Sorry, Select orders first';
        }

    };


    window.test = $scope;
    console.groupEnd();

});


angular.module('OrderApp.controllers').controller('OrderReportCtrl', function ($scope, $stateParams, $cookieStore, $location, $state, $filter, flash, Order, Dicom) {
    // get order details

    flash.info = "Order loading...";
    $scope.order = {};
    Order.get({guid: $stateParams.guid}, function success(data) {
        $scope.order = data;
        if (data.status == 0)
            $scope.method = "Add";
        else {
            $scope.method = "Edit";

        }

        flash.info = "";
    });

    // get doctor list for hospital
    $scope.$watch('order.dicoms', function () {
        if ($scope.order.dicoms) {
            if ($scope.order.dicoms) {
                loadStudyDicomView($('#testing'), $scope.order, $filter, Dicom);
            }
        }

    });


});

angular.module('OrderApp.controllers').controller('OrderProviderReportCtrl', function ($scope, $rootScope, $modal, $stateParams, $cookieStore, $location, $state, $filter, $http, FileUploader, Order, Hospital, Provider, Doctor, OrderEmail, DoctorSignature, flash, Activity, cfpLoadingBar, Dicom) {

    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.order = {};
    flash.info = "Order loading...";

    Order.get({guid: $stateParams.guid}, function success(data) {
        $scope.order = data;
        if (data.radiologist) $scope.radioBtn.serviceType = 'radiologist';

        if (data.status == 0)
            $scope.method = "Add";
        else {
            $scope.method = "Edit";
        }

        // get Doctor list
        Doctor.query({
                page: -1,
            },
            function success(response) {
                $scope.order.doctors = response.doctors;

                // if ( $rootScope.provider_radiologist ){
                //     $scope.provider_radiologist = $rootScope.provider_radiologist
                //     $scope.order.provider_radiologist = $rootScope.provider.id
                // }
            });

        flash.info = "";
    });


    // doctor signature file upload

    $scope.doctor_signature_uploader = new FileUploader();

    // uploader options
    $scope.doctor_signature_upload_options = {
        url: '/api/doctor-signature/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.guid}],
        autoUpload: true
    };

    $scope.doctor_signature_uploader.onAfterAddingFile = function () {
        $scope.doctor_signature_uploader.uploadAll();
    };

    $scope.doctor_signature_uploader.onCompleteItem = function (item, response) {
        var index = $scope.doctor_signature_uploader.getIndexOfItem(item);
        $scope.doctor_signature_uploader.queue[index].data = response;
        console.log(item);
        console.log(response);
        $scope.order.doctor_signature = response.doctor_signature;
    };

    $scope.removeDoctorSignature = function (item) {
        flash.info = 'Deleting doctor signature ...';
        DoctorSignature.delete({id: item.data.id, guid: $stateParams.guid}, function success(data) {
            flash.info = '';
            flash.success = 'Doctor signature successfully remove...';
        }, function error(err) {
            flash.info = '';
            flash.success = 'Please try again later , some thing went wrong ..';
        });
        $scope.doctor_signature_uploader.removeFromQueue(item);

    };


    $scope.removeDoctorSignatureByID = function (id, index) {
        flash.info = 'Deleting doctor signature ...';
        DoctorSignature.delete({id: id, guid: $stateParams.guid}, function success(data) {
            flash.info = '';
            flash.success = 'Doctor signature successfully remove...';
            $scope.order.doctor_signatures.splice(index, 1);
        }, function error(err) {
            flash.info = '';
            flash.success = 'Please try again later , some thing went wrong ..';
        });
    };


    // create/update report
    $scope.createReport = function () {
        var report = $scope.order.report.replace(/<p>/g, '');
        report = report.replace(new RegExp('</p>', 'g'), '<br />');
        flash.info = 'Pdf generating ...';
        Order.update({
            guid: $stateParams.guid,
            type: 'report_update',
            report: report,
            status: 5,
            op_status: $scope.order.op_status
        }, function success(response) {

            $scope.order.status = response.status;
            $scope.order.status_text = response.status_text;
            $scope.order.report_filename = response.report_filename;
            $scope.order.receipt = response.receipt;
            flash.info = '';
            flash.success = "Your report has been created.";

        }, function err(error) {
            flash.info = '';
            if (error.status == 400)
                flash.error = error.data['message'];
            else
                flash.error = 'Somethings wrong please try again later';
        });

    };


    // update provider radiologist
    $scope.addProviderRadiologists = function () {
        flash.info = "Add Provider Radiologists";
        Order.update({
            guid: $stateParams.guid,
            type: 'provider_radiologist',
            provider_radiologist: $scope.order.provider_radiologist
        }, function success(response) {
            flash.info = '';
            flash.success = "Provider Radiologists changed successfully";

        }, function err(error) {
            flash.info = '';
            if (error.status == 400)
                flash.error = error.data['message'];
            else
                flash.error = 'Somethings wrong please try again later';
        });
    };


});

angular.module('OrderApp.controllers').controller('OrderSecondProviderReportCtrl', function ($scope, $rootScope, $modal, $stateParams, $cookieStore, $location, $state, $filter, $http, FileUploader, Order, Hospital, Provider, Doctor, OrderEmail, DoctorSignature, flash, Activity, cfpLoadingBar, Dicom) {

    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.order = {};
    flash.info = "Order loading...";

    Order.get({guid: $stateParams.guid}, function success(data) {
        $scope.order = data;
        if (data.radiologist) $scope.radioBtn.serviceType = 'radiologist';

        if (data.status == 0)
            $scope.method = "Add";
        else {
            $scope.method = "Edit";
        }

        // get Doctor list
        Doctor.query({
                page: -1,
            },
            function success(response) {
                $scope.order.doctors = response.doctors;

                // if ( $rootScope.provider_radiologist ){
                //     $scope.provider_radiologist = $rootScope.provider_radiologist
                //     $scope.order.provider_radiologist = $rootScope.provider.id
                // }
            });

        flash.info = "";
    });


    // doctor signature file upload

    $scope.doctor_signature_uploader = new FileUploader();

    // uploader options
    $scope.doctor_signature_upload_options = {
        url: '/api/second-doctor-signature/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.guid}],
        autoUpload: true
    };

    $scope.doctor_signature_uploader.onAfterAddingFile = function () {
        $scope.doctor_signature_uploader.uploadAll();
    };

    $scope.doctor_signature_uploader.onCompleteItem = function (item, response) {
        var index = $scope.doctor_signature_uploader.getIndexOfItem(item);
        $scope.doctor_signature_uploader.queue[index].data = response;
        console.log(item);
        console.log(response);
        $scope.order.doctor_signature = response.doctor_signature;
    };

    $scope.removeDoctorSignature = function (item) {
        flash.info = 'Deleting doctor signature ...';
        DoctorSignature.delete({id: item.data.id, guid: $stateParams.guid}, function success(data) {
            flash.info = '';
            flash.success = 'Doctor signature successfully remove...';
        }, function error(err) {
            flash.info = '';
            flash.success = 'Please try again later , some thing went wrong ..';
        });
        $scope.doctor_signature_uploader.removeFromQueue(item);

    };


    $scope.removeDoctorSignatureByID = function (id, index) {
        flash.info = 'Deleting doctor signature ...';
        DoctorSignature.delete({id: id, guid: $stateParams.guid}, function success(data) {
            flash.info = '';
            flash.success = 'Doctor signature successfully remove...';
            $scope.order.doctor_signatures.splice(index, 1);
        }, function error(err) {
            flash.info = '';
            flash.success = 'Please try again later , some thing went wrong ..';
        });
    };


    // create/update report
    $scope.createReport = function () {
        flash.info = 'Pdf generating ...';
        Order.update({
            guid: $stateParams.guid,
            type: 'second_report_update',
            second_report: $scope.order.second_report,
            status: 5,
            op_status: $scope.order.op_status
        }, function success(response) {

            $scope.order.status = response.status;
            $scope.order.status_text = response.status_text;
            $scope.order.report_filename = response.report_filename;
            $scope.order.receipt = response.receipt;
            flash.info = '';
            flash.success = "Your report has been created.";

        }, function err(error) {
            flash.info = '';
            if (error.status == 400)
                flash.error = error.data['message'];
            else
                flash.error = 'Somethings wrong please try again later';
        });

    };


});
angular.module('OrderApp.controllers').controller('OrderCtrl', function ($cookies, $scope, $rootScope, $modal, $stateParams, $cookieStore, $location, $state, $filter, $http, $timeout, FileUploader, Order, Hospital, Provider, Doctor, OrderEmail, DoctorSignature, flash, Activity, cfpLoadingBar, Dicom) {

    console.group("Order Detail Page: %s", $stateParams.guid)
    // Check activity  notifications
    Activity.get({'notification': 1}, function success(data) {
        $rootScope.activities = data.activities;
        $rootScope.activities.notification = data.meta.count;
    });
    var allProviders = [];

    $rootScope.activeTab = 'order';

    if ($rootScope.user_group != 'doctor')
        $scope.checkAccess = false;
    else
        $scope.checkAccess = true;


    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.order = {};
    $scope.order.showCreateReport = false;
    $scope.mainContainer = true;
    $scope.showReport = false;

    $scope.dateFormat = /^\d{4}(0?[1-9]|1[012])(0?[1-9]|[12][0-9]|3[01])$/
    $scope.timeFormat = /^(0?[1-9]|1[012]|2[01234])(0?[1-9]|[1-5][0-9]|60)(0?[1-9]|[1-5][0-9]|60)(.*)?$/
    $scope.ageFormat = /\d{3}[Y,W,D,M]$/

    //if($stateParams.report) $scope.showReport = True

    $scope.order.studydate_opened = false;
    $scope.radioBtn = {};
    $scope.radioBtn.serviceType = 'provider';

    $scope.openStudyDatepicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.order.studydate_opened = true;
    };

    $scope.order.dob_opened = false;


    $scope.openDOBDatepicker = function ($event) {
        $event.preventDefault();
        $event.stopPropagation();
        $scope.order.dob_opened = true;
    };

    $scope.format = 'dd-MMM-yyyy';

    // dicom study date
    $scope.$watch('order.metas.StudyDate', function (data) {
        if (data != undefined) {
            $scope.order.StudyDate = getDateObj(data, false);
        }
    })

    // patient date of birth
    $scope.$watch('order.patient.date_of_birth', function (data) {
        if (data != undefined) {
            $scope.order.date_of_birth = getDateObj(data, true);
        }
    })

    // dicom study time
    $scope.$watch('order.metas.StudyTime', function (data) {
        if (data != undefined) {
            $scope.order.StudyTime = getTimeObj(data)
        }
    })


    // auto complete Doctors
    $scope.getDoctors = function (val) {
        //if (val.length() > 1){
        flash.info = "Searching doctor ..."
        return $http.get('/api/doctors/', {
            params: {
                page: -1,
                q: val,
                type: 'doctor'
            }
        }).then(function (response) {
            flash.info = ''
            return response.data.doctors.map(function (item) {
                return {
                    'name': item.name,
                    'id': item.id,
                    'specialty': item.specialty,
                    'city': item.city,
                    'country': item.country
                };
            });
        });
        //}
    };


    // add doctor from auto compute list

    $scope.addDoctorFromList = function (item, model) {
        $scope.order.doctor = model.id;
    }
    // add Doctor

    $scope.addDoctor = function () {

        flash.info = 'Adding Doctor .....';
        var data = {
            'name': $scope.order.doctor_name,
            'phone': $scope.order.doctor_phone,
            'operator': true
        };
        Doctor.save(data,
            function success(response) {
                $scope.order.doctor = response.id
                flash.info = ''
                flash.success = 'Doctor add  successfully';
            }, function error(err) {

            });

    }


    // auto compute  radiologist
    $scope.getRadiologist = function (val) {
        //if (val.length() > 1){
        flash.info = "Searching doctor ..."
        return $http.get('/api/doctors/', {
            params: {
                page: -1,
                q: val,
                type: 'radiologist'
            }
        }).then(function (response) {
            flash.info = ''
            return response.data.doctors.map(function (item) {
                return {
                    'name': item.name,
                    'id': item.id,
                    'specialty': item.specialty,
                    'city': item.city,
                    'country': item.country
                };
            });
        });
        //}
    };

    // add radiologist
    $scope.addRadiologistFromList = function (item, model) {
        $scope.order.radiologist = model.id;
    };

    // get order details

    flash.info = "Order loading...";
    $scope.loadBoxViewer = function (guid) {
        window.open("/api/dicom-image-viewer/" + guid, "_blank");
    };

    Order.get({guid: $stateParams.guid}, function success(data) {
        console.dir(data)

        $scope.order = data;
        if (data.radiologist) $scope.radioBtn.serviceType = 'radiologist';

        if (data.status == 0)
            $scope.method = "Add";
        else {
            $scope.method = "Edit";
        }

        // get Doctor list
        Doctor.query({
                page: -1,
            },
            function success(response) {
                $scope.order.doctors = response.doctors;

                // if ( $rootScope.provider_radiologist ){
                //     $scope.provider_radiologist = $rootScope.provider_radiologist
                //     $scope.order.provider_radiologist = $rootScope.provider.id
                // }
            });

        flash.info = "";
    });
    $scope.order.serviceType = 'provider';
    window.filter = $filter;
    // get doctor list for hospital
    // $scope.$watch('order.dicoms', function(){

    //     if($scope.order.dicoms){
    //         console.log("wathing order dicoms");
    //         if($scope.order.dicoms){
    //             //loadCount++;
    //             //if( loadCount == 2)
    //             loadStudyDicomView($('#testing'), $scope.order, $filter);
    //         }
    //     }

    // });

    var count = 0;
    $scope.showDicomViewer = function () {
        $scope.mainContainer = !$scope.mainContainer;
        if ($rootScope.cachedStudyGuid == undefined) {
            $rootScope.cachedStudyGuid = [];
        }
        ;
        if (count == 0) {
            loadStudyDicomView($('#testing'), $scope.order, $filter, Dicom);
            count++;
        }
        ;
        //window.dispatchEvent(new Event('resize'));
    }

    // dicoms file upload
    $scope.uploader = new FileUploader();

    // uploader options
    $scope.upload_options = {
        url: '/api/orders/dicom/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.guid}],
        autoUpload: true
    }


    $rootScope.uploader = $scope.uploader;

    $scope.uploader.onAfterAddingAll = function () {
        $scope.uploader.uploadAll();
    }

    $scope.uploader.onCompleteItem = function (item, response) {

        if (response.guid) {
            $scope.order = response;
            $location.hash('order-details');
        }
    }

    // // doctor signature file upload
    //
    // $scope.doctor_signature_uploader = new FileUploader();
    //
    // // uploader options
    // $scope.doctor_signature_upload_options = {
    //     url: '/api/doctor-signature/' ,
    //     headers: {
    //         'X-CSRFToken': $scope.token,
    //         'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''
    //
    //     },
    //     formData: [ { guid : $stateParams.guid }],
    //     autoUpload: true
    // };
    //
    // $scope.doctor_signature_uploader.onAfterAddingFile = function(){
    //     $scope.doctor_signature_uploader.uploadAll();
    // };
    //
    // $scope.doctor_signature_uploader.onCompleteItem = function(item, response){
    //     var index = $scope.doctor_signature_uploader.getIndexOfItem(item);
    //     $scope.doctor_signature_uploader.queue[index].data = response;
    //     console.log(item);
    //     console.log(response);
    //     $scope.order.doctor_signature = response.doctor_signature;
    // };
    //
    // $scope.removeDoctorSignature = function(item){
    //     flash.info = 'Deleting doctor signature ...';
    //     DoctorSignature.delete({id: item.data.id, guid: $stateParams.guid}, function success(data){
    //         flash.info = '';
    //         flash.success = 'Doctor signature successfully remove...';
    //     }, function error(err){
    //         flash.info = '';
    //         flash.success = 'Please try again later , some thing went wrong ..';
    //     });
    //     $scope.doctor_signature_uploader.removeFromQueue(item);
    //
    // };
    //
    //
    // $scope.removeDoctorSignatureByID = function(id, index){
    //     flash.info ='Deleting doctor signature ...';
    //     DoctorSignature.delete({id: id, guid: $stateParams.guid}, function success(data){
    //         flash.info = '';
    //         flash.success ='Doctor signature successfully remove...';
    //         $scope.order.doctor_signatures.splice(index, 1);
    //     }, function error(err){
    //         flash.info = '';
    //         flash.success ='Please try again later , some thing went wrong ..';
    //     });
    // };
    // $scope.doctor_signature_uploader.onAfterAddingFile = function(item){
    //     //debugger;
    // }


    $scope.submitOrder = function () {
        $scope.order.metas.StudyDate = getDateStr($scope.order.StudyDate)
        $scope.order.patient.date_of_birth = getDateStr($scope.order.date_of_birth)
        $scope.order.metas.StudyTime = getTimeStr($scope.order.StudyTime)

        if ($scope.radioBtn.serviceType == 'provider') {
            $scope.order.radiologist = null
        } else {
            $scope.order.provider = null
        }

        Order.update({guid: $stateParams.guid}, $scope.order, function success(response) {
                flash.success = "Order Added successfully";
                $state.transitionTo('dashboard.orders');
            },
            function err(error) {
                if (error.status == 400)
                    flash.error = error.data['message']
                else
                    flash.error = 'Somethings wrong please try again later';
            });
    }

    //  get all hospital list

    Hospital.query({page: -1}, function success(response) {
        $scope.hospitals = response.hospitals;
        //$scope.order.hospital = response.hospitals[0].id
    });

    //  get all provider list

    Provider.query({page: -1}, function success(response) {
        $scope.providers = response.providers;
        allProviders = response.providers;
        //$scope.order.provider = response.providers[0].id
    });


    // get doctor list for hospital
    $scope.$watch('order.hospital', function () {
        if ($scope.order.hospital) {
            console.log('I am in watch hospital %s', $scope.order.hospital);

            // check select hospital type
            if ($scope.hospitals) {
                $scope.selected_hospital_type = $scope.hospitals.filter(function (i) {
                    return i.id == $scope.order.hospital;
                });

                $scope.order.hospital_type = $scope.selected_hospital_type[0].hospital_type;
                var selectedHospital = $scope.hospitals.find(function (data) {
                    return $scope.order.hospital == data.id;
                });

                if ($scope.selected_hospital_type[0].providers != null) {
                    $scope.providers = $scope.selected_hospital_type[0].providers;
                } else {
                    $scope.providers = allProviders;
                }

                console.log("selectedHospital", selectedHospital);
                console.log("selected_hospital_type", $scope.selected_hospital_type);

                if ($scope.selected_hospital_type[0].hospital_type == 3) {
                    return;
                }
            }
            // search doctors for hospitals
            Doctor.query({'hospital': $scope.order.hospital, 'page': -1}, function success(response) {
                $scope.doctors = response.doctors;
                $scope.radiologists = response.radiologists
            });
        }
    });

    $scope.$watch('hospitals', function () {
        if ($scope.hospitals) {
            $scope.selected_hospital_type = $scope.hospitals.filter(function (i) {
                return i.id == $scope.order.hospital;
            });

            $scope.order.hospital_type = $scope.selected_hospital_type[0].hospital_type;
            var selectedHospital = $scope.hospitals.find(function (data) {
                return $scope.order.hospital == data.id;
            });

            if ($scope.selected_hospital_type[0].providers != null) {
                $scope.providers = $scope.selected_hospital_type[0].providers;
            } else {
                $scope.providers = allProviders;
            }

            console.log("selectedHospital", selectedHospital);
            console.log("selected_hospital_type", $scope.selected_hospital_type);

            if ($scope.selected_hospital_type[0].hospital_type == 3) {
                return;
            }
        }
    });

    // create/update report from viewer
    $scope.saveReportFromViewer = function(){
        flash.info = 'Report updating...';
        Order.update({
            guid: $stateParams.guid,
            type: 'viewer_report_update',
            report: $scope.order.report,
        }, function success(response){

            flash.info=''
            flash.success = "Your report has been changed."

        },function err(error){
            flash.info=''
            if( error.status == 400 )
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });

        return false;
    }

    // start routing
    $scope.startRouting = function () {
        Order.update({
            guid: $stateParams.guid,
            type: 'start_routing',
            status: 2
        }, function success(response) {

            $scope.order.status = response.status;
            $scope.order.status_text = response.status_text;

            flash.success = "Order routed successfully"

        }, function err(error) {
            if (error.status == 400)
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });

    }


    // Email to hospital

    $scope.emailToHospital = function () {
        if ($rootScope.user_group != 'hospital') {
            flash.info = "Sending Email to Hospital"
            OrderEmail.save({guid: $stateParams.guid}, function success(response) {
                flash.info = ''
                flash.success = "Email is successfully send to Hospital"
            }, function error(err) {
                flash.info = ''
                flash.error = err.data.error
            });
        } else {
            flash.info = "Not allowed to send email"
        }

    };


    // delete modal

    $scope.openDeleteModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'deleteModal.html',
            controller: orderDeleteModalCtrl,
            size: 'lg'
        });
    }


    // open sms modal
    $scope.openSMSModal = function (text, phone) {
        var modalInstance = $modal.open({
            templateUrl: 'smsModal.html',
            controller: smsModelCtrl,
            size: 'sm',
            resolve: {
                data: function () {
                    return {
                        'text': text,
                        'phone': phone
                    }
                }
            }
        });
    }

    // order delivered confirmation modal
    $scope.openOrderDeliveredModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'orderDelivered.html',
            controller: orderDeliveredModalCtrl,
            size: 'lg'
        });
    }

    // email this report
    $scope.openEmailReportModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'emailModal.html',
            controller: emailReportMoralCtrl,
            size: 'sm'
        });
    }

    // showReportModal
    $scope.showReportModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'viewReport.html',
            controller: viewReportModalCtrl,
            size: 'lg',
            resolve: {
                data: function () {
                    return {
                        'order': $scope.order
                    }
                }
            }
        });
    }


    // download all dicoms
    $scope.download_all_dicoms = function () {

        $scope.download_count = 0
        angular.forEach($scope.order.dicoms, function (dicom) {
            $scope.downloadURL($filter('AppStorage')(dicom.file))

        })
    }


    $scope.downloadURL = function (url) {
        var hiddenIFrameID = 'hiddenDownloader' + $scope.download_count++;
        var iframe = document.createElement('iframe');
        iframe.id = hiddenIFrameID;
        iframe.style.display = 'none';
        document.body.appendChild(iframe);
        iframe.src = url;
    }

    // change bill status

    $scope.changeBillStatus = function () {
        flash.info = "Changing billing status"
        Order.update({
            guid: $stateParams.guid,
            type: 'billing_status',
            billing_status: $scope.order.billing_status
        }, function success(response) {
            flash.info = ''
            flash.success = "Billing status changed successfully"

        }, function err(error) {
            flash.info = ''
            if (error.status == 400)
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });
    }

    window.test = $scope

    // getDisableStatus
    $scope.getDisableStatus = function () {
        return $scope.order.status >= 4 ? true : false;
    };

    // update provider radiologist
    $scope.addProviderRadiologists = function () {
        flash.info = "Add Provider Radiologists"
        Order.update({
            guid: $stateParams.guid,
            type: 'provider_radiologist',
            provider_radiologist: $scope.order.provider_radiologist
        }, function success(response) {
            flash.info = ''
            flash.success = "Provider Radiologists changed successfully"

        }, function err(error) {
            flash.info = ''
            if (error.status == 400)
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });
    };

    // add second opinion
    $scope.addSecondOpinion = function () {
        flash.info = "Adding Second Opinion"
        Order.update({
            guid: $stateParams.guid,
            type: 'second_provider',
            second_provider: $scope.order.second_provider
        }, function success(response) {
            flash.info = ''
            flash.success = "Second opinion Provider added successfully"

        }, function err(error) {
            flash.info = ''
            if (error.status == 400)
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });
    };

    $scope.showCreateSecondReport = false;

});

// Referring Doctor dashboard
angular.module('OrderApp.controllers').controller('DoctorsStudiesCtrl', function ($scope, $rootScope, $state, Order, flash, $filter, Activity) {

    // Check activity  notifications
    Activity.get({'notification': 1}, function success(data) {
        $rootScope.activities = data.activities
        $rootScope.activities.notification = data.meta.count
    })

    $scope = $rootScope
    console.log("Dr studies page");

    $scope.unread_list = Order.query({
            per_page: 4
        },
        function success(response) {
            flash.info = '';
        });

    $scope.read_list = Order.query({
            per_page: 4,
            read_status: 1
        },
        function success(response) {
            flash.info = '';
        });

});

// Referring Dr studies with read and unread category
angular.module('OrderApp.controllers').controller('DoctorStudiesCatCtrl', function ($scope, $rootScope, $stateParams, $state, Order, flash, $filter, Hospital, Activity) {

    // Check activity  notifications
    Activity.get({'notification': 1}, function success(data) {
        $rootScope.activities = data.activities
        $rootScope.activities.notification = data.meta.count
    })

    console.log("Dr Orders Page");

    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.q = undefined;
    $scope.status = undefined;
    $scope.priority = undefined

    if ($stateParams.category == 'unread') {
        var read_status = 0
        $scope.category = 'Unread'
    }
    else {
        var read_status = 1
        $scope.category = 'Read'
    }

    $scope.fetchResults = function () {
        flash.info = "Order loading..."
        $scope.results = Order.query({
                page: $scope.bigCurrentPage,
                q: $scope.q,
                status: $scope.status,
                read_status: read_status,
                priority: $scope.priority
            },
            function success(response) {
                flash.info = '';
                $scope.bigTotalItems = response.meta.count;
            });
    }

    $scope.fetchResults();

    $scope.$watch('status', function () {
        if ($scope.status != undefined)
            $scope.fetchResults();
    });

    console.groupEnd();

});

angular.module('OrderApp.controllers').controller('reportCtrl', function ($scope, $state, $stateParams, $cookieStore, reportService, reportFormatService, studyDetailService, Order, DoctorSignature, FileUploader, flash) {
    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.order = {};
    $scope.reportDetail = {};
    $scope.report = {
        clinical_information: ""
    };
    var clinical_information = [];

    Order.get({guid: $stateParams.order_guid}, function success(data) {
        $scope.order = data;
        flash.info = "";
    });

    // get report data
    if ($stateParams.report_guid) {
        reportService.query({guid: $stateParams.report_guid}, function (res) {
            if (res.success != undefined) {
                var data_html = res.success.data_html; // report data in html
                $scope.reportDetail = res.success;
                $scope.report.guid = res.success.guid;
                $scope.report.order_guid = res.success.order_guid;
                $scope.report.procedure = data_html.procedure;
                $scope.report.clinical_information = data_html.clinical_information;
                $scope.report.comparison = data_html.comparison;
                $scope.report.findings = data_html.findings;
                $scope.report.impression = data_html.impression;
                $scope.reportDetail.template = 1;
            } else if (res.error != undefined) {
                flash.error = res.error;
            }
        }, function (err) {
            console.log(err);
        });
    }

    // doctor's signature file upload
    $scope.doctor_signature_uploader = new FileUploader();

    // uploader options
    $scope.doctor_signature_upload_options = {
        url: '/api/doctor-signature/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.order_guid}],
        autoUpload: true
    };

    $scope.doctor_signature_uploader.onAfterAddingFile = function () {
        $scope.doctor_signature_uploader.uploadAll();
    };

    $scope.doctor_signature_uploader.onCompleteItem = function (item, response) {
        var index = $scope.doctor_signature_uploader.getIndexOfItem(item);
        $scope.doctor_signature_uploader.queue[index].data = response;
        $scope.order.doctor_signature = response.doctor_signature;
    };

    $scope.removeDoctorSignature = function (item) {
        flash.info = 'Deleting doctor signature ...';

        DoctorSignature.delete({id: item.data.id, guid: $stateParams.order_guid}, function success(data) {
            flash.info = '';
            flash.success = 'Doctor signature successfully remove...';
        }, function error(err) {
            flash.info = '';
            flash.success = 'Please try again later , some thing went wrong ..';
        });

        $scope.doctor_signature_uploader.removeFromQueue(item);
    };

    $scope.removeDoctorSignatureByID = function (id, index) {
        flash.info = 'Deleting doctor signature ...';

        DoctorSignature.delete({id: id, guid: $stateParams.order_guid}, function success(data) {
            flash.info = '';
            flash.success = 'Doctor signature successfully remove...';
            $scope.order.doctor_signatures.splice(index, 1);
        }, function error(err) {
            flash.info = '';
            flash.success = 'Please try again later , some thing went wrong ..';
        });
    };

    $scope.report_submit = function () {
        var report_data = "";

        try {
            if ($.trim($scope.report.procedure) != "") {
                var procedure_data = $scope.report.procedure.replace(/<p[^>]*>/g, '<br/>').replace(/<\/p>/g, '');
                report_data = "<div style='text-align: center;'><u><b>" + procedure_data + "</b></u></div>";
            }

            if ($.trim($scope.report.clinical_information) != "") {
                var clinical_data = $scope.report.clinical_information.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                report_data += "<div style='text-align: left;'><b>Clinical Information:</b></br>" + clinical_data + "</div>";
            }

            if ($.trim($scope.report.comparison) != "") {
                var comparison_data = $scope.report.comparison.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                report_data += "<div style='text-align: left;'><b>Comparison:</b></br>" + comparison_data + "</div>";
            }

            if ($.trim($scope.report.findings) != "") {
                var findings_data = $scope.report.findings.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                report_data += "<div style='text-align: left;'><b>Findings:</b></br>" + findings_data + "</div>";
            }

            if ($.trim($scope.report.impression) != "") {
                var impression_data = $scope.report.impression.replace(/<p[^>]*>/g, '').replace(/<\/p>/g, '</br>');
                report_data += "<div style='text-align: left;'><b>Impression:</b></br>" + impression_data + "</div>";
            }

            // report data
            var data = {
                "order_guid": $stateParams.order_guid,
                "report_data": {
                    "procedure": $scope.report.procedure,
                    "clinical_information": $scope.report.clinical_information,
                    "comparison": $scope.report.comparison,
                    "findings": $scope.report.findings,
                    "impression": $scope.report.impression,
                },
                "report_full_html": report_data,
            };

            // report_guid into data for report update
            if ($stateParams.report_guid) {
                data["guid"] = $stateParams.report_guid;
            }

            // save/update report data to report table
            // if report_guid in data then report will be updated for that guid
            reportService.create(data, function (res) {
                if (res.success != undefined) {
                    $state.go('dashboard.order', {guid: $stateParams.order_guid});
                    flash.success = res.success;
                }
                else if (res.error != undefined) {
                    flash.error = res.error;
                }
            }, function (err) {
                console.log(err);
            });
        } catch (e) {
            console.log(e);
        }
    };

    try {
        studyDetailService.query({guid: $stateParams.order_guid}, function (res) {
            if (res.success != undefined) {
                if (res.success.metas[9].value != undefined && res.success.metas[9].value != "None") {
                    $scope.report.clinical_information = res.success.metas[9].value.replace(/\n/g, "<br />");
                    clinical_information.push({clinical_information: $scope.report.clinical_information});
                }
            }
        }, function (err) {
            console.log(err.error);
        })
    } catch (e) {
        console.log(e);
    }

    $scope.oneAtATime = true;
    $scope.first = {
        open: true
    }

    $scope.$watch('first.open', function (isOpen) {
        if (!isOpen) {
            console.log('First group was opened');
        }
    });

    //   $scope.addNew = function() {
    //     $scope.groups.push({
    //       title: "New One Created",
    //       content: "Dynamically added new one",
    //       open: false
    //     });
    //   }

    $scope.reportTemplate = [{
        'name': 'Noraml Post Nasal Space',
        'report': '<strong>PNS </strong> ' +
        '<br /><br />' +
        '<p>Normal nasopharyngeal air column. </p>'
    }, {
        'name': 'NORMAL Abdominal X-ray',
        'report': '<strong>ABDOMEN</strong> ' +
        '<br /><br />' +
        '<p>Bowel gas pattern is normal.</p>' +
        '<p>Bowel loops are not dilated.</p>' +
        '<p>No extraluminal air is seen.</p>'
    }, {
        'name': 'NORMAL Abdominal X-ray',
        'report': '<strong>BOTH  ANKLES</strong>' +
        '<br /><br /><p>Normal ankle mortices.</p>' +
        '<p>No bone or joint lesion is seen.</p>'
    }, {
        'name': 'NORMAL BARIUM ENEMA',
        'report': '<strong>BARIUM ENEMA </strong>' +
        '<br /><br />' +
        '<strong>CONTROL FILM</strong>' +
        '<p>There is fullness of the abdominal flank.</p>' +
        '<p>There is abdominal distension evidenced by the fullness of the flanks.</p>' +
        '<p>There is enlargement of both renal bed.</p>' +
        '<br /><br />' +
        '<strong>ON INJECTION OF CONTRAST</strong>' +
        '<p>There is free retrograde flow of contrast outlining the normal colon.</p>' +
        '<p>No abnormal area of narrowing is noted.</p>' +
        '<p>Presacral space is within normal limit.</p>' +
        '<br /><br />' +
        '<p><strong>Impression:</strong> Essentially normal study.  </p>'
    }];

    $scope.selectReportTemplate = function () {
        $scope.reportDetail.report = $scope.reportTemplate[$scope.reportDetail.template]['report'];
    };

    $scope.bigCurrentPage = 1;
    $scope.maxSize = 20;

    try {
        reportFormatService.query({
            page: $scope.bigCurrentPage,
            per_page: $scope.maxSize,
            provider_id: 10
        }, function (res) {
            $scope.reports = res.reports;
        }, function (err) {
            console.log(err);
        });
    } catch (e) {
        console.log(e);
    }

    $scope.selectReport = function () {
        reportFormatService.query({
            report_id: $scope.reportId
        }, function (res) {
            $scope.report.procedure = res.report.procedure;
            $scope.report.clinical_information = res.report.clinical_information;
            $scope.report.comparison = res.report.comparison;
            $scope.report.findings = res.report.findings;
            $scope.report.impression = res.report.impression;

            if (clinical_information.length != 0) {
                $scope.report.clinical_information = clinical_information[0].clinical_information;
            }
        }, function (err) {
            console.log(err);
        });
    };

    $scope.$watch('report.procedure', function (data) {
        if (data != undefined) {
            var procedure;

            if (data.match(/<br>/g) != null) {
                procedure = data.replace(/<br>/g, '\n');
                $scope.report.procedure = procedure;
            }

            if (data.match(/&nbsp;/g) != null) {
                procedure = data.replace(/&nbsp;/g, ' ');
                $scope.report.procedure = procedure;
            }
        }
    });

    $scope.$watch('report.clinical_information', function (data) {
        if (data != undefined) {
            var clinical;

            if (data.match(/<br>/g) != null) {
                clinical = data.replace(/<br>/g, '\n');
                $scope.report.clinical_information = clinical;
            }

            if (data.match(/&nbsp;/g) != null) {
                clinical = data.replace(/&nbsp;/g, ' ');
                $scope.report.clinclinical_informationical = clinical;
            }
        }
    });

    $scope.$watch('report.comparison', function (data) {
        if (data != undefined) {
            var comparison;

            if (data.match(/<br>/g) != null) {
                comparison = data.replace(/<br>/g, '\n');
                $scope.report.comparison = comparison;
            }

            if (data.match(/&nbsp;/g) != null) {
                comparison = data.replace(/&nbsp;/g, ' ');
                $scope.report.comparison = comparison;
            }
        }
    });

    $scope.$watch('report.findings', function (data) {
        if (data != undefined) {
            var findings;

            if (data.match(/<br>/g) != null) {
                findings = data.replace(/<br>/g, '\n');
                $scope.report.findings = findings;
            }

            if (data.match(/&nbsp;/g) != null) {
                findings = data.replace(/&nbsp;/g, ' ');
                $scope.report.findings = findings;
            }
        }
    });

    $scope.$watch('report.impression', function (data) {
        if (data != undefined) {
            var impression;

            if (data.match(/<br>/g) != null) {
                impression = data.replace(/<br>/g, '\n');
                $scope.report.impression = impression;
            }

            if (data.match(/&nbsp;/g) != null) {
                impression = data.replace(/&nbsp;/g, ' ');
                $scope.report.impression = impression;
            }
        }
    });
});

var orderDeleteModalCtrl = function ($scope, $stateParams, $modalInstance, $state, flash, Order) {
    $scope.close = function () {
        $modalInstance.close();
    };
    $scope.deleteModel = function () {
        flash.info = 'Order deleting on process ...';
        $modalInstance.close();
        Order.delete({guid: $stateParams.guid}
            ,
            function success(response) {
                flash.success = response.message;
                $state.transitionTo('dashboard.orders');
            },
            function err(error) {
                if (error.status == 400)
                    flash.error = error.data['message']
                else
                    flash.error = 'Somethings wrong please try again later';
            }
        );
    };


};

var smsModelCtrl = function ($scope, $stateParams, $modalInstance, $state, flash, SMS, data) {


    $scope.data = data

    $scope.close = function () {
        $modalInstance.close();
    };

    $scope.sendSMS = function () {
        flash.info = 'SMS sending'
        SMS.save({
            phone: $scope.data.phone,
            message: $scope.data.message
        }, function success(response) {
            flash.info = ''
            flash.success = 'SMS successfully send';
            $modalInstance.close();
        }, function error(err) {
            flash.info = ''
            flash.error = err.data.error

        });

    }

};


var orderDeliveredModalCtrl = function ($scope, $stateParams, $modalInstance, $state, flash, Order) {
    $scope.close = function () {
        $modalInstance.close();
    };
    $scope.orderDelivered = function () {
        $modalInstance.close();

        Order.update({
            guid: $stateParams.guid,
            type: 'report_delivered',
            status: 6
        }, function success(response) {
            flash.success = "Report has been marked as delivered"
            $state.transitionTo('dashboard.orders');

        }, function err(error) {
            if (error.status == 400)
                flash.error = error.data['message']
            else
                flash.error = 'Somethings wrong please try again later';
        });
    };


};


var emailReportMoralCtrl = function ($scope, $stateParams, $modalInstance, $state, flash, OrderEmail) {

    $scope.email = ''
    $scope.close = function () {
        $modalInstance.close();
    };

    $scope.sendEmail = function () {
        flash.info = "Sending Email ..."
        OrderEmail.save({guid: $stateParams.guid, email: $scope.email}, function success(response) {
            flash.info = ''
            flash.success = "Email is successfully send"
            $modalInstance.close();
        }, function error(err) {
            flash.info = ''
            flash.error = err.data.error
        });
    };


};

var viewReportModalCtrl = function ($scope, $stateParams, $modalInstance, $state, flash, data, $filter, $sce) {

    $scope.order = data.order;

    $scope.close = function () {
        $modalInstance.close();
    };

    $scope.report_title = $scope.order.report_filename.split('/')[2];

    $scope.pdfURL = $filter('AppStorage')($scope.order.report_filename);

    $scope.sendEmail = function () {
        flash.info = "Sending Email ..."
        OrderEmail.save({guid: $stateParams.guid, email: $scope.email}, function success(response) {
            flash.info = ''
            flash.success = "Email is successfully send"
            $modalInstance.close();
        }, function error(err) {
            flash.info = ''
            flash.error = err.data.error
        });
    };

    $scope.secure_url = function(pad_url){
        return $sce.trustAsResourceUrl(pad_url);
    };

    window.test = $scope;
};


// return a valid date obj from string

function getDateObj(dateStr, chk_today) {
    //debugger
    if (dateStr != '') {

        var today = new Date();

        date = new Date(
            dateStr.substring(0, 4) + '-' + dateStr.substring(4, 6) + '-' + dateStr.substring(6, 8)
        )
        return date
        // if its today then return null string .
        if (chk_today && today.getFullYear() == date.getFullYear() && today.getMonth() == date.getMonth() && today.getDate() == date.getDate())
            return ''
        else
            return date

    }
    else
        return ''
}

// return 2 digit number
function n(n) {
    return n > 9 ? "" + n : "0" + n;
}
// return a valid Dicom date string from Date obj

function getDateStr(dateObj) {

    if (dateObj != '') {
        return dateObj.getFullYear().toString() + n(dateObj.getMonth() + 1) + n(dateObj.getDate())
    }
}


function getTimeObj(timeStr) {
    var d = new Date();
    if (timeStr != '') {
        d.setHours(parseInt(timeStr.substring(0, 2)));
        d.setMinutes(parseInt(timeStr.substring(2, 4)));
    }

    return d
}

function getTimeStr(dateObj) {
    if (dateObj != '') {
        return n(dateObj.getHours()) + n(dateObj.getMinutes()) + '00'
    }
}
