function loadStudyDicomView(studyViewer, data, $filter, Dicom){
    // Load the first series into the viewport
    $('#wadoURL').val();

    console.log("called loadStudyJson");
    
    $('#dicom-slider-input').jRange({from: 1, to: 30, step: 1, scale: [1,6,11,16,21,26,31],
        format: '%s', width: 200, showLabels: true, onstatechange: frameRateChanged});
    

    $('.dicom-btn-table td').on('click', gridItemSelected);
    $('.dicom-btn-table td').hover(gridIn, gridOut);
    createGrid(1,1);

    
    
    var dicomList = [];
    var currentStackIndex = 0;
    var currentLoadIndex = 1;
    var seriesIndex = 0;
    var imageLoadStatus = {};
    var totalImages = data.dicoms.length;
    var isPlaying = false;
    var dicomStack = {
        seriesDescription: data.metas.SeriesDescription,
        stackId : data.id,
        imageIds: [],
        imageGUIDs: [],
        annotationData: [],
        //seriesIndex : seriesIndex,
        currentImageIdIndex: 0,
        frameRate: data.frameRate
    }

    data.dicoms.forEach(function(image) {
        var imageId = image.file;
        if(imageId.substr(0, 4) !== 'http') {
            //imageId = "dicomweb://cornerstonetech.org/images/ClearCanvas/" + image.imageId;
            imageId = "dicomweb:" + $filter('AppStorage')(imageId);
        }
        dicomStack.imageIds.push(imageId);
        dicomStack.imageGUIDs.push(image.guid);
        dicomStack.annotationData.push(image.annotation_data);
    });
    
    
    // resize the parent div of the viewport to fit the screen
    var imageViewer = $(studyViewer).find('.imageViewer')[0];
    var viewportWrapper = $(imageViewer).find('.viewportWrapper')[0];
    var parentDiv = $(studyViewer).find('.viewer')[0];
    //viewportWrapper.style.width = (parentDiv.style.width - 60) + "px";
    //viewportWrapper.style.height= parentDiv.style.height + "px";
    
    // image enable the dicomImage element and activate a few tools
    var element = $(studyViewer).find('.viewport')[0];
    var parent = $(element).parent();
    var childDivs = $(parent).find('.overlay');
    var topLeft = $(childDivs[0]).find('div');
    $(topLeft[0]).text(data.patient.name);
    $(topLeft[1]).text(data.patient.patient_id);
    $(topLeft[2]).text(dicomStack.seriesDescription);
    $(topLeft[3]).text(data.metas.StudyDate);
    var studyInfoPanel = $('.dicom-sidebar-panel .panel-heading');
    $(studyInfoPanel[0]).text(data.patient.name);
    $(studyInfoPanel[1]).text(" dhisting dhisting");
    var nanobar = new Nanobar( {bg:'#15a9e1', className:'dicom-loadbar', target:document.getElementById('dicom-loadbar')} );
    $(element).attr("contentEditable", "true");
    $(element).contentEditable = true;
    
    function onNewImage(e, param) {
        // if we are currently playing a clip then update the FPS
        var playClipToolData = cornerstoneTools.getToolState(element, 'playClip');
        if(playClipToolData !== undefined && playClipToolData.data.length > 0 && 
            playClipToolData.data[0].intervalId !== undefined && param.frameRate !== undefined) {
            $(topLeft[4]).text("FPS: " + Math.round(param.frameRate));
        } else {
            if($(topLeft[4]).text().length > 0) {
                $(topLeft[4]).text("");
            }
        }
        $(topLeft[6]).text("Image #" + (dicomStack.currentImageIdIndex + 1) + "/" + dicomStack.imageIds.length);

        var toolState = dicomStack.annotationData[dicomStack.currentImageIdIndex];
        if (toolState !== null){
            cornerstoneTools.setAllToolState(element, toolState);
        }
    }
    $(element).on("CornerstoneNewImage", onNewImage);

    function onImageRendered(e, param) {
        //debugger
        $('#loading').hide();
        $(topLeft[7]).text("Zoom:" + param.viewport.scale.toFixed(2));
        $(topLeft[8]).text("WW/WL:" + Math.round(param.viewport.voi.windowWidth) + "/" + Math.round(param.viewport.voi.windowCenter));
        $(topLeft[5]).text("Render Time:" + param.renderTimeInMs + " ms");
    }
    $(element).on("CornerstoneImageRendered", onImageRendered);

    var imageId = dicomStack.imageIds[0];
    // image enable the dicomImage element
    cornerstone.enable(element);
    cornerstone.loadAndCacheImage(imageId).then(function(image) {
        cornerstone.displayImage(element, image);
        if(dicomStack.frameRate !== undefined) {
            cornerstone.playClip(element, dicomStack.frameRate);
        }
        cornerstoneTools.keyboardInput.enable(element);
        cornerstoneTools.mouseInput.enable(element);
        cornerstoneTools.mouseWheelInput.enable(element);
        cornerstoneTools.touchInput.enable(element);

        // Enable all tools we want to use with this element
        cornerstoneTools.wwwc.activate(element, 1); // ww/wc is the default tool for left mouse button
        cornerstoneTools.pan.activate(element, 2); // pan is the default tool for middle mouse button
        cornerstoneTools.zoom.activate(element, 4); // zoom is the default tool for right mouse button
        cornerstoneTools.text.enable(element);
        cornerstoneTools.select.enable(element);
        cornerstoneTools.probe.enable(element);
        cornerstoneTools.length.enable(element);
        cornerstoneTools.ellipticalRoi.enable(element);
        cornerstoneTools.rectangleRoi.enable(element);
        cornerstoneTools.wwwcTouchDrag.activate(element);
        cornerstoneTools.zoomTouchPinch.activate(element);

        // stack tools
        cornerstoneTools.addStackStateManager(element, ['playClip']);
        cornerstoneTools.addToolState(element, 'stack', dicomStack);
        cornerstoneTools.stackScrollWheel.activate(element);
        cornerstoneTools.stackPrefetch.enable(element);


        function disableAllTools()
        {
            cornerstoneTools.wwwc.disable(element);
            cornerstoneTools.pan.activate(element, 2); // 2 is middle mouse button
            cornerstoneTools.zoom.activate(element, 4); // 4 is right mouse button
            cornerstoneTools.text.deactivate(element, 1);
            cornerstoneTools.select.deactivate(element, 1);
            cornerstoneTools.probe.deactivate(element, 1);
            cornerstoneTools.length.deactivate(element, 1);
            cornerstoneTools.ellipticalRoi.deactivate(element, 1);
            cornerstoneTools.rectangleRoi.deactivate(element, 1);
            cornerstoneTools.stackScroll.deactivate(element, 1);
            cornerstoneTools.wwwcTouchDrag.deactivate(element);
            cornerstoneTools.zoomTouchDrag.deactivate(element);
            cornerstoneTools.panTouchDrag.deactivate(element);
            cornerstoneTools.stackScrollTouchDrag.deactivate(element);
        }

        var buttons = $(studyViewer).find('button');

        
        // Tool button event handlers that set the new active tool
        $(buttons[0]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.wwwc.activate(element, 1);
            cornerstoneTools.wwwcTouchDrag.activate(element);
        });
        $(buttons[1]).on('click touchstart',function() {
            disableAllTools();
            var viewport = cornerstone.getViewport(element);
            if(viewport.invert === true) {
                viewport.invert = false;
            }
            else {
                viewport.invert = true;
            }
            cornerstone.setViewport(element, viewport);
        });
        $(buttons[2]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.zoom.activate(element, 5); // 5 is right mouse button and left mouse button
            cornerstoneTools.zoomTouchDrag.activate(element);
        });
        $(buttons[3]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.pan.activate(element, 3); // 3 is middle mouse button and left mouse button
            cornerstoneTools.panTouchDrag.activate(element);
        });
        $(buttons[4]).on('click touchstart',function() {
            disableAllTools();
            cornerstoneTools.stackScroll.activate(element, 1);
            cornerstoneTools.stackScrollTouchDrag.activate(element);
        });
        $(buttons[5]).children().on('click touchstart',function() {
            
            
        });
        $(".annotations li a").click( function() {
            disableAllTools();
            var yourText = $(this).text();
            if(yourText == "Text"){
                cornerstoneTools.text.activate(element, 1);
            }else if(yourText == "Length"){
                cornerstoneTools.length.activate(element, 1);
            }else if (yourText == "Pixel Probe") {
                cornerstoneTools.probe.activate(element, 1);
            }else if (yourText == "Elliptical ROI") {
                cornerstoneTools.ellipticalRoi.activate(element, 1);
            }else if (yourText == "Rectangle ROI") {
                cornerstoneTools.rectangleRoi.activate(element, 1);
            };
            $(".dicom-viewer-dropdown").removeClass('open');
        });
        $(buttons[6]).on('click touchstart',function() {
            cornerstoneTools.select.activate(element, 1);
        });
        $(buttons[7]).on('click touchstart',function() {
            cornerstoneTools.text.clear(element);
            cornerstoneTools.length.clear(element);
            cornerstoneTools.probe.clear(element);
            cornerstoneTools.ellipticalRoi.clear(element);
            cornerstoneTools.rectangleRoi.clear(element);
            cornerstoneTools.angle.clear(element);
        });
        $(buttons[8]).on('click touchstart',function() {
            var frameRate = dicomStack.frameRate;
            if(frameRate === undefined) {
                dicomStack.frameRate = $('#dicom-slider-input').val() || 9;
            }
            isPlaying = true;
            cornerstoneTools.playClip(element, dicomStack.frameRate);
            $(buttons[8]).addClass("hidden");
            $(buttons[9]).removeClass("hidden");
        });
        $(buttons[9]).on('click touchstart',function() {
            isPlaying = false;
            cornerstoneTools.stopClip(element);
            $(buttons[9]).addClass("hidden");
            $(buttons[8]).removeClass("hidden");
        });
        $(buttons[10]).on('click touchstart',function() {
            disableAllTools();
            var annotationData = cornerstoneTools.getAllToolState(element);
            var dicomGuid = dicomStack.imageGUIDs[dicomStack.imageIds.indexOf(cornerstone.getEnabledElement(element).image.imageId)];
            Dicom.update({ guid: dicomGuid }, { 'annotation_data': annotationData}, function success(data){
                console.log('annotation saved', data);
            }, function error(err){
                console.log('annotation error', err);
            });
        });

        $(buttons[12]).on('click touchstart',function() {
            $(this).find(">:first-child").toggleClass("fa-toggle-off fa-toggle-on");
            $(".overlay").toggleClass("hidden");
        });


        var toolState = dicomStack.annotationData[0];
        if (toolState !== null) {
            cornerstoneTools.setAllToolState(element, toolState);
        }

        /*       rotation       */

        // HFlip
        $(buttons[13]).on('click touchstart',function() {

            var viewport = cornerstone.getViewport(element);
            viewport.hflip = !viewport.hflip;
            cornerstone.setViewport(element, viewport);

        });

        // VFlip
        $(buttons[14]).on('click touchstart',function() {

            var viewport = cornerstone.getViewport(element);
            viewport.vflip = !viewport.vflip;
            cornerstone.setViewport(element, viewport);

        });

        // Rotate Left
        $(buttons[15]).on('click touchstart',function() {

            var viewport = cornerstone.getViewport(element);
            viewport.rotation-=90;
            cornerstone.setViewport(element, viewport);

        });

        // Rotate Right
        $(buttons[16]).on('click touchstart',function() {
            var viewport = cornerstone.getViewport(element);
            viewport.rotation+=90;
            cornerstone.setViewport(element, viewport);

        });


        // Reset
        $(buttons[17]).on('click touchstart',function(e) {
            cornerstone.reset(element);

            console.log(this);
        });

        /*      rotation        */

        $(buttons[0]).tooltip();
        $(buttons[1]).tooltip();
        $(buttons[2]).tooltip();
        $(buttons[3]).tooltip();
        $(buttons[4]).tooltip();
        $(buttons[5]).tooltip();
        $(buttons[6]).tooltip();
        $(buttons[7]).tooltip();
        $(buttons[8]).tooltip();
        $(buttons[9]).tooltip();
        $(buttons[10]).tooltip();

        // ------------------ study started ---------------------
        var seriesList = $('.dicom-sidebar-panel-body');
        dicomStack.imageIds.forEach(function(stack) {
            seriesIndex++;
            var seriesEntry = '<a class="dicom-sidebar-inner-box" oncontextmenu="return false" ' +
                'imageId="' +stack + '"><div class="csthumbnail"' +
                '></div><div>Dicom #' + seriesIndex + '</div></a>';
            var seriesElement = $(seriesEntry).appendTo(seriesList);
            var thumbnail = $(seriesElement).find('div')[0];
            cornerstone.enable(thumbnail);
            cornerstone.loadAndCacheImage(stack).then(function(image) {
                if(seriesIndex === 0) {
                    $(seriesElement).addClass('active');
                }
                cornerstone.displayImage(thumbnail, image);
                var cnv = cornerstone.getEnabledElement(thumbnail).canvas;
                var img = new Image();
                img.crossOrigin = 'anonymous';
                img.src = cnv.toDataURL("image/png");
                $('<img src="'+img.src+'" />').appendTo(thumbnail);
                $(cnv).addClass('hidden');
            });
            
            $(seriesElement).on('click touchstart', function () {
                // make this series visible
                var activeThumbnails = $(seriesList).find('a').each(function() {
                    $(this).removeClass('active');
                });
                $(seriesElement).addClass('active');

                cornerstoneTools.stopClip(element);
                cornerstoneTools.stackScroll.disable(element);
                cornerstoneTools.stackScroll.enable(element, dicomStack, 0);
                cornerstone.loadAndCacheImage(stack).then(function(image) {
                    var defViewport = cornerstone.getDefaultViewport(element, image);
                    currentStackIndex = seriesIndex;
                    cornerstone.displayImage(element, image, defViewport);
                    cornerstone.fitToWindow(element);
                    var stackState = cornerstoneTools.getToolState(element, 'stack');
                    stackState.data[0] = dicomStack;
                    stackState.data[0].currentImageIdIndex = seriesIndex;
                    cornerstoneTools.stackPrefetch.enable(element);
                    //$(bottomLeft[1]).text("# Images: " + dicomStack.imageIds.length);

                    if(dicomStack.frameRate !== undefined) {
                        cornerstoneTools.playClip(element, dicomStack.frameRate);
                    }
                });
            });
        });
        // ---------------- study ended ---------------------------

        function resizeStudyViewer() {
            var studyRow = $(studyViewer).find('.studyContainer')[0];
            var height = '' + $(studyRow).height();
            var width = '' + $(studyRow).width();
            $(parentDiv).width(width);
            viewportWrapper.style.width = width + "px";
            viewportWrapper.style.height = height + "px";
            cornerstone.resize(element, true);
        }
        
        $(window).resize(function() {
            resizeStudyViewer();
        });
        resizeStudyViewer();
        timeout = setTimeout(resizeStudyViewer, 300);
    });
            
    $(cornerstone).on("CornerstoneImageLoadProgress", function(event, oProgress){
        imageLoadStatus[oProgress.imageId] = oProgress.percentComplete ;
        if (oProgress.imageId == imageId) {
            $('.dicom_percent').text(oProgress.percentComplete + '%');
        }else{
            var totalLoadedPercent = 0;
            for (var i in imageLoadStatus) {
                totalLoadedPercent += imageLoadStatus[i];
            };
            
            nanobar.go( totalLoadedPercent / totalImages );
        }
        
        if (oProgress.percentComplete === 100) { 
            currentLoadIndex++;
        }
    }); 

    function frameRateChanged(){
        if(cornerstone.getEnabledElement(element)){
            dicomStack.frameRate = $('#dicom-slider-input').val();
            if (isPlaying) {
                cornerstoneTools.stopClip(element);
                cornerstoneTools.playClip(element, dicomStack.frameRate);
            }
        }
    }

    $("#sidebarToggle").on('click touchstart',function() {
        $(".dicom-sidebar-container").toggleClass("hidden");
        $(".dicom-main-container").toggleClass("dicom-main-container-with-sidebar dicom-main-container-without-sidebar");
        $("#sidebarToggle > span:first-child").toggleClass("fa-arrow-right fa-arrow-left");
        $(window).trigger('resize');
    });


    function initiateDrag(){

        // make dragable
        $( ".dicom-sidebar-inner-box" ).draggable({ 
            revert: "invalid", 
            containment: "document",
            helper: "clone" });
        $( ".dicom-main-container .dicom-cell" ).droppable({
            drop: function( event, ui ) {
                var item = $( ui.draggable ).attr('imageId');
                var element = $(this).find('.viewport')[0];
                loadGridCell(element, item);
            }
        });
    }

    function loadGridCell(dicomCell, imageId){
        cornerstone.enable(dicomCell);
        cornerstone.loadAndCacheImage(imageId).then(function(image) {
            //$(dicomCell).addClass('selected');
            cornerstone.displayImage(dicomCell, image);
        });
    }

    function createGrid(row, col){
        var currentRows = $(".dicom-main-container > .rowContainer");
        var currentRowsLength = currentRows.length;
        var rowContainer = $('.rowParent');

        if (currentRowsLength < row){
            $(currentRows).removeClass('row' + currentRowsLength).addClass('row'+row);
            for (var i = row; i > currentRowsLength; i--) {
                rowContainer.clone(true)
                    .addClass('row'+row)
                    .removeClass('hidden rowParent')
                    .appendTo(".dicom-main-container");
            }
        }else if (currentRowsLength > row){
            $(currentRows).removeClass('row' + currentRowsLength).addClass('row'+row);
            for (var i = currentRowsLength - row; i > 0; i--) {
                $(".dicom-main-container").children().last().remove();
            }
        }

        var currentCols = $(currentRows[0]).children();
        var currentColsLength = $(currentCols).length;
        var columnContainer = $(".studyContainer");
        if (currentColsLength < col) {
            for (var j = col - currentColsLength; j > 0; j--) {
                columnContainer.clone(true)
                    .removeClass('hidden studyContainer')
                    .addClass('grid-col' + col)
                    .appendTo('.rowContainer');
            }
        }else if (currentColsLength > col) {
            for (var i = row-1; i >= 0; i--) {
                rowContainer = $(currentRows[i]).children();
                for (var j = currentColsLength; j >= col; j--) {
                    $(rowContainer[j]).remove();
                };
            };
        }
        $('.rowContainer div.dicom-cell').removeClass("grid-col" + currentColsLength).addClass('grid-col' + col);
        
        initiateDrag();
        $(window).trigger('resize');
    }

    function gridItemSelected(e){
        var position = $(this).data('val');
        var col = position % 10;
        var row = (position - col) / 10;
        createGrid(row, col);
    }

    function gridIn(e){
        var position = $(this).data('val');
        var col = position % 10;
        var row = (position - col) / 10;
        for (var i = row; i >= 0; i--) {
            //row
            for (var j = col; j >= 0; j--) {
                $('.dicom-btn-table tr:nth-child('+i+') td:nth-child('+j+')').css('background', '#8CAEC3');
            }
        }
    }

    function gridOut(e){
        $('.dicom-btn-table td').css('background', '#c8c8c8');
    }

};


