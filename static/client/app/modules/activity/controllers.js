'use strict';

angular.module('ActivityApp.controllers').controller('ActivitiesCtrl', function ($scope, $rootScope, $state, Activity) {

	// Check activity  notifications
    Activity.get({'notification':1}, function success(data){
        $rootScope.activities = data.activities
        $rootScope.activities.notification = data.meta.count
    })

    console.log("Activity page");
    $rootScope.activeTab = 'Activity'
    $scope.busy = false
    $scope.results = Activity.get()

    $scope.mark_as_read = function(guid, index){
    	$scope.results.activities[index].mark_as_read = true
    	Activity.update({
    			guid: guid
    		},{
    			'mark_as_read' : 1 
    		},
    		function success(data){
    			$rootScope.activities.notification = parseInt($rootScope.activities.notification)-1

    		},
    		function error(err){

    		}
    	);
    }

    $scope.page = 1;
    $scope.results_available = true;
    $scope.loadmore = function(){

		if( ! $scope.results_available ) return;

		Activity.get({ 'page': ++$scope.page }, function success(data){
			if(data.activities.length > 0){
				angular.forEach( data.activities, function(activity){
					$scope.results.activities.push(activity)		
				})	
			}else{
				$scope.results_available = false
			}
			
		})    	
    }


    window.test = $scope
});