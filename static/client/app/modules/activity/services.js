'use strict';

angular.module('ActivityApp.services').factory('Activity', function($resource){
    return $resource('/api/activities/:guid', { guid: '@guid'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});