'use strict';


angular.module('ActivityApp.services', [])
angular.module('ActivityApp.controllers', [])
angular.module('ActivityApp.directives', [])

angular.module('ActivityApp')
    .config(function ($stateProvider) {
        var templateDir = '/static/client/app/templates/activity/'
        $stateProvider
            .state('dashboard.activities', {
                url: "/activities",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'activities.html',
                        controller: 'ActivitiesCtrl'
                    }
                },
                access : [ 'admin', 'operator', 'hospital', 'doctor', 'provider']
            });
            //    .state('dashboard.activity', {
            //     url: "/hospital",
            //     views: {
            //         'dashboard-view': {
            //             templateUrl: templateDir + 'hospital.html',
            //             controller: 'HospitalCtrl'
            //         }
            //     },
            //     access : [ 'admin']
            // }).state('dashboard.edit_activity', {
            //     url: "/hospital/:guid",
            //     views: {
            //         'dashboard-view': {
            //             templateUrl: templateDir + 'hospital.html',
            //             controller: 'HospitalCtrl'
            //         }
            //     },
            //     access : [ 'admin']
            // });
    });
