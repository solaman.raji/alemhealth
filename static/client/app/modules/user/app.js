'use strict';


angular.module('UserApp.services', [])
angular.module('UserApp.controllers', [])
angular.module('UserApp.directives', [])

angular.module('UserApp')
    .config(function ($stateProvider) {
        var templateDir = '/static/client/app/templates/user/'
        $stateProvider
            .state('dashboard.user', {
                url: "/user",
                views: {
                    'dashboard-view': {
                        templateUrl:  templateDir + "user.html",
                        controller: 'UserCtrl'
                    }
                },
                access : [ 'admin' , 'operator', 'hospital', 'doctor','provider']
            }).state('dashboard.provider_settings', {
                url: "/provider-settings",
                views: {
                    'dashboard-view': {
                        templateUrl:  templateDir + "provider-settings.html",
                        controller: 'ProvidersSettingCtrl'
                    }
                },
                access : ['provider']
            }).state('dashboard.hospitals', {
                url: "/hospitals",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'hospitals.html',
                        controller: 'HospitalsCtrl'
                    }
                },
                access : [ 'admin']
            }).state('dashboard.hospital', {
                url: "/hospital",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'hospital.html',
                        controller: 'HospitalCtrl'
                    }
                },
                access : [ 'admin']
            }).state('dashboard.edit_hospital', {
                url: "/hospital/:guid",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'hospital.html',
                        controller: 'HospitalCtrl'
                    }
                },
                access : [ 'admin']
            }).state('dashboard.doctors', {
                url: "/doctors",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'doctors.html',
                        controller: 'DoctorsCtrl'
                    }
                },
                access : [ 'admin' , 'hospital', 'provider']
            }).state('dashboard.doctor', {
                url: "/doctor",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'doctor.html',
                        controller: 'DoctorCtrl'
                    }
                },
                access : [ 'admin', 'hospital', 'provider']
            }).state('dashboard.edit_doctor', {
                url: "/doctor/:guid",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'doctor.html',
                        controller: 'DoctorCtrl'
                    }
                },
                access : [ 'admin', 'hospital', 'provider']
            }).state('dashboard.providers', {
                url: "/provides",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'providers.html',
                        controller: 'ProvidersCtrl'
                    }
                },
                access : [ 'admin' , 'operator']
            }).state('dashboard.provider', {
                url: "/provider",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'provider.html',
                        controller: 'ProviderCtrl'
                    }
                },
                access : [ 'admin' , 'operator']
            }).state('dashboard.edit_provider', {
                url: "/provider/:guid",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'provider.html',
                        controller: 'ProviderCtrl'
                    }
                },
                access : [ 'admin' , 'operator']
            }).state('dashboard.operators', {
                url: "/operators",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'operators.html',
                        controller: 'OperatorsCtrl'
                    }
                },
                access : [ 'admin']
            }).state('dashboard.edit_operator', {
                url: "/operator/:guid",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'operator.html',
                        controller: 'OperatorCtrl'
                    }
                },
                access : ['admin']
            }).state('dashboard.operator', {
                url: "/operator",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + 'operator.html',
                        controller: 'OperatorCtrl'
                    }
                },
                access : ['admin']
            });
    });
