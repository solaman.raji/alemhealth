'use strict';
angular.module('UserApp.controllers').controller('baseCtrl', function ($scope, $rootScope, $state) {
    console.log("base ctrl")
});

angular.module('UserApp.controllers').controller('HomeCtrl', function ($scope, $rootScope, $state, Report) {
    console.log("Home page");
    $rootScope.activeTab = 'Home';


    // hide home page for provider, Doctor
    if (($rootScope.user_group == "doctor" || $rootScope.user_group == "provider") && $state.current.name == "dashboard.home") {
        $state.transitionTo("dashboard.orders");
    }
    // redirect broker user to there home page
//    if($rootScope.user_group == 'broker' )
//        $state.transitionTo('dashboard.broker_home');

    // total order

    $scope.totalOrderChart = {};

    $scope.totalOrderChart.data = {
        "cols": [
            {id: "t", label: "Date", type: "string"},
            {id: "s", label: "Orders", type: "number"}
        ]
    };

    $scope.totalOrderChart.type = 'ColumnChart'
    $scope.totalOrderChart.options = {
        'title': ''
    }


    // hospital pie chart
    $scope.hospitalChartObject = {};

    $scope.hospitalChartObject.data = {
        "cols": [
            {id: "t", label: "Facility", type: "string"},
            {id: "s", label: "Studies", type: "number"}
        ]
    };


    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
    $scope.hospitalChartObject.type = 'PieChart';
    $scope.hospitalChartObject.cssStyle = "height:500px; width:100%;",
        $scope.hospitalChartObject.options = {
            'title': ''
        }


    // pie chart
    $scope.chartObject = {};

    $scope.chartObject.data = {
        "cols": [
            {id: "t", label: "Topping", type: "string"},
            {id: "s", label: "Slices", type: "number"}
        ]
    };


    // $routeParams.chartType == BarChart or PieChart or ColumnChart...
    $scope.chartObject.type = 'PieChart';
    $scope.chartObject.cssStyle = "height:500px; width:100%;",
        $scope.chartObject.options = {
            'title': ''
        }


    // line chart

    $scope.lineChart = {};


    $scope.lineChart.data = {
        "cols": [
            {id: "t", label: "Date", type: "string"},
            {id: "s", label: "Hours", type: "number"}
        ]
    };


    $scope.lineChart.type = 'LineChart'
    $scope.lineChart.options = {
        'title': ''
    }


    $scope.report_date = {startDate: null, endDate: null};

    $scope.$watch('report_date', function (data) {

        $scope.totalOrderChart.data['rows'] = []
        Report.query({
            'type': 'total_order',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : '',
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : '',
        }, function success(data) {
            console.log(data)
            angular.forEach(data['reports'], function (key) {
                $scope.totalOrderChart.data.rows.push(
                    {
                        c: [
                            {v: key['date']},
                            {v: key['count']},
                        ]
                    }
                );

            });
        });


        $scope.hospitalChartObject.data["rows"] = []
        Report.query({
            'type': 'hospital',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : '',
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : '',

        }, function success(data) {

            angular.forEach(data['reports'], function (key) {
                $scope.hospitalChartObject.data.rows.push(
                    {
                        c: [
                            {v: key['hospital']},
                            {v: key['count']},
                        ]
                    }
                );

            });
        });
        $scope.chartObject.data["rows"] = []
        Report.query({
            'type': 'modality',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : '',
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : '',

        }, function success(data) {

            angular.forEach(data['reports'], function (key) {
                $scope.chartObject.data.rows.push(
                    {
                        c: [
                            {v: key['modality']},
                            {v: key['count']},
                        ]
                    }
                );

            });
        });


        $scope.lineChart.data['rows'] = []
        Report.query({
            'type': 'turn_around_time',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : '',
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : '',
        }, function success(data) {
            console.log(data)
            angular.forEach(data['reports'], function (key) {
                $scope.lineChart.data.rows.push(
                    {
                        c: [
                            {v: key['date']},
                            {v: key['avg']},
                        ]
                    }
                );

            });
        });
    })

    window.test = $scope
});


// ------------------------------------
// User Controllers
// ------------------------------------

angular.module('UserApp.controllers').controller('LoginCtrl', function ($scope, $rootScope, $state, $cookieStore, User, flash, Activity) {
    if ($cookieStore.get('user'))
        $state.transitionTo('dashboard.home')

    $scope.user = {}


    $scope.userlogin = function () {

        User.get({username: $scope.user.username, password: $scope.user.password},
            function success(data) {
                $cookieStore.put('user', data)

//                    // set the main dashboard ui router template url
//                    var  sub_domain = data.business.sub_domain;
//                    var patent_state = $state.get('dashboard');
//                    patent_state['templateUrl'] =  '/static/client/templates/' + sub_domain + '/' + patent_state['templateFile'];

                // top header
                $rootScope.username = $cookieStore.get('user').username;
                $rootScope.email = $cookieStore.get('user').email;
                $rootScope.user_firstname = $cookieStore.get('user').first_name
                //$rootScope.business_name = $cookieStore.get('user').business.name;

                flash.success = 'Welcome to Alemhealth.';
                $rootScope.user_group = $cookieStore.get('user').group;

                if ($cookieStore.get('user').group == "hospital")
                    $rootScope.hospital = $cookieStore.get('user').hospital;

                $state.transitionTo('dashboard.home');

//                if($rootScope.user_group == 'admin')
//                    $state.transitionTo('dashboard.home');
//                else
//                    $state.transitionTo('dashboard.broker_home');

                // Check activity
                Activity.get({'notification': 1}, function success(data) {
                    $rootScope.activities = data.activities
                    $rootScope.activities.notification = data.meta.count
                })

            },
            function err(error) {
                flash.error = 'Wrong username and password.';
            }
        );
    }
});


angular.module('UserApp.controllers').controller('LogoutCtrl', function ($scope, $cookieStore, $state, flash) {
    $cookieStore.remove('user');
    $state.transitionTo('login');
});


angular.module('UserApp.controllers').controller('UserCtrl', function ($scope, $rootScope, $state, $cookieStore, User, flash) {

    $scope.user = $cookieStore.get('user');
    console.log($scope.user)
    console.log("user page")

    $scope.saveUser = function () {

        if ($scope.user.new_password != "" && $scope.user.retype_password != "") {
            if ($scope.user.new_password == $scope.user.retype_password)
                $scope.user.password = $scope.user.new_password;
            else {
                flash.error = 'Password don\'t match';
                return false;
            }
        }

        User.update({username: $scope.user.username}, $scope.user,
            function success(response) {
                $cookieStore.put('user', response)

                // top header
                $rootScope.username = $cookieStore.get('user').username;


                flash.success = 'Acccounts update successfully';
                $state.transitionTo('dashboard.home')
            })
    }
});

angular.module('UserApp.controllers').controller('WebstoreCtrl', function ($scope, $rootScope, $state, $cookieStore, Business, flash) {
    $scope.business = Business.query({});

    $scope.saveBusiness = function () {
        Business.save($scope.business, function success(response) {
            console.log(response)
            $state.transitionTo('dashboard.home')

        }, function err(error) {

        })
    };


});


// ------------------------------------
// Hospital controllers
// ------------------------------------


angular.module('UserApp.controllers').controller('HospitalsCtrl', function ($scope, $state, Hospital, flash) {
    console.log("HospitalsCtrl");
    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.fetchResults = function () {
        flash.info = "Facility loading..."
        $scope.results = Hospital.query({
                page: $scope.bigCurrentPage
            },
            function success(response) {
                flash.info = ""
                $scope.bigTotalItems = response.meta.count;
            });
    }

    $scope.fetchResults();
});

angular.module('UserApp.controllers').controller('HospitalCtrl', function ($scope, $rootScope, $state, $stateParams, $cookieStore, flash, FileUploader, Hospital, Country, User, Provider) {

    // TODO :  need to delete the logo file while remove from front end
    $scope.hospital = {}
    $scope.hospital.user = {}
    $rootScope.activeTab = 'hospital'
    Provider.query({}, function (response) {
        $scope.providers = response.providers;
        console.log('provider List');
        console.log(response);
    });

    $scope.validEmail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/

    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.countries = Country.query({});

    User.query({type: 'hospital'}, function success(response) {
        $scope.users = response.users;
    });



    if ($stateParams.guid == undefined) {
        $scope.method = "Add"
        $scope.hospital.sendEmail = true
    } else {
        $scope.method = "Edit"
        $scope.hospital.sendEmail = false
        $scope.hospital = Hospital.get({guid: $stateParams.guid},
            function success(data) {
                $scope.temp_email = data.user.email;
                console.group("Hospital Info");
                console.log($scope.hospital.providers);
                console.groupEnd();
            },
            function error(err) {
                console.error(err);
            }
        );

    }

    // check email exist or not in blur
    $scope.checkEmail = function () {
        if ($scope.hospital.user.email != undefined) {

            if ($scope.temp_email && $scope.hospital.user.email == $scope.temp_email)
                return;

            User.get({email: $scope.hospital.user.email, check_email: true},
                function success(data) {
                },
                function error(err) {
                    flash.error = 'Sorry, email already exist';
                }
            )
        }

    }

    $scope.saveHospital = function () {
        console.group("saveHospital");
        console.group("hospital form");
        console.log($scope.hospital);
        console.groupEnd();
        if ($scope.method == 'Add') {

            Hospital.save($scope.hospital, function success(response) {
                console.group("response from api");
                console.log(response);
                console.groupEnd();
                flash.success = 'Facility add  successfully';
                $state.transitionTo('dashboard.hospitals')

            }, function error(err) {
                console.log(err)
                flash.error = err.data.message;
            });
        } else {
            Hospital.update($scope.hospital, function success(response) {
                console.log(response)
                flash.success = 'Facility update  successfully';
                $state.transitionTo('dashboard.hospitals')
            }, function error(err) {
                flash.error = err.data.message
            });
        }
        console.groupEnd();

    };

    // hospital logo  file upload

    $scope.logo_uploader = new FileUploader();

    // uploader options
    $scope.logo_uploader_options = {
        url: '/api/hospitals/logo/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.guid}],
        autoUpload: true
    }

    $scope.logo_uploader.onAfterAddingFile = function () {
        $scope.logo_uploader.uploadAll()
    }

    $scope.logo_uploader.onCompleteItem = function (item, response) {

        $scope.hospital.logo = response.logo
        $scope.hospital.logo_file = response.logo_file
    }


    window.test = $scope

});


// ------------------------------------
// Doctor controllers
// ------------------------------------


angular.module('UserApp.controllers').controller('DoctorsCtrl', function ($scope, $rootScope, $state, Doctor, flash) {
    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.hospital = undefined;
    if ($rootScope.hospital) {
        $scope.hospital = $rootScope.hospital.id;
    }

    $scope.$watch('search_key',function() {
        if($scope.search_key != undefined) {
            $scope.fetchResults();
        }
    });

    $scope.fetchResults = function () {
        flash.info = "Doctor loading...";
        $scope.results = Doctor.query({
                page: $scope.bigCurrentPage,
                hospital: $scope.hospital,
                search_key : $scope.search_key
            },
            function success(response) {
                flash.info = '';
                $scope.bigTotalItems = response.meta.count;
            });
    };

    $scope.fetchResults();
});

angular.module('UserApp.controllers').controller('DoctorCtrl', function ($scope, $rootScope, $state, $stateParams, $http, $cookieStore, flash, FileUploader, Hospital, Provider, Doctor, Country, User) {

    $rootScope.activeTab = 'doctor'
    $scope.token = $('input[name=csrfmiddlewaretoken]').val();

    $scope.doctor = {}

    Hospital.query({
            page: -1
        },
        function success(response) {
            $scope.hospitals = response.hospitals;
            if ($rootScope.hospital) {
                $scope.hospital = $rootScope.hospital
                $scope.doctor.hospital = $rootScope.hospital.id
            }

        });

    Provider.query({
            page: -1
        },
        function success(response) {
            $scope.providers = response.providers;
            if ($rootScope.provider) {
                $scope.provider = $rootScope.provider
                $scope.doctor.provider = $rootScope.provider.id
            }
        });

    $scope.countries = Country.query({});

    if ($stateParams.guid == undefined) {
        $scope.method = "Add"
        $scope.affiliatedHospital = false
    } else {
        $scope.method = "Edit"
        $scope.affiliatedHospital = false

        $scope.doctor = Doctor.get({guid: $stateParams.guid},
            function success(data) {
                console.log(data)
                if (!data.user) {
                    $scope.doctor.user = {}
                    $scope.doctor.user.first_name = data.name
                    $scope.doctor.user.email = ''
                } else {
                    $scope.temp_email = data.user.email
                }
                if (data.hospital)
                    $scope.affiliatedHospital = true
            },
            function error(err) {
                flash.error = err.data.message

            });

    }

    $scope.checkEmail = function () {
        if ($scope.doctor.user.email != undefined) {

            if ($scope.temp_email && $scope.doctor.user.email == $scope.temp_email)
                return;


            User.get({email: $scope.doctor.user.email, check_email: true},
                function success(data) {
                },
                function error(err) {
                    flash.error = 'Sorry, email already exist!';
                }
            )
        }

    }

    $scope.saveDoctor = function () {

        if ($scope.method == 'Add') {
            Doctor.save($scope.doctor, function success(response) {
                console.log(response)
                flash.success = 'Doctor add  successfully';
                $state.transitionTo('dashboard.doctors')

            }, function error(err) {
                flash.error = err.data.message
            });
        } else {
            if ($scope.affiliatedHospital == 'false')
                $scope.doctor.hospital = -1

            Doctor.update({guid: $stateParams.guid}, $scope.doctor, function success(response) {
                console.log(response)
                flash.success = 'Doctor updated  successfully';
                $state.transitionTo('dashboard.doctors')

            }, function error(err) {
                flash.error = err.data.message

            });
        }

    }


    // auto complete Doctors
    $scope.getDoctorsSpecialty = function (val) {
        //if (val.length() > 1){
        flash.info = "Searching doctor specialty ..."
        return $http.get('/api/doctors-specialty/', {
            params: {
                page: -1,
                q: val
            }
        }).then(function (response) {
            flash.info = ''
            return response.data.doctor_specialies.map(function (item) {
                return {
                    'name': item.name
                };
            });
        });
        //}
    };


    // doctor signature  upload

    $scope.logo_uploader = new FileUploader();

    // uploader options
    $scope.logo_uploader_options = {
        url: '/api/doctors/signature/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.guid}],
        autoUpload: true
    }

    $scope.logo_uploader.onAfterAddingFile = function () {
        $scope.logo_uploader.uploadAll()
    }

    $scope.logo_uploader.onCompleteItem = function (item, response) {

        $scope.doctor.signature = response.signature
        $scope.doctor.signature_file = response.signature_file
    }

    window.test = $scope

});


// ------------------------------------
// Provider controllers
// ------------------------------------
angular.module('UserApp.controllers').controller('ProvidersCtrl', function ($scope, $state, Provider, flash) {
    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.fetchResults = function () {
        flash.info = "Provider loading..."
        $scope.results = Provider.query({
                page: $scope.bigCurrentPage
            },
            function success(response) {
                flash.info = ""
                $scope.bigTotalItems = response.meta.count;
            });
    }

    $scope.fetchResults();
});

// settings for provider 
angular.module('UserApp.controllers').controller('ProvidersSettingCtrl', function ($scope, $timeout, $state, flash, Provider) {
    console.log("provider setting");

    // getting current doctor list
    Provider.query({'doctor_list': 1}, function success(response) {
        $timeout(function () {
            $scope.provider = response.providers;
        }, 0);

    }, function err(response) {
        console.log(response)
    });


    $scope.saveRadiologist = function () {

        Provider.update($scope.provider, function success(response) {
            flash.success = 'Provider update  successfully';
        }, function error(err) {
            flash.error = 'Something went wrong.';
        });
    };

    window.test = $scope;
});

angular.module('UserApp.controllers').controller('ProviderCtrl', function ($scope, $state, $rootScope, $stateParams, $cookieStore, $modal, flash, FileUploader, Provider, Country, User) {

    $rootScope.activeTab = 'provider'
    // TODO :  need to delete the logo file while remove from front end
    $scope.provider = {}
    $scope.provider.user = {};

    $scope.validEmail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/
    $scope.token = $('input[name=csrfmiddlewaretoken]').val();
    $scope.countries = Country.query({});

    User.query({type: 'provider'}, function success(response) {
        $scope.users = response.users;
    })


    if ($stateParams.guid == undefined) {
        $scope.method = "Add"
    } else {
        $scope.method = "Edit"
        flash.info = "Provider loading..."
        $scope.provider = Provider.get({guid: $stateParams.guid}, function success(response) {
            flash.info = ""

        }, function error(err) {

        });

    }

    $scope.saveProvider = function () {

        if ($scope.method == 'Add') {
            Provider.save($scope.provider, function success(response) {
                console.log(response)
                flash.success = 'Provider add  successfully';
                $state.transitionTo('dashboard.providers')

            }, function error(err) {

            });
        } else {
            Provider.update($scope.provider, function success(response) {
                console.log(response)
                flash.success = 'Provider update  successfully';
                $state.transitionTo('dashboard.providers')
            }, function error(err) {

            });
        }

    }

    // check email exist or not in blur
    $scope.checkEmail = function () {
        if ($scope.provider.user.email != undefined) {

            if ($scope.temp_email && $scope.provider.user.email == $scope.temp_email)
                return;

            User.get({email: $scope.provider.user.email, check_email: true},
                function success(data) {
                },
                function error(err) {
                    flash.error = 'Sorry, email already exist!';
                }
            )
        }
    }

    // provider logo  file upload

    $scope.logo_uploader = new FileUploader();

    // uploader options
    $scope.logo_uploader_options = {
        url: '/api/providers/logo/',
        headers: {
            'X-CSRFToken': $scope.token,
            'Authentication': $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''

        },
        formData: [{guid: $stateParams.guid}],
        autoUpload: true
    }

    $scope.logo_uploader.onAfterAddingFile = function () {
        $scope.logo_uploader.uploadAll()
    }

    $scope.logo_uploader.onCompleteItem = function (item, response) {

        $scope.provider.logo = response.logo
        $scope.provider.logo_file = response.logo_file
    }


    // delete modal

    $scope.openDeleteModal = function () {
        var modalInstance = $modal.open({
            templateUrl: 'deleteModal.html',
            controller: providerDeleteModalCtrl,
            size: 'lg'
        });
    }


    window.test = $scope

});


var providerDeleteModalCtrl = function ($scope, $stateParams, $modalInstance, $state, flash, Provider) {
    $scope.close = function () {
        $modalInstance.close();
    };

    $scope.deleteModel = function () {
        $modalInstance.close();
        Provider.delete({guid: $stateParams.guid}
            ,
            function success(response) {
                flash.success = response.message;
                $state.transitionTo('dashboard.providers');
            },
            function err(error) {
                if (error.status == 400)
                    flash.error = error.data['message']
                else
                    flash.error = 'Somethings wrong please try again later';
            }
        );
    };


};


// ------------------------------------
// Operator controllers
// ------------------------------------
angular.module('UserApp.controllers').controller('OperatorsCtrl', function ($scope, $state, Operator, flash) {
    $scope.maxSize = 10;
    $scope.bigCurrentPage = 1;

    $scope.fetchResults = function () {
        flash.info = "Operators loading..."
        $scope.results = Operator.query({
                page: $scope.bigCurrentPage
            },
            function success(response) {
                flash.info = ""
                $scope.bigTotalItems = response.meta.count;
            });
    }

    $scope.fetchResults();
});


angular.module('UserApp.controllers').controller('OperatorCtrl', function ($scope, $rootScope, $state, $stateParams, flash, Hospital, Operator, User) {

    $rootScope.activeTab = 'operator'
    $scope.operator = {}
    $scope.validEmail = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/


    $scope.multiSelectOptions = {enableSearch: true, dynamicTitle: false};


    $scope.operator.test = false
    Hospital.query({
            page: -1
        },
        function success(response) {
            $scope.hospitals = response.hospitals;
            // if ( $rootScope.hospital ){
            //     //$scope.hospital = $rootScope.hospital
            //     //$scope.doctor.hospital = $rootScope.hospital.id
            // }

        });

    //$scope.countries = Country.query({});

    if ($stateParams.guid == undefined) {
        $scope.method = "Add"
        $scope.operator.hospitals = []
        $scope.operator.sendEmail = true
    } else {
        $scope.method = "Edit"
        $scope.operator.sendEmail = false
        $scope.operator = Operator.get({guid: $stateParams.guid},
            function success(data) {
                $scope.temp_email = data.email
                if (!data.hospitals) {
                    $scope.operator.hospitals = []
                }
            },
            function error(err) {
            });

    }

    // check email exist or not in blur
    $scope.checkEmail = function () {
        if ($scope.operator.email != undefined) {

            if ($scope.temp_email && $scope.operator.email == $scope.temp_email)
                return;


            User.get({email: $scope.operator.email, check_email: true},
                function success(data) {
                },
                function error(err) {
                    flash.error = 'Sorry, email already exist!';
                }
            )
        }

    }

    $scope.saveOperator = function () {

        if ($scope.operator.hospitals.length == 0) {
            flash.error = 'Please add hospital';
            return;
        }

        if ($scope.method == 'Add') {

            Operator.save($scope.operator, function success(response) {
                console.log(response)
                flash.success = 'Operator add  successfully';
                $state.transitionTo('dashboard.operators')

            }, function error(err) {
                flash.error = 'Sorry, Please try again later ..';
            });
        } else {

            Operator.update({guid: $stateParams.guid}, $scope.operator, function success(response) {
                console.log(response)
                flash.success = 'Operator updated  successfully';
                $state.transitionTo('dashboard.operators')

            }, function error(err) {
                flash.error = 'Sorry, Please try again later ..';
            });
        }

    }
    window.test = $scope

});
