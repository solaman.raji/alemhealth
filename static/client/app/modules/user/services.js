'use strict';

angular.module('UserApp.services').factory('User', function($resource){
    return $resource('/api/users/:username', { username: '@username'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});

angular.module('UserApp.services').factory('Hospital', function($resource){
    return $resource('/api/hospitals/:guid', { guid: '@guid'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});

angular.module('UserApp.services').factory('Operator', function($resource){
    return $resource('/api/operators/:guid', { guid: '@guid'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});

angular.module('UserApp.services').factory('Provider', function($resource){
    return $resource('/api/providers/:guid', { guid: '@guid'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});

angular.module('UserApp.services').factory('Doctor', function($resource){
    return $resource('/api/doctors/:guid', { guid: '@guid'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});

// angular.module('UserApp.services').factory('DoctorSpeciality', function($resource){
//     return $resource('/api/doctor-specialty/', { }, {
//             query : { method: 'GET' },
//             update : { method: 'PUT' }
//         }
//     );
// });

angular.module('UserApp.services').factory('Country', function($resource){
    return $resource('/api/countries/', {}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});




// not use models list


angular.module('UserApp.services').factory('Image', function($resource){
    return $resource('/api/images/:guid', {guid: '@guid'}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});

angular.module('UserApp.services').factory('Business', function($resource){
    return $resource('/api/business/', {}, {
            query : { method: 'GET' },
            update : { method: 'PUT' }
        }
    );
});