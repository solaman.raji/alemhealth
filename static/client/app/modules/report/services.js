'use strict';

angular.module('ReportApp.services').factory('AllReports', function($resource){
    return $resource('/api/all-reports/', {},{
            query : { method: 'GET' }
        }
    );
});
