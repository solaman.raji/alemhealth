'use strict';

angular.module('ReportApp.services',[])
angular.module('ReportApp.controllers',[])
angular.module('ReportApp.directives', [])

angular.module('ReportApp').config(function($stateProvider){
    var templateDir = '/static/client/app/templates/report/';

    $stateProvider.state('dashboard.allreports',{
      url : "/all-reports",
      views : {
        'dashboard-view' : {
          controller: 'ReportCtrl',
          templateUrl : templateDir + "reffered.html"
        }
      },
      access : ['admin']

    })
    .state('dashboard.referred',{
        url : "/referred-doctors-reports",
        views : {
          'dashboard-view' : {
            controller: 'AllReportCtrl',
            templateUrl : templateDir + "referred-reports.html"
          }
        },
        access : ['admin']

  })
  .state('dashboard.genderbreakdown',{
      url : "/gender-breakdown",
      views : {
        'dashboard-view' : {
          controller: 'GenderBreakdownCtrl',
          templateUrl : templateDir + "gender-breakdown.html"
        }
      },
      access : ['admin']
})
.state('dashboard.hospitalstudy',{
  url : "/hospital-study",
  views : {
    'dashboard-view': {
      controller: 'HospitalStudyCtrl',
      templateUrl: templateDir + "hospital-study.html"

    }
  },
  access : ['admin']
})
.state('dashboard.modalitystudy',{
  url: "/modaility-study",
views: {
  'dashboard-view': {
    controller: 'ModalityStudyCtrl',
    templateUrl: templateDir + "modality-study.html"
  }
},
})

 ;

})
