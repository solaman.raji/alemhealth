'use strict';
angular.module('ReportApp.controllers').controller('ReportCtrl', function ($scope, $rootScope, $state) {
    console.log("Report ctrl")
});


angular.module('ReportApp.controllers').controller('ModalityStudyCtrl', function ($scope, $rootScope, $state, AllReports) {
    console.log("Modality ctrl")

    $scope.report_date = {startDate: null, endDate: null};
    $scope.$watch('report_date', function (data) {

        // todays report
        var from = moment().format('YYYY-MM-DD') + " 0:0:0";
        var to = moment().format('YYYY-MM-DD') + " 23:0:0";

        $scope.all_reports = AllReports.query({
            'type': 'modalitystudy',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : from,
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : to,
        }, function success(data) {
            console.log(data)
            //console.log(AllReports)
        });
    });
    window.test = $scope;


});


angular.module('ReportApp.controllers').controller('HospitalStudyCtrl', function ($scope, $rootScope, $state, AllReports) {
    console.log("HospitalStudy ctrl")


    $scope.report_date = {startDate: null, endDate: null};
    $scope.$watch('report_date', function (data) {

        // todays report
        var from = moment().format('YYYY-MM-DD') + " 0:0:0";
        var to = moment().format('YYYY-MM-DD') + " 23:0:0";
        //debugger;

        $scope.all_reports = AllReports.query({
            'type': 'hospitalstudy',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : from,
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : to
        }, function success(data) {
            console.log(data)
            //console.log(AllReports)
        });

    });
    window.test = $scope;


});


angular.module('ReportApp.controllers').controller('GenderBreakdownCtrl', function ($scope, $rootScope, $state, AllReports) {
    console.log("Gender breakdown ctrl")


    $scope.year = 2016;
    $scope.$watch('year', function (data) {
        //var Months = ["January","February","March","April","May","June",
        //            "July","August","September","October","November","December"]


        $scope.genderBreakdownObject = AllReports.query({
            'type': 'gender_breakdown',
            'year_select': $scope.year,
        }, function success(data) {
            console.log(data);
            $scope.genderBreakdownObject = {}
            $scope.genderBreakdownObject.type = 'ColumnChart'
            $scope.genderBreakdownObject.options = {
                'title': 'Gender Breakdown'
            };
            $scope.genderBreakdownObject.data = {};
            $scope.genderBreakdownObject.data = {
                'cols': [
                    {id: "mo", label: "Months", type: "string"},
                    {id: "o", label: "Male", type: "number"},
                    {id: "f", label: "Female", type: "number"}
                ],
                'rows': []
            };
            var i = 0;
            angular.forEach(data['reports'], function (key) {
                console.log(key);
                $scope.genderBreakdownObject.data.rows.push(
                    {c: [{v: key['month']}, {v: key['male']}, {v: key['female']}]}
                );
                i++;
            });
        });
    });

    window.test = $scope;

});


angular.module('ReportApp.controllers').controller('AllReportCtrl', function ($scope, $rootScope, $state, AllReports) {
    console.log(AllReports);
    console.log("We are geeting All report Controller");


    $scope.report_date = {startDate: null, endDate: null};
    $scope.$watch('report_date', function (data) {

        // todays report
        var from = moment().format('YYYY-MM-DD') + " 0:0:0";
        var to = moment().format('YYYY-MM-DD') + " 23:0:0";

        $scope.all_reports = AllReports.query({
            'type': 'reportlist',
            'start_date': $scope.report_date.startDate != null ? moment($scope.report_date.startDate).format('YYYY-MM-DD') : from,
            'end_date': $scope.report_date.endDate != null ? moment($scope.report_date.endDate).format('YYYY-MM-DD') : to,
        }, function success(data) {
            console.log(data)
            //console.log(AllReports)
        });
    });
    window.test = $scope;

});
