'use strict';

// Define all your modules with no dependencies

//Common app
angular.module('CommonApp.directives', [])
angular.module('CommonApp', [
    'CommonApp.directives'
]);

angular.module('CommonApp.filters', [])
angular.module('CommonApp', [
    'CommonApp.filters'
]);

//User app
angular.module('UserApp', [
    'ui.router',
    'ngResource',
    'angular-flash.service',
    'angular-flash.flash-alert-directive',
    'googlechart',
    'ui.checkbox',
    'angularjs-dropdown-multiselect',
    'ui.bootstrap',
    'daterangepicker',

    'UserApp.services',
    'UserApp.controllers',
    'UserApp.directives'
]);

// Order app
angular.module('OrderApp', [
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'angular-flash.service',
    'angular-flash.flash-alert-directive',
    'infinite-scroll',
    'textAngular',
    'ui.checkbox',
    'angularjs-dropdown-multiselect',
    'flow',  // not using
    'angularFileUpload',
    'angular-loading-bar',
    'cfp.loadingBar',
    'ngSanitize',
    
    'OrderApp.services',
    'OrderApp.controllers',
    'OrderApp.directives',

]);


//Activity app
angular.module('ActivityApp', [
    'ui.router',
    'ngResource',
    'angular-flash.service',
    'angular-flash.flash-alert-directive',
    'googlechart',
    'ui.checkbox',
    'angularjs-dropdown-multiselect',
    'infinite-scroll',

    'ActivityApp.services',
    'ActivityApp.controllers',
    'ActivityApp.directives'
]);


angular.module('ReportApp', [
    'ui.router',
    'ngResource',
    'ui.bootstrap',
    'angular-flash.service',
    'googlechart',
    'angular-flash.flash-alert-directive',
    'infinite-scroll',
    'textAngular',
    'ui.checkbox',
    'angularjs-dropdown-multiselect',
    'flow',  // not using
    'angularFileUpload',
    'angular-loading-bar',
    'cfp.loadingBar',
    'ngSanitize',
    //'ngTable',
    'ReportApp.services',
    'ReportApp.controllers',
    'ReportApp.directives',

]);


// Lastly, define your "main" module and inject all other modules as dependencies
angular.module('alemhealth', [
    'CommonApp',
    'UserApp',
    'OrderApp',
    'ActivityApp',
    'ReportApp',
    'ngResource',
    'ui.router',
    'ngCookies'
]);

// main alemhealth App

angular.module('alemhealth')
    .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {

        var templateDir = '/static/client/app/templates/'

        $stateProvider
            .state('dashboard', {
                url: "",
                abstract: true,
                templateUrl: templateDir + "base.html",
                authenticate: true,
                controller: function($scope, $rootScope, $state, Activity){
                    $scope.mark_as_read = function(guid, index){
                        $scope.activities.splice(index, 1)
                        $rootScope.activities.notification = parseInt($rootScope.activities.notification)-1
                        Activity.update({
                                guid: guid
                            },{
                                'mark_as_read' : 1
                            },
                            function success(data){



                            },
                            function error(err){

                            }
                        );

                        return false;
                    }
                }
            }).state('dashboard.home', {
                url: "/home",
                views: {
                    'dashboard-view': {
                        controller: 'HomeCtrl',
                        templateUrl: templateDir + "user/home.html"

                    }
                },
                authenticate: true
            }).state('dashboard.broker_home', {
                url: "/broker-home",
                views: {
                    'dashboard-view': {
                        //controller: 'BrokerHomeCtrl',
                        templateUrl: templateDir + "user/broker-home.html"

                    }
                },
                authenticate: true
            }).state('login', {
                url: "/login",
                templateUrl: templateDir + "user/login.html",
                controller: 'LoginCtrl',
                authenticate: false
            }).state('logout', {
                url: "/logout",
                controller: 'LogoutCtrl',
                authenticate: true
            }).state('dashboard.page-not-found', {
                url: "/page-not-found",
                views: {
                    'dashboard-view': {
                        templateUrl: templateDir + "user/page-not-found.html"
                    }
                }
            });

        $urlRouterProvider.otherwise("/home");


        $httpProvider.interceptors.push('httpRequestInterceptor');
        $httpProvider.interceptors.push('httpResponseInspector');


    }
);

angular.module("alemhealth").factory('httpRequestInterceptor', function ($cookieStore ) {
    return {
        request: function (config) {
            if(config.url.match('^\/static.+html$') != null){
                config.url = config.url + '?' + $("#static_guid").val()
            }

            config.headers['Authentication'] = $cookieStore.get('user') ? $cookieStore.get('user').access_key : ''
            config.headers['X-CSRFToken'] = $('input[name=csrfmiddlewaretoken]').val()

            return config;
        }
    };
});

angular.module("alemhealth").factory('httpResponseInspector', function ($q, $cookieStore) {
    return {
        response: function (response) {
            // do something on success
//            if(response.headers()['content-type'] === "application/json; charset=utf-8"){
//                // Validate response if not ok reject
//                var data = examineJSONResponse(response); // assumes this function is available
//
//                if(!data)
//                    return $q.reject(response);
//            }
            return response;
        },
        responseError: function (response) {
            // do something on error
            if (response.data.code == '403')
                $cookieStore.remove('user');
            return $q.reject(response);
        }
    };
});


angular.module("alemhealth")
    .run(function ($rootScope, $state, $cookieStore, $cookies, $http, Activity) {
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {

            // if($cookieStore.get('user') == undefined){
            //     $state.transitionTo("login");
            //     event.preventDefault();

            // }
            // access check here
            if ($cookieStore.get('user') && toState.access) {

                var access = false;
                for (var i = 0; i < toState.access.length; i++) {
                    if ($rootScope.user_group == toState.access[i]) {
                        access = true
                    }
                }

                

                if (access == false) {
                    $state.transitionTo("dashboard.page-not-found");
                    event.preventDefault();
                }

            }

            if (toState.authenticate && !$cookieStore.get('user')) {
                // User isn’t authenticated
                $state.transitionTo("login");
                event.preventDefault();
            }


        });

        if ($cookieStore.get('user')) {
            $rootScope.email = $cookieStore.get('user').email;
            $rootScope.username = $cookieStore.get('user').username;
            $rootScope.user_group = $cookieStore.get('user').group;
            $rootScope.user_firstname = $cookieStore.get('user').first_name
            if($cookieStore.get('user').group=="hospital")
                $rootScope.hospital = $cookieStore.get('user').hospital;
        }


        // Check activity
        Activity.get({'notification':1}, function success(data){
            $rootScope.activities = data.activities
            $rootScope.activities.notification = data.meta.count
        })

        $rootScope.activeTab = ''

    });
