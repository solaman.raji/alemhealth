var App = function () {
  var config = {//Basic Config
  };
  var voice_methods = [];
      function toggleSideBar(_this){
        var b = $("#sidebar-collapse")[0];
        var w = $("#cl-wrapper");
        var s = $(".cl-sidebar");
        
        if(w.hasClass("sb-collapsed")){
          $(".fa",b).addClass("fa-long-arrow-left").removeClass("fa-long-arrow-right");
          w.removeClass("sb-collapsed");
        }else{
          $(".fa",b).removeClass("fa-long-arrow-left").addClass("fa-long-arrow-right");
          w.addClass("sb-collapsed");
        }
        //updateHeight();
      }
        
  return {
   
    init: function (options) {
      //Extends basic config with options
      $.extend( config, options );
      /*Small devices toggle*/
      $(".cl-toggle").click(function(e){
        var ul = $(".cl-vnavigation");
        ul.slideToggle(300, 'swing', function () {
        });
        e.preventDefault();
      });

      /*Collapse sidebar*/
      $("#sidebar-collapse").click(function(){

          toggleSideBar();
          $("#sb-collapsed").animate('slow','linear');
      });


      /*SubMenu hover */
        var tool = $("<div id='sub-menu-nav' style='position:fixed;z-index:9999;'></div>");

      $(window).resize(function(){
        //updateHeight();
      });

      /*Return to top*/
      var offset = 220;
      var duration = 500;
      var button = $('<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>');
      button.appendTo("body");

      jQuery(window).scroll(function() {
        if (jQuery(this).scrollTop() > offset) {
            jQuery('.back-to-top').fadeIn(duration);
        } else {
            jQuery('.back-to-top').fadeOut(duration);
        }
      });

      jQuery('.back-to-top').click(function(event) {
          event.preventDefault();
          jQuery('html, body').animate({scrollTop: 0}, duration);
          return false;
      });

    },
      
    /*Pages Javascript Methods*/
    toggleSideBar: function(){
      toggleSideBar();
    },

  };
 
}();

$(function(){
  //$("body").animate({opacity:1,'margin-left':0},500);
  $("body").css({opacity:1,'margin-left':0});
});