from django.contrib.auth.models import User, Group


def create_groups():
    Group(name='operator').save()
    Group(name='doctor').save()
    Group(name='hospital').save()


if __name__ == '__main__':
    create_groups()