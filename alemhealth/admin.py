from django.contrib import admin
from django.contrib.auth.models import User
from django.contrib.auth.admin import UserAdmin
from alemhealth.models import *

admin.site.unregister(User)

class UserProfileInline(admin.StackedInline):
    model = UserProfile

class UserProfileAdmin(UserAdmin):
    inlines = [ UserProfileInline ]

admin.site.register(User, UserProfileAdmin)

class CurrencyAdmin(admin.ModelAdmin):
    readonly_fields = ['updated_at']

admin.site.register(Currency, CurrencyAdmin)
admin.site.register(Hospital)
admin.site.register(AhFile)
admin.site.register(Provider)
admin.site.register(Patient)
admin.site.register(DoctorSpeciality)


from django.contrib.admin import SimpleListFilter
from django.contrib.auth.models import Group
class GroupListFilter(SimpleListFilter):
    title = 'group'
    parameter_name = 'group'
    def lookups(self, request, model_admin):
        groups = Group.objects.all()
        items = ()
        for group in groups:
            items += ((str(group.id), str(group.name),),)
        return items

    def queryset(self, request, queryset):
        group_id = request.GET.get(self.parameter_name, None)
        if group_id:
            return queryset.filter(groups=group_id)
        return queryset


from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

class MyUserAdmin(UserAdmin):
    list_filter = UserAdmin.list_filter + (GroupListFilter,)

admin.site.unregister(User)
admin.site.register(User, MyUserAdmin)