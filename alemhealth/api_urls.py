from django.conf.urls import url
from order.api import *
import alemhealth
from alemhealth.api import *

urlpatterns = [
    #  alemhealth api  urls

    url(r'^api/users/?$', UsersView.as_view()),
    #url(r'^api/users/(?P<username>\w+)?$', UserView.as_view()),

    url(r'^api/login$', LoginView.as_view()),
    url(r'^api/logout$', LogoutView.as_view()),

    url(r'^api/hospitals/?$', HospitalsView.as_view()),
    url(r'^api/hospitals/logo/?$', HospitalsLogoView.as_view()),
    url(r'^api/hospitals/(?P<guid>[^/]+)?$', HospitalView.as_view()),

    url(r'^api/providers/?$', ProvidersView.as_view()),
    url(r'^api/providers/(?P<guid>[^/]+)?$', ProviderView.as_view()),
    url(r'^api/providers/logo/?$', ProviderLogoView.as_view()),

    url(r'^api/doctors/?$', DoctorsView.as_view()),
    url(r'^api/doctors/signature/?$', DoctorSignatureView.as_view()),
    url(r'^api/doctors/(?P<guid>[^/]+)?$', DoctorView.as_view()),
    url(r'^api/doctors-specialty/?$', DoctorSpecilitiesView.as_view()),

    url(r'^api/countries/?$', alemhealth.api.country_list),
    url(r'^api/users/(?P<username>([\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{1,4})?\w+)?$', UserView.as_view()),

    url(r'^api/operators/?$', OperatorsView.as_view()),
    url(r'^api/operators/(?P<guid>[^/]+)?$', OperatorView.as_view()),

    url(r'^api/individual-radiologists/?$', IndividualRadiologistList.as_view())
]
