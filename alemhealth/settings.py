"""
Django settings for alemhealth project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '%+jx$#gqxh+@mrzqi2*+7yszztu9q4_&rtme5jsr3a4(aw7kb*'

# client secret key for API
CLIENT_SECRET_KEY = 'AH-cfb618dd24b34810ae56b07dc8bb7e2d'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = False

ALLOWED_HOSTS = [
    '192.168.33.33',
    'cloud-dev.local',
    'test-platform-mumbai-01.cloud.alemhealth.com',
    'dev-platform-01.cloud.alemhealth.com'
    'dev-telerad-portal.cloud.alemhealth.com',
    'prod-telerad-portal.cloud.alemhealth.com',
    'portal.alem.health',
]

# CORS_ORIGIN_WHITELIST = ALLOWED_HOSTS[:] + ['localhost:3000']

# Application definition

DEFAULT_APPS = (
    'bootstrap_admin',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
)

THIRD_PARTY_APPS = (
    'djcelery',
    # 'django_user_agents',
    'csvimport.management',
    'django_extensions',
    'corsheaders',
)

LOCAL_APPS = (
    'alemhealth',
    'order',
    'report',
    'activity',
)

INSTALLED_APPS = DEFAULT_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'alemhealth.urls'

WSGI_APPLICATION = 'alemhealth.wsgi.application'

# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'alemhealth',
        'USER': 'root',
        'PASSWORD': '!qweqwe@#'
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/


#STATIC_ROOT = os.path.join(BASE_DIR, 'static')

# URL prefix for static files.
# Example: "http://media.lawrence.com/static/"
STATIC_URL = '/static/'
STATIC_ROOT = '/var/www/alemhealth/static'


# Additional locations of static files
STATICFILES_DIRS = (
    #STATIC_ROOT,
    os.path.join(BASE_DIR, "static"),
    #os.path.join(STATIC_ROOT, 'admin'),
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
)

STATICFILES_FINDERS = (
    'django.contrib.staticfiles.finders.FileSystemFinder',
    'django.contrib.staticfiles.finders.AppDirectoriesFinder',
    #'django.contrib.staticfiles.finders.DefaultStorageFinder',
)

MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['templates'],
        'APP_DIRS': False,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'loaders': [
                'django.template.loaders.filesystem.Loader',
                'django.template.loaders.app_directories.Loader',
            ]
        },
    },
]

#-------------------
# twilio api
#-------------------

TWILIO_ACCOUNT_SID = "ACa75adfa8df01c851b6c45700ccf46b28"
TWILIO_AUTH_TOKEN = "ab0b1e6b98151b2b1dcdee4244707933"


#-------------------
# mandrill api
#-------------------

MANDRILL_ACCESS_KEY ='gQkpIi3e1FF6Evq81tO1DA'


# ------------------------------
## celery configuration
# ------------------------------

BROKER_URL = 'amqp://guest:guest@localhost:5672/'

# celery beat for scheduling
CELERYBEAT_SCHEDULER = 'djcelery.schedulers.DatabaseScheduler'
CELERY_TIMEZONE ='Asia/Kabul'

# -------------------------
## logging
# -------------------------
#
# LOGGING = {
#     'version': 1,
#     'formatters': {
#         'verbose': {
#             'format': '%(levelname)s %(asctime)s %(module)s %(process)d %(thread)d %(message)s'
#         },
#         'simple': {
#             'format': '%(levelname)s %(message)s'
#         },
#     },
#     'handlers': {
#         'file': {
#             'level': 'DEBUG',
#             'class': 'logging.FileHandler',
#             'formatter': 'simple',
#             'filename': BASE_DIR + '/log/debug.log'
#         },
#         'null': {
#             'level': 'DEBUG',
#             'class': 'logging.NullHandler',
#         },
#         'console': {
#             'level': 'DEBUG',
#             'class': 'logging.StreamHandler',
#             'formatter': 'simple',
#
#         }
#     },
#     'loggers': {
#         'django': {
#             'handlers': ['file'],
#             'propagate': True,
#             'level': 'DEBUG',
#         }
#     }
# }

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '[%(asctime)s] %(levelname)s [%(pathname)s:%(lineno)d] %(message)s',
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': '/var/log/alemhealth/platform.log',
            'mode': 'a',
            'maxBytes': 50 * 1024 * 1024,  # 50 MB LOG FILE SIZE
            'backupCount': 5,
            'encoding':None,
            'delay' : 0,
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'alemhealth': {
            'handlers': ['file'],
            'propagate': True,
            'level': 'DEBUG',
        },
        'django.request': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
    }
}


# dcmtk configs


STORESCU_PARAMS = (
        # '--propose-uncompr',
        # '--propose-little',
        # '--propose-big',
        # '--propose-implicit',
        # '--propose-lossless',
        # '--propose-jpeg8',
        # '--propose-jpeg12',
        # '--propose-j2k-lossless',
        # '--propose-j2k-lossy',
        # '--propose-jls-lossless',
        # '--propose-jls-lossy',
        # '--propose-mpeg2',
        # '--propose-mpeg2-high',
        # '--propose-rle',
        # '-xv',
        # '-xw',
#        '--log-config %s/dcmtk_log_config.txt' % BASE_DIR,
        #'--debug',
)

# aws default settings
USE_S3 = False
AWS_STORAGE_BUCKET_NAME = 'alemhealth-local'


VIEWER_INFO = None

DCM4CHE_VIEWER_INFO = None

PUBLIC_DCM4CHEE_VIEWER = None

# Local settings
if os.path.isfile(BASE_DIR + '/alemhealth/local_settings.py'):
    from local_settings import *

# django development environment



# django celery start

try:
    import djcelery
    djcelery.setup_loader()

except ImportError:
    pass


# create alemhealth temp directory
ah_temp_dir = '/tmp/alemhealth'
if not os.path.exists(ah_temp_dir):
    os.makedirs(ah_temp_dir)


# generate GUID for static

import uuid
STATIC_GUID = uuid.uuid1().hex

