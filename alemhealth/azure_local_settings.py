# AWS access keys for S3
USE_S3 = True
AWS_ACCESS_KEY_ID = 'AKIAIPO26XCYN4YN3TFA'  # 'AKIAJIHNP2PTKVCGKBQA'
AWS_SECRET_ACCESS_KEY = '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'  # 'iOMypZsZQdr4iTS8Jwy8HIuIWDs6htRhU7r7/m1z'
AWS_STORAGE_BUCKET_NAME = 'alemhealth-prod'

S3_URL = 'https://%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

MEDIA_DIRECTORY = '/media/'

AZURE_ACCOUNT_NAME = 'portal15storage'
AZURE_ACCOUNT_KEY = '7fteEEvvwBPYmQ6qrvx4JLcoI7oaKdPMeqMs2Q1MkWq3f8ZHGoITwl0V5D3B1XfAKJ16kraCFobpFgRPBGfnig=='
AZURE_CONTAINER = 'alemhealth-prod'


if USE_S3:
    DEFAULT_FILE_STORAGE = 'alemhealth.s3utils.MediaRootS3BotoStorage'
    MEDIA_URL = S3_URL + MEDIA_DIRECTORY

PROD_DB_HOST = 'dev-platform-db-01.azr.alem.health'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'alemhealth',
        'USER': 'manager@dev-platform-db-02',
        'HOST': PROD_DB_HOST,
        'PASSWORD': 'alemchealth#2018'
    }
}

# Use private IP address as rabbitMQ will not be accessed from outside EC2
BROKER_HOST = 'dev-platform-celery-01.azr.alem.health'
BROKER_URL = 'amqp://alemhealth:alem2018health@%s:5672/' % BROKER_HOST

AH_ENV = 'prod'

ALLOWED_HOSTS = [
    'localhost',
    'cloud.alemhealth.com',  # For clients
    '172.31.19.155',  # For ELB Health check (prod-nginx-01)
    '172.31.25.8',  # For ELB Health check (prod-nginx-02)
    'prod-platform-nginx-01.cloud.alemhealth.com',
    'prod-platform-nginx-02.cloud.alemhealth.com',

    'dev-platform-nginx-01.azr.alem.health',
    'platform.azr.alem.health',
    'portal.azr.alem.health',
    'prod-platform-elb-1935050458.ap-southeast-1.elb.amazonaws.com',  # If anyone access through ELB

    'dev-telerad-portal.cloud.alemhealth.com',
    'prod-telerad-portal.cloud.alemhealth.com',
    'portal.alem.health',
]


VIEWER_INFO = {
    "base_url": "http://alem-viewer.azr.alem.health",
    "domain": "platform.azr.alem.health",
    "path": "/alemhubviewer",
}

DCM4CHE_VIEWER_INFO = {
    "host": "dcm4che-viewer-1.azr.alem.health",
    "port": "11112",
    "aet": "DCM4CHEE",
}

PUBLIC_DCM4CHEE_VIEWER = 'http://dicom-viewer.azr.alem.health'
