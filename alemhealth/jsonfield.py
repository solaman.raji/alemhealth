import logging
import json
from django.db import models

logger = logging.getLogger('alemhealth')

class JSONField(models.TextField):
    """
    JSONField is a generic textfield that neatly serializes/unserializes
    JSON objects seamlessly.
    Django snippet #1478

    example:
        class Page(models.Model):
            data = JSONField(blank=True, null=True)


        page = Page.objects.get(pk=5)
        page.data = {'title': 'test', 'type': 3}
        page.save()
    """

    def get_prep_value(self, value):
        logger.debug("Inside get prep value: %s" % value)
        # return super(JSONField, self).get_prep_value(value)
        return value

    def to_python(self, value):
        if value == "":
            return None

        try:
            if isinstance(value, basestring):
                value_dict = json.loads(value)
                logger.debug('Json field to python: %s -> %s' % (value, value_dict))
                return value_dict
        except ValueError:
            pass
        logger.debug('Json field to python: %s -> %s' % (value, value))
        return value

    def get_db_prep_save(self, value, *args, **kwargs):
        logger.debug('Json field get_db_prep_save: %s -> ' % (value))
        if value == "":
            return None
        if isinstance(value, dict):
            value = json.dumps(value)

        logger.debug('Json field get_db_prep_save: -> %s' % (value))
        return super(JSONField, self).get_db_prep_save(value, *args, **kwargs)

    def south_field_triple(self):
        try:
            from south.modelsinspector import introspector
            cls_name = '{0}.{1}'.format(
                self.__class__.__module__,
                self.__class__.__name__)
            args, kwargs = introspector(self)
            return cls_name, args, kwargs
        except ImportError:
            pass
