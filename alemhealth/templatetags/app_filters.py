from django import template
from django.conf import settings


register = template.Library()


@register.filter(name='appStorage')
def appStorage(value):

    bucket = settings.AWS_STORAGE_BUCKET_NAME
    if settings.USE_S3:
        media_url = 'https://s3-ap-southeast-1.amazonaws.com/' + bucket + '/media/'
    else:
        media_url = 'media/'
    return media_url + str(value)


@register.simple_tag(name='currency')
def currency(value, is_left, sign):

    if(is_left):
        return sign + ' ' + str(round(value, 2))
    else:
        return str(round(value, 2)) + ' ' + sign
