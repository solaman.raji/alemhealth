import mandrill
from django.conf import settings
from alemhealth.utils import *

import logging
logger = logging.getLogger('alemhealth')


def user_registration_email(user, password):

    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)

        logger.info('Sending user registration email to ' + user.email)
        # send new user registration email
        message = {
            'from_email': 'team@alemhealth.com',
            'from_name': 'AlemHealth',
            'headers': {'Reply-To': 'noreply@alemhealth.com'},
            'html': " Dear " + user.first_name + ' ' + user.last_name + ",<br/><br/>"
                    "You have been assigned an account on AlemHealth, here are your access details,<br/> <br/>"
                    "email : " + user.email + "<br/>"
                    "password : " + password + "<br/>"
                    "Signin link  : https://cloud.alemhealth.com <br/><br/>"
                    "Team AlemHealth<br/><br/>",
            'important': True,
            'subject': 'AlemHealth Registration',
            'tags': ['password-resets'],
            'to': [{'email': user.email, 'name': user.first_name + user.last_name }],
        }
        result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool', send_at='')

        if result[0]['status'] != "sent" and result[0]['status'] != "queued" :
            return 501
        print result 

    except mandrill.Error, e:
        logger.error(str(e))
        return 501
    
    return 200



def user_reset_email(user, password):

    try:
        mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)
        logger.info('Sending reset user info email to ' + user.email)
        # send reset user registration email
        message = {
            'from_email': 'team@alemhealth.com',
            'from_name': 'AlemHealth',
            'headers': {'Reply-To': 'noreply@alemhealth.com'},
            'html': "Dear " + user.first_name + ' ' + user.last_name + ",<br/><br/>"
                    "Your AlemHealth access has been reset. Here are your new access details <br/> <br/>"
                    "email : " + user.email + "<br/>"
                    "password : " + password + "<br/>"
                    "Signin link  : https://cloud.alemhealth.com <br/><br/>"
                    "Team AlemHealth<br/><br/>",

            'important': True,
            'subject': 'Alemhealth Access Reset',
            'tags': ['password-resets'],
            'to': [{'email': user.email, 'name': user.first_name + user.last_name }],
        }
        result = mandrill_client.messages.send(message=message, async=False, ip_pool='Main Pool', send_at='')

        if result[0]['status'] != "sent" and result[0]['status'] != "queued" :
            return 501
        print result 

    except mandrill.Error, e:
        logger.error(str(e))
        return 501
    
    return 200
