# AWS access keys for S3
USE_S3 = True
AWS_ACCESS_KEY_ID = 'AKIAIPO26XCYN4YN3TFA'  # 'AKIAJIHNP2PTKVCGKBQA'
AWS_SECRET_ACCESS_KEY = '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'  # 'iOMypZsZQdr4iTS8Jwy8HIuIWDs6htRhU7r7/m1z'
AWS_STORAGE_BUCKET_NAME = 'alemhealth-prod'

S3_URL = 'https://%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

MEDIA_DIRECTORY = '/media/'

if USE_S3:
    DEFAULT_FILE_STORAGE = 'alemhealth.s3utils.MediaRootS3BotoStorage'
    MEDIA_URL = S3_URL + MEDIA_DIRECTORY

PROD_DB_HOST = 'ah-prod-01.cl6dtc2x2soy.ap-southeast-1.rds.amazonaws.com'
# BACKUP_DB_HOST = 'migration-test-alemhealth-2017-08-28.cl6dtc2x2soy.ap-southeast-1.rds.amazonaws.com'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'alemhealth',
        'USER': 'root',
        'HOST': PROD_DB_HOST,
        'PASSWORD': '!qweqwe#$2015'
    }
}

# Use private IP address as rabbitMQ will not be accessed from outside EC2
BROKER_HOST = 'prod-platform-celery-01.cloud.alemhealth.com'
BROKER_URL = 'amqp://alemhealth:alem2018health@%s:5672/' % BROKER_HOST

AH_ENV = 'prod'

ALLOWED_HOSTS = [
    'localhost',
    'cloud.alemhealth.com',  # For clients
    '172.31.19.155',  # For ELB Health check (prod-nginx-01)
    '172.31.25.8',  # For ELB Health check (prod-nginx-02)
    'prod-platform-nginx-01.cloud.alemhealth.com',
    'prod-platform-nginx-02.cloud.alemhealth.com',
    'prod-platform-elb-1935050458.ap-southeast-1.elb.amazonaws.com',  # If anyone access through ELB

    'dev-telerad-portal.cloud.alemhealth.com',
    'prod-telerad-portal.cloud.alemhealth.com',
    'portal.alem.health',
]


VIEWER_INFO = {
    "base_url": "http://alem-viewer.cloud.alemhealth.com",
    "domain": "cloud.alemhealth.com",
    "path": "/alemhubviewer",
}

DCM4CHE_VIEWER_INFO = {
    "host": "dcm4che-viewer-1.cloud.alemhealth.com",
    "port": "11112",
    "aet": "DCM4CHEE",
}


PUBLIC_DCM4CHEE_VIEWER = 'https://listener.alem.health'
