from django.shortcuts import render_to_response, render
from django.template import RequestContext

from django.template.loader import render_to_string
from django.http import HttpResponse
import xhtml2pdf.pisa as pisa

from django.template.loader import get_template
from django.template import Context
from django.conf import settings

import os, logging

# from rlextra.rml2pdf import rml2pdf
# import cStringIO

from order.models import *



def home(request):

#    facebook = Pyfb(FACEBOOK_APP_ID)
#    return HttpResponseRedirect(facebook.get_auth_code_url(redirect_uri=FACEBOOK_REDIRECT_URL))
    data = {
        'test': "Its test"
    }
    # return render_to_response(
    #     'home.html', data, RequestContext(request))
    return render(request, 'home.html', context=data)

def dashboard(request):
    from order.tasks import *
    guid = '2d96dbc5f6aa4a9ba7338ef9830ad963'
    # provider_report_status.delay(guid)
    # send_dicom_to_provider.delay(guid)

    from order.emails import *
    # provider_remainder_email(Order.objects.get(guid=guid))

    data = {
        'AWS_STORAGE_BUCKET_NAME': settings.AWS_STORAGE_BUCKET_NAME,
        'USE_S3': settings.USE_S3,
        'STATIC_GUID': settings.STATIC_GUID,
    }

    # return render_to_response(
    #     'dashboard/index.html', data, RequestContext(request))

    return render(request, 'dashboard/index.html', context=data)


def dicom(rquest):

    #!/usr/bin/python
    """
    Storage SCU example.

    This demonstrates a simple application entity that support the RT Plan
    Storage SOP Class as SCU. For this example to work, there must be an
    SCP listening on the specified host and port.

    For help on usage,
    python storescu.py -h
    """

    import sys
    import argparse
    from netdicom import AE, StorageSOPClass, VerificationSOPClass
    from dicom.UID import ExplicitVRLittleEndian, ImplicitVRLittleEndian, ExplicitVRBigEndian
    from dicom import read_file

    remote_host = '192.168.0.104'
    remote_port = 11112
    remote_aet = 'MacBooks-MacBook'

    dicom_file = settings.MEDIA_ROOT + '/jamila.dcm'


    # parse commandline
    # parser = argparse.ArgumentParser(description='storage SCU example')
    # parser.add_argument('remotehost')
    # parser.add_argument('remoteport', type=int)
    # parser.add_argument('file', nargs='+' )
    # parser.add_argument('-aet', help='calling AE title', default='PYNETDICOM')
    # parser.add_argument('-aec', help='called AE title', default='REMOTESCU')
    # parser.add_argument('-implicit', action='store_true', help='negociate implicit transfer syntax only', default=False)
    # parser.add_argument('-explicit', action='store_true', help='negociate explicit transfer syntax only', default=False)
    #
    # args = parser.parse_args()
    #
    # if args.implicit:
    #     ts = [ImplicitVRLittleEndian]
    # elif args.explicit:
    #     ts = [ExplicitVRLittleEndian]
    # else:
    #     ts = [
    #         ExplicitVRLittleEndian,
    #         ImplicitVRLittleEndian,
    #         ExplicitVRBigEndian
    #         ]

    ts = [
        ExplicitVRLittleEndian,
        ImplicitVRLittleEndian,
        ExplicitVRBigEndian
    ]
    # call back
    def OnAssociateResponse(association):
        print "Association response received"

    # create application entity
    MyAE = AE('localhost', 9999, [StorageSOPClass,  VerificationSOPClass], [], ts)
    MyAE.OnAssociateResponse = OnAssociateResponse

    # remote application entity
    RemoteAE = dict(Address=remote_host, Port=remote_port, AET=remote_aet)

    # create association with remote AE
    print "Request association"
    assoc = MyAE.RequestAssociation(RemoteAE)

    if not assoc:
        print "Could not establish association"
        sys.exit(1)

    # perform a DICOM ECHO, just to make sure remote AE is listening
    print "DICOM Echo ... ",
    #st = assoc.VerificationSOPClass.SCU(1)
    #print 'done with status "%s"' % st


    # create some dataset
    f = read_file(dicom_file)
    print "DICOM StoreSCU ... ",
    try:
        st = assoc.SCU(f, 1)
        print 'done with status "%s"' % st
    except:
        print "problem", f.SOPClassUID

    print "Release association"
    assoc.Release(0)


    # done
    MyAE.Quit()

    return HttpResponse({})

def pdf(request):
    import datetime

    guid = '334cf168c94d42cb9f70e563913a488d'

    try:
        order = Order.objects.get(guid=guid)
    except Order.DoesNotExist:
        return api_error_message('DoesNotExist', "Order")


    # Prepare context
    data = {}
    today = str(datetime.date.today())
    data['order'] = order
    data['settings'] = settings
    # Render html content through html template with context
    template = get_template('report/report-new.html')
    html  = template.render(Context(data))

    # Write PDF to file
    file = open(settings.MEDIA_ROOT + '/report-' + str(order.id) + '.pdf', "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=file)

    # Return PDF document through a Django HTTP response
    file.seek(0)
    pdf = file.read()
    file.close()            # Don't forget to close the file handle
    return HttpResponse(pdf, mimetype='application/pdf')


def receipt(request):
    import datetime

    guid = '5cc96f705b214f7db35edf400d5a1cff'

    try:
        order = Order.objects.get(guid=guid)
    except Order.DoesNotExist:
        return api_error_message('DoesNotExist', "Order")


    # Prepare context
    data = {}
    today = str(datetime.date.today())
    data['order'] = order
    data['settings'] = settings
    # Render html content through html template with context
    template = get_template('report/receipt.html')
    html  = template.render(Context(data))

    # Write PDF to file
    file = open(settings.MEDIA_ROOT + '/report-' + str(order.id) + '.pdf', "w+b")
    pisaStatus = pisa.CreatePDF(html, dest=file)

    # Return PDF document through a Django HTTP response
    file.seek(0)
    pdf = file.read()
    file.close()            # Don't forget to close the file handle
    return HttpResponse(pdf, mimetype='application/pdf')


def health_check(request):
    if Hospital.objects.count():
        return HttpResponse('Successful')
    else:
        return HttpResponse('Not good', status=500)