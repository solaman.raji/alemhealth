import logging

from alemhealth.models import *
from alemhealth.utils import *
from django.conf import settings
import urlparse
import json

logger = logging.getLogger('alemhealth')


def check_accesskey(f):
    def wrap(request, *args, **kwargs):

        # #this check the access key is valid or invalid
        if request.META.get('HTTP_AUTHENTICATION', ''):
            access_key = request.META.get('HTTP_AUTHENTICATION')
            try:
                user = UserProfile.objects.get(access_key=access_key)
                request.user = user.user
            except UserProfile.DoesNotExist:
                return api_error_message("InvalidAccessKey")
        elif request.GET.get('secret_key'):
            if request.GET.get('secret_key') != settings.CLIENT_SECRET_KEY:
                return api_error_message("InvalidAccessKey")
        else:
            return api_error_message("InvalidAccessKey")
        return f(request, *args, **kwargs)
    wrap.__doc__=f.__doc__
    wrap.__name__=f.__name__
    return wrap


def check_user_cookie(f):
    def wrap(request, *args, **kwargs):
        logger.info('Inside check check_user_cookie decorator for user: {}'.format(request.user))
        if request.COOKIES.get('user', ''):
            user_cookie = request.COOKIES.get('user', '')
            user_cookie_parsed = urlparse.unquote(user_cookie)
            user_cookie_json = json.loads(user_cookie_parsed)
            access_key = user_cookie_json['access_key']

            try:
                user = UserProfile.objects.get(access_key=access_key)
                request.user = user.user
            except UserProfile.DoesNotExist:
                return api_error_message("InvalidAccessKey")
        elif request.user.is_authenticated:
            logger.info('User {} is authenticated in check_user_cookie decorator'.format(request.user))
        else:
            return api_error_message("InvalidAccessKey")
        return f(request, *args, **kwargs)
    wrap.__doc__ = f.__doc__
    wrap.__name__ = f.__name__
    return wrap
