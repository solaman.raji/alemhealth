from celery import task
from django.conf import settings
from alemhealth.utils import *


import dicom
import os.path
import os
import shutil
import urllib
import subprocess

from boto.s3.connection import S3Connection
from boto.s3.key import Key

import logging
logger = logging.getLogger('alemhealth')

# Task : write dicom meta to each order
# This is a async task to write meta to each order


@task(name='dicom_meta_write')
def dicom_meta_write(guid, user):
    from order.emails import *
    from order.models import *
    from django.core.files import File
    from activity.models import *

    logger.info('Dicom meta write is starting for order: {}'.format(guid))
    try:
        order = Order.objects.get(guid=guid)
        logger.info('Order : ' + str(order.pk) + ' start meta writing')
    except Order.DoesNotExist:
        logger.error('Order : ' + str(guid) + ' not found')
        return

    # create alemhealth temp directory
    ah_temp_dir = '/tmp/alemhealth'
    if not os.path.exists(ah_temp_dir):
        os.makedirs(ah_temp_dir)

    ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid)
    if not os.path.exists(ah_temp_order_dir):
        logger.info("Creating alemhealth temp order directory")
        os.makedirs(ah_temp_order_dir)

    count = 0
    for dicom_file in order.dicoms.all():
        try:

            filename = dicom_file.file.name.split('/')[2]
            file_image = dicom.read_file(dicom_file.file)

            file_image.PatientName = str(order.patient.name)
            file_image.PatientID = str(order.patient.patient_id)
            file_image.PatientBirthDate = str(order.patient.date_of_birth)
            file_image.PatientSex = str(order.patient.gender)
            file_image.PatientTelephoneNumbers = str(order.patient.phone)
            file_image.PatientAge = str(order.patient.age)

            file_image.AccessionNumber = str(order.accession_id)
            file_image.Modality = str(order.modality)

            # if order.hospital:
            #     file_image.OperatorsName = str(order.hospital.name)

            file_image.StationName = str(order.hospital.name)
            file_image.ReferringPhysicianName = str(order.doctor.name) if order.doctor else ''

            for meta in order.metas.all():
                # metas[meta.key] = meta.value
                if meta.key in file_image:
                    # print meta.key
                    data_element = file_image.data_element(meta.key)
                    data_element.value = str(meta.value)

                else:
                    setattr(file_image, meta.key, str(meta.value))

            # file_image.BodyPartExamined = "Foot"

            # write the meta
            if settings.USE_S3:
                # save to temp directory if using S3

                file_image.save_as(ah_temp_order_dir + '/' + filename)
                logger.info("Order : " + str(order.pk) +
                            " Dicom  " + str(dicom_file.guid) +
                            " , store in temp dir")

                # save django dicom file object
                dicom_file_path = urllib.pathname2url(
                    ah_temp_order_dir + '/' + filename)
                f = open(dicom_file_path)
                dicom_file.file.save(filename, File(f))
                f.close()

                logger.info("Order : " + str(order.pk) +
                            " Meta write successful in dicom  "
                            + str(dicom_file.guid))

                count += 1
                logger.info("Dicom #" + str(count))

                # set dicom transfer syntax
                if not dicom_file.transfer_syntax:
                    # fetch and set the Network Syntax from Dicom
                    stdOut = os.popen(
                        'dcmdump %s | grep "(0002,0010)"'
                        % dicom_file_path).read()
                    re_result = re.search('(?<==)\w+', stdOut)
                    if re_result:
                        dicom_file.transfer_syntax = re_result.group()

            else:
                # save to local media
                file_image.save_as(
                    settings.BASE_DIR + '/media/dicoms/'
                    + str(order.guid.hex) + '/' + filename)
                logger.info("Order : " + str(order.pk) +
                            " Meta write successful in dicom  "
                            + str(dicom_file.guid))

            dicom_file.edit_meta = 1
            dicom_file.save()

        except Exception as e:
            logger.error("Order : " + str(order.pk) +
                         " Meta write failed in dicom  "
                         + str(dicom_file.guid) + ' ' + str(e))
            pass

    # check all meta write successfully or not
    if order.dicoms.filter(edit_meta=0).count() == 0:
        # all meta edit successfully
        logger.info('Dicom meta write is done for order: {}'.format(guid))
        order.meta_status = 2
        # TODO  : when all meta write successfully we'd sync
        # these to provider server, for now we just set it manually
        # order.status = 2 # images synced in provider server
        # order.status = 3 # images synced complete

        # all meta successfully write
        # send Study detail to Radiologist by email/SMS while its ready
        if order.radiologist:
            radiologist_new_study_email(order)

        # activity
        actor = {
            'type': 'study',
            'name': str(order.accession_id),
            'guid': str(order.guid)
        }
        context = 'Study details with ' + str(order.dicoms.all().count()) + ' images successfully updated'
        #context = 'updated meta of ' + str(order.dicoms.all().count()) + ' images successfully'

        listeners = [order.operator, order.hospital.user.all()[0]]

        create_activity(
            actor,
            context,
            listeners,
            order=order
        )

        # zip dicom files
        ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid) + '/'
        zipfile_name = 'order-' + str(order.pk)

        logger.info(
            "Order : " + str(order.pk) + " zipping order")
        # making zip file in tmp dir
        shutil.make_archive('/tmp/' + zipfile_name, 'zip', ah_temp_order_dir)

        f = open('/tmp/' + zipfile_name + '.zip')

        order.zipfile.save(zipfile_name + '.zip', File(f))

        # auto sync start
        order.status = 2

        order.save()
        f.close()

        try:
            if os.path.isfile('/tmp/' + zipfile_name + '.zip'):
                os.unlink('/tmp/' + zipfile_name + '.zip')
                logger.info(
                    "Order : " + str(order.pk) + " remove temp zip file")

        except Exception, e:
            logger.error(str(e))

        context = 'Zipping ' + str(order.dicoms.all().count()) + ' DICOM images complete'
        create_activity(
            actor,
            context,
            listeners,
            order=order
        )

        logger.info(
            "Order : " + str(order.pk) + " Meta write successfully complete ")


    # change the meta status "Write" if all
    # images meta write completed
    # successfully
    # if not order.dicoms.filter(meta_edit=0).count:
    #     order.meta_status = 2
    #     order.save()
    if order.provider:
        logger.error("Provider found for order " + str(order.guid))
        logger.info("Provider is: " + str(order.provider.__dict__))
        try:
            send_email_to_all_provider(order, order.provider, order.hospital)
            logger.info("Order " + str(order.guid) + ": mail has been sent to the providers")
        except Exception as e:
            logger.error("Order " + str(order.guid) + ": cannot send mail. Reason: " + str(e))
    else:
        logger.error("No provider found for order " + str(order.guid))
    return


# Task : send dicom to specific provider by using dcmtk
# This is a async task to send dicom images to there provider

@task(
    name='send_dicom_to_provider',
    bind=True, default_retry_delay=10,
    max_retries=10
)
def send_dicom_to_provider(self, guid):
    from order.models import *
    from order.emails import *
    from activity.models import *
    order = Order.objects.get(guid=guid)

    if not order.provider.is_csend or order.is_storage:
        order.status = 4
        order.save()
        return

    logger.info('Order : ' + str(order.pk) +
                ' start sending dicom to provider ' + order.provider.name)

    # create alemhealth temp directory
    ah_temp_dir = '/tmp/alemhealth'
    if not os.path.exists(ah_temp_dir):
        os.makedirs(ah_temp_dir)

    ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid)
    if not os.path.exists(ah_temp_order_dir):
        logger.info("Creating alemhealth temp order directory")
        os.makedirs(ah_temp_order_dir)

    remote_host = str(order.provider.host)
    remote_port = int(order.provider.port)
    remote_aet = str(order.provider.ae_title)

    # os.system('storescu localhost 11112 -aet alemhealth
    # /Users/macbookpro/development/python/alemhealth/media/test2.dcm ')

    for dicom_file in order.dicoms.filter(edit_meta=1):

        # filename = os.path.basename(dicom_file.file.name)
        # file_image = dicom.read_file(dicom_file.file)

        # ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid)
        if settings.USE_S3:
            filename = dicom_file.file.name.split('/')[2]
            dicom_file_path = ah_temp_order_dir + '/' + filename

            # if temp folder delete due to system restart
            # copy dicom s3 to temp folder
            if not os.path.exists(dicom_file_path):

                # Connect to S3
                try:
                    conn = S3Connection(
                        settings.AWS_ACCESS_KEY_ID,
                        settings.AWS_SECRET_ACCESS_KEY,
                        host='s3-ap-southeast-1.amazonaws.com'
                    )

                except Exception as e:
                    logger.error(e)
                    if self.request.retries >= self.max_retries:
                        logger.error('Cannot connect to s3 : ')
                        self.apply_async(args=[guid], countdown=30)
                        return
                    else:
                        logger.error('Cannot connect to s3 : ')
                        self.retry(exc=str(e))

                # get bucket
                bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)

                open(dicom_file_path, 'w+')
                k = Key(bucket)
                k.key = 'media/dicoms/' + str(order.guid) + '/' + filename
                k.set_contents_from_filename(dicom_file_path)

        else:
            dicom_file_path = dicom_file.file.path

        # try:
            # check dicom modality is "HSG"

        transfer_syntax_display = dicom_file.get_transfer_syntax_display()
        if not transfer_syntax_display:
            transfer_syntax_display = ''

        if order.modality == "HSG":
            logger.info(
                'Decompresses Dicom file  for order : ' + str(order.pk))
            result = os.system(
                'dcmdjpeg %s %s' %
                (dicom_file_path, dicom_file_path))

        logger.info(
            'dcmtk command : ' +
            'storescu %s %s -aet alemhealth -aec %s %s %s %s  -xv' % (
                remote_host, remote_port, remote_aet, dicom_file_path,
                ' '.join(settings.STORESCU_PARAMS),
                transfer_syntax_display
            )
        )

        try:
            # send the dicom to provider by DCMTK storescu
            subprocess.check_call(
                'storescu %s %s -aet alemhealth -aec %s %s %s %s  -xv' % (
                    remote_host, remote_port, remote_aet, dicom_file_path,
                    ' '.join(settings.STORESCU_PARAMS),
                    transfer_syntax_display
                    ), shell=True
            )

            logger.info("Order : " + str(order.pk) + " Dicom : " +
                        str(dicom_file.guid) +
                        "  send successful to " + order.provider.name)

            dicom_file.edit_meta = 2
            dicom_file.save()
        except Exception as e:

            if self.request.retries >= self.max_retries:
                # WEHN MAX RETRY EXCEED IT'LL
                # AGAIN AUTO RETRYING AFTER 60 SEC ..
                logger.error(
                    "Retrying Order : " +
                    str(order.pk) + ' ' + str(e))
                self.apply_async(args=[guid], countdown=60)
                return
            else:
                logger.error(
                    "Retrying Order : " + str(order.pk) + ' ' + str(e))
                self.retry(exc=str(e))

    # all dycom route successfully
    if order.dicoms.filter(edit_meta=1).count() == 0:

        order.status = 4  # images synced in provider server
        order.save()
        logger.info('Order : ' + str(order.pk) +
                    ' all dicoms successfully send to provider '
                    + order.provider.name)
        send_provider_dicom_route_email(order, order.provider)

         # add activity

        actor = {
            'type': 'study',
            'name': order.accession_id,
            'guid': str(order.guid)
        }

        context = 'successfully routed to'

        obj = {
            'type': 'provider',
            'name': order.provider.name,
            'guid': str(order.provider.guid)
        }

        listeners = [order.operator, order.hospital.user.all()[0]]

        create_activity(actor, context, listeners, obj=obj, order=order)


        if order.priority == 'Routine':
            turnaround_time = order.provider.routine_turnaround
        else:
            turnaround_time = order.provider.stat_turnaround


        logger.info(
            'Provider report ' +
            'status check after :' + str(60*60*int(turnaround_time)) + ' sec ..')
        provider_report_status.apply_async(
            args=[guid], countdown=60*60*int(turnaround_time))
        # remove all temp files
        if settings.USE_S3:
            for the_file in os.listdir(ah_temp_order_dir):
                file_path = os.path.join(ah_temp_order_dir, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception, e:
                    logger.error(str(e))

    return

# Task : send dicom to 2nd provider by using dcmtk
# This is a async task to send dicom images to there provider

@task(
    name='send_dicom_to_2nd_provider',
    bind=True, default_retry_delay=10,
    max_retries=10
)
def send_dicom_to_2nd_provider(self, guid):
    from order.models import *
    from order.emails import *
    from activity.models import *
    order = Order.objects.get(guid=guid)

    if not order.second_provider.is_csend:
        order.status = 4
        order.save()
        return

    logger.info('Order : ' + str(order.pk) +
                ' start sending dicom to provider ' + order.second_provider.name)

    # create alemhealth temp directory
    ah_temp_dir = '/tmp/alemhealth'
    if not os.path.exists(ah_temp_dir):
        os.makedirs(ah_temp_dir)

    ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid)
    if not os.path.exists(ah_temp_order_dir):
        logger.info("Creating alemhealth temp order directory")
        os.makedirs(ah_temp_order_dir)

    remote_host = str(order.second_provider.host)
    remote_port = int(order.second_provider.port)
    remote_aet = str(order.second_provider.ae_title)

    # os.system('storescu localhost 11112 -aet alemhealth
    # /Users/macbookpro/development/python/alemhealth/media/test2.dcm ')

    for dicom_file in order.dicoms.filter(edit_meta=1):

        # filename = os.path.basename(dicom_file.file.name)
        # file_image = dicom.read_file(dicom_file.file)

        # ah_temp_order_dir = '/tmp/alemhealth/dicoms/' + str(order.guid)
        if settings.USE_S3:
            filename = dicom_file.file.name.split('/')[2]
            dicom_file_path = ah_temp_order_dir + '/' + filename

            # if temp folder delete due to system restart
            # copy dicom s3 to temp folder
            if not os.path.exists(dicom_file_path):

                # Connect to S3
                try:
                    conn = S3Connection(
                        settings.AWS_ACCESS_KEY_ID,
                        settings.AWS_SECRET_ACCESS_KEY,
                        host='s3-ap-southeast-1.amazonaws.com'
                    )

                except Exception as e:
                    logger.error(e)
                    if self.request.retries >= self.max_retries:
                        logger.error('Cannot connect to s3 : ')
                        self.apply_async(args=[guid], countdown=30)
                        return
                    else:
                        logger.error('Cannot connect to s3 : ')
                        self.retry(exc=str(e))

                # get bucket
                bucket = conn.get_bucket(settings.AWS_STORAGE_BUCKET_NAME)

                open(dicom_file_path, 'w+')
                k = Key(bucket)
                k.key = 'media/dicoms/' + str(order.guid) + '/' + filename
                k.set_contents_from_filename(dicom_file_path)

        else:
            dicom_file_path = dicom_file.file.path

        # try:
            # check dicom modality is "HSG"

        transfer_syntax_display = dicom_file.get_transfer_syntax_display()
        if not transfer_syntax_display:
            transfer_syntax_display = ''

        if order.modality == "HSG":
            logger.info(
                'Decompresses Dicom file  for order : ' + str(order.pk))
            result = os.system(
                'dcmdjpeg %s %s' %
                (dicom_file_path, dicom_file_path))

        logger.info(
            'dcmtk command : ' +
            'storescu %s %s -aet alemhealth -aec %s %s %s %s  -xv' % (
                remote_host, remote_port, remote_aet, dicom_file_path,
                ' '.join(settings.STORESCU_PARAMS),
                transfer_syntax_display
            )
        )

        try:
            # send the dicom to provider by DCMTK storescu
            subprocess.check_call(
                'storescu %s %s -aet alemhealth -aec %s %s %s %s  -xv' % (
                    remote_host, remote_port, remote_aet, dicom_file_path,
                    ' '.join(settings.STORESCU_PARAMS),
                    transfer_syntax_display
                    ), shell=True
            )

            logger.info("Order : " + str(order.pk) + " Dicom : " +
                        str(dicom_file.guid) +
                        "  send successful to " + order.second_provider.name)

            dicom_file.edit_meta = 2
            dicom_file.save()
        except Exception as e:

            if self.request.retries >= self.max_retries:
                # WEHN MAX RETRY EXCEED IT'LL
                # AGAIN AUTO RETRYING AFTER 60 SEC ..
                logger.error(
                    "Retrying Order : " +
                    str(order.pk) + ' ' + str(e))
                self.apply_async(args=[guid], countdown=60)
                return
            else:
                logger.error(
                    "Retrying Order : " + str(order.pk) + ' ' + str(e))
                self.retry(exc=str(e))

    # all dycom route successfully
    if order.dicoms.filter(edit_meta=1).count() == 0:

        # order.status = 4  # images synced in provider server
        # order.save()
        logger.info('Order : ' + str(order.pk) +
                    ' all dicoms successfully send to provider '
                    + order.second_provider.name)
        send_provider_dicom_route_email(order, order.second_provider)

         # add activity

        actor = {
            'type': 'study',
            'name': order.accession_id,
            'guid': str(order.guid)
        }

        context = 'successfully routed to'

        obj = {
            'type': 'provider',
            'name': order.second_provider.name,
            'guid': str(order.second_provider.guid)
        }

        listeners = [order.operator, order.hospital.user.all()[0]]

        create_activity(actor, context, listeners, obj=obj, order=order)


        if order.priority == 'Routine':
            turnaround_time = order.second_provider.routine_turnaround
        else:
            turnaround_time = order.second_provider.stat_turnaround


        logger.info(
            'Provider report ' +
            'status check after :' + str(60*60*int(turnaround_time)) + ' sec ..')
        provider_report_status.apply_async(
            args=[guid], countdown=60*60*int(turnaround_time))
        # remove all temp files
        if settings.USE_S3:
            for the_file in os.listdir(ah_temp_order_dir):
                file_path = os.path.join(ah_temp_order_dir, the_file)
                try:
                    if os.path.isfile(file_path):
                        os.unlink(file_path)
                except Exception, e:
                    logger.error(str(e))

    return

# storescp 5555 -aet alemhealth -fe ".dcm"  -su ah --fork

# Periodic Task :
# store dicom to alemhealth system which get by storescp listener
@task(name='store_dicom_from_listener_dir')
def store_dicom_from_listener_dir():
    from order.models import *
    from django.core.files import File

    storescp_folder_dir = settings.BASE_DIR + "/storescp/"
    for folder_name in os.listdir(storescp_folder_dir + "."):

        # read only directories
        if os.path.isdir(storescp_folder_dir + folder_name):
            dicom_uid = folder_name[3:]

            # check this order exist or not
            try:
                order = Order.objects.get(dicom_uid=dicom_uid)
            except Order.DoesNotExist:

                # save order as draft
                # Todo :
                # need to assign order with a specific operator
                order = Order()
                order.operator_id = 2
                order.dicom_uid = dicom_uid
                order.save()

            # read all dicom image within folder
            for dicom_file in os.listdir(storescp_folder_dir + folder_name + "/."):

                dicom_file_path = 'dicoms/' + \
                    str(order.guid) + '/' + dicom_file

                # check, file is with valid .dcm extension or not and its not
                # already uploaded
                if dicom_file.endswith('.dcm') and not dicom_file_path in order.dicoms.all().values_list('file', flat=True):
                    try:
                        # save the file object if not created yet
                        f = open(storescp_folder_dir + folder_name + '/' + dicom_file)
                        dcm_file = DicomFile()
                        dcm_file.order = order
                        dcm_file.file.save(dicom_file, File(f))
                        dcm_file.save()
                        f.close()
                    except Exception as e:
                        logger.error("Order : " + str(order.pk) + " Dicom : "
                                     + str(dcm_file.guid) + " save failed "
                                     + str(e))

                    # update after first dicom meta write
                    if order.meta_status == 0:
                        order.meta_status = 1
                        order.save()
                        logger.info("Order : " + str(order.pk) +
                                    " First Dicom : " + str(dcm_file.guid) +
                                    " save successfully ")
                    else:
                        logger.info(
                            "Order : " + str(order.pk) +
                            " Dicom : " + str(dcm_file.guid) +
                            " save successfully "
                        )

    return True


# Task : dicom sync complete with desktop app/ mobile app
# we need to collect that dicom and add to our order system as draft

@task(
    name='store_dicom_from_s3_bucket',
    bind=True,
    default_retry_delay=30,
    max_retries=10
)
def store_dicom_from_s3_bucket(self, key, hospital_guid):
    #import pdb; pdb.set_trace()
    from order.models import Order, DicomFile
    from django.core.files import File
    from alemhealth.models import Hospital

    logger.info("Dicom : " + key + " start processing from s3 bucket ")
    dicom_uid = key.split('/')[0]
    dicom_filename = key.split('/')[1]


    # Connect to S3
    try:
        conn = S3Connection(
            settings.AWS_ACCESS_KEY_ID,
            settings.AWS_SECRET_ACCESS_KEY,
            host='s3-ap-southeast-1.amazonaws.com'
        )

    except Exception as e:
        logger.error(e)
        if self.request.retries >= self.max_retries:
            logger.error('Cannot connect to s3 : ')
            return False
        # retry gain task
        #store_dicom_from_s3_bucket.retry(exc='Cannot connect to s3 : ')
        return False

    # get bucket
    bucket = conn.get_bucket('alemhealth-desktop')

    try:
        order = Order.objects.get(dicom_uid=dicom_uid)
    except Order.DoesNotExist:

        # save order as draft
        # Todo :
        # need to assign order with a specific operator
        order = Order()
        order.operator_id = 2
        order.dicom_uid = dicom_uid

        if hospital_guid:
            try:
                hospital = Hospital.objects.get(guid=hospital_guid)
                order.hospital = hospital
            except Hospital.DoesNotExist:
                pass

        order.save()

    # create a temp directory
    #temp_directory = settings.BASE_DIR + '/media/temp/'
    temp_directory = '/var/tmp/'
    if not os.path.exists(temp_directory):
        os.makedirs(temp_directory)

    # get the file from s3 bucket
    # store in a temp folder and remove that after use
    try:
        # get the bucket key
        print key
        k = Key(bucket)
        k.key = key
        # make a temp file to store the s3 dicom on that
        temp_dicom_file_path = temp_directory + dicom_filename
        temp_file = open(temp_dicom_file_path, 'w')
        k.get_contents_to_filename(temp_dicom_file_path)
        temp_file.close()

    except Exception as e:
        # todo
        # need to retry again
        logger.error(e)

        if self.request.retries >= self.max_retries:
            logger.error(
                'Order : ' + str(order.pk) +
                ' maximum try to collect dicom ' +
                ' from s3 ' + str(e)
            )
            print 'its return with max try'
            return False
        # retry gain task
        #store_dicom_from_s3_bucket.retry(exc="Issue with bucket " + str(e))
        return False

    # add the temp dicom file to AH system
    try:

        if order.dicoms.all().count() > 0:
            order.meta_status = 1
            order.save()

        f = open(temp_dicom_file_path)
        dcm_file = DicomFile()
        dcm_file.order = order
        dcm_file.file.save(dicom_filename, File(f))
        dcm_file.save()

        logger.info("Dicom : " + key + " successfully add to AH system")
        f.close()

        #remove dicom temp file
        os.remove(temp_dicom_file_path)

    except Exception as e:
        # todo retry
        logger.error("Order : " + str(order.pk) + " Dicom : "
                     + str(dcm_file.guid) + " save failed ")
        if self.request.retries >= self.max_retries:
            logger.error(
                'Order : ' + str(order.pk) +
                ' maximum try to collect ' +
                ' dicom : ' + str(dcm_file.guid) + ' from s3 '
            )
            return False
        # retry gain task
        #store_dicom_from_s3_bucket.retry(exc="problem ")
        return False

    return True

# this task retry after 1 hour with max try 10 times and again start the task


@task(
    name='provider_report_status',
    bind=True, default_retry_delay=60*60,
    max_retries=10
)
def provider_report_status(self, guid):
    from order.models import *
    from order.emails import *

    try:
        order = Order.objects.get(guid=guid)
        logger.info("Order : " + str(order.pk) + " provider status check")
        if order.status == 4:
            # remind_provider_email
            provider_remainder_email(order)
            print "Send remainder email to provider"
            # retry this task
            if self.request.retries >= self.max_retries:
                logger.info(
                    "Maximum try exit, Order : %s retrying provider_report_status in 1 hour " % str(order.pk))
                self.apply_async(args=[guid], countdown=60*60)
                return
            else:
                logger.info('Retrying provider_report_status task in 1 hour')
                self.retry(
                    exc="Order : %s Provider not response with report" % str(order.pk))
        else:
            logger.info("Order : %s Provider respond with report" % str(order.pk))
    except Order.DoesNotExist:
        pass


# predioc
@task(
    name='check_exchange_rate_in_1_day',
)
def check_exchange_rate_in_1_day():
    logger.info('Check exchange rate')

    from alemhealth.models import Currency
    import requests
    currencies = Currency.objects.all().values_list('code', flat=True)
    currency_str = ''
    count = 0
    for currency in currencies:
        if count:
            currency_str += ',"USD' + currency + '"'
        else:
            currency_str += '"USD' + currency + '"'
        count = count + 1

    if currency_str:
        url = 'http://query.yahooapis.com/v1/public/yql?q=select * from yahoo.finance.xchange where pair in (%s)&env=store://datatables.org/alltableswithkeys&format=json' % currency_str

        r = requests.get(url)

        if r.status_code == 200:

            results = r.json().get('query').get('results').get('rate')

            if count == 1:
                try:
                    currency_obj = Currency.objects.get(code=results.get('id')[3:])
                    old_rate = currency_obj.rate
                    currency_obj.rate = results.get('Rate')
                    currency_obj.save()
                    logger.info('Exchange rate change for ' + results.get('id')[3:] + ' ' + str(old_rate) + ' -> ' + results.get('Rate'))
                except Currency.DoesNotExist:
                    pass
            else :
                for currency in results:
                    try:
                        currency_obj = Currency.objects.get(code=currency.get('id')[3:])
                        old_rate = currency_obj.rate
                        currency_obj.rate = currency.get('Rate')
                        currency_obj.save()
                        logger.info('Exchange rate change for ' + currency.get('id')[3:] + ' ' + str(old_rate) + ' -> ' + currency.get('Rate'))
                    except Currency.DoesNotExist:
                        pass



@task(
    name='send_daily_study_report',
    default_retry_delay=5,
    max_retries=5
)
def send_daily_study_report():
    from order.models import *
    import base64
    import datetime
    import time

    _now = str(datetime.date.today())
    html = "Daily report " + _now
    start_date = datetime.datetime.strptime(_now, "%Y-%m-%d")

    start_date_range = (
    # The start_date with the minimum possible time
    datetime.datetime.combine(start_date, datetime.datetime.min.time()),
    # The start_date with the maximum possible time
    datetime.datetime.combine(start_date, datetime.datetime.max.time()))

    datas = Order.objects.filter(created_at__range=start_date_range).select_related()


    today =  time.strftime("%Y-%m-%d")
    #test_date =  "2014-11-14"

    email_list = ['sakib2309@gmail.com','sajjad@alemcloud.com', 'ir@inov.io', 'aschkan@alemcloud.com']

    with open('media/daily_report.csv',"wb") as csvfile:
        writer = csv.writer(csvfile, dialect='excel')
        writer.writerow([ "Patient Id",  "Patient Name", "Modality", "Hospital ID ","Hospital Name", "Referral Code","Doctor name", "Priority", "Provider"])

        for data in datas:
    		writer.writerow([ data.patient.id if data.patient else None, data.patient.name if data.patient else None, data.modality,
    		data.hospital_id, data.hospital.name if data.hospital else None,
    		data.referral_code, data.doctor.name if data.doctor else None, data.priority,
            data.provider.name if data.provider else None])

    with open ('media/daily_report.csv', "r") as file_to_read:
    	daily_report = file_to_read.read()
    for email in email_list:

    	try:
    	    mandrill_client = mandrill.Mandrill(settings.MANDRILL_ACCESS_KEY)
    	    message = {
    	    'from_email': 'team@alemhealth.com',
    	    'from_name': 'Alemhealth',
    	    'headers': {'Reply-To': 'noreply@alemhealth.com'},
    	    'html': html,
    	    'important': True,
    	    'subject': 'Daily report ' + _now,
    	    'tags': [''],
    	    'to': [{'email': email, 'name': 'sakib' }],
    		'attachments' : [{
    			'type' :'text/csv',
    			'name' :'Daily-report-' + _now,
    			'content': base64.b64encode(daily_report)
    			}],
    		}

    	    result = mandrill_client.messages.send(
    	    message=message, async=False, ip_pool='Main Pool', send_at='')

    	    if result[0]['status'] != "sent" and result[0]['status'] != "queued":
    	        logger.error('Queued')

    	except mandrill.Error, e:
    		logger.error(str(e))

    os.remove('media/daily_report.csv')

    return


@task(
    name='another_test_task',
    default_retry_delay=5,
    max_retries=5
)
def another_test_task(t):

    print "another task execute  after " + str(t) + ' Sec'

@task(
    name='test_task',
    bind=True, default_retry_delay=5,
    max_retries=5
)
def test_task(self):
    try:
        print "This is the first task"
        another_test_task.apply_async(args=[60], countdown=60)
    except Exception as e:

        if self.request.retries >= self.max_retries:
            print "max Try"
            self.apply_async(args=[], countdown=5)
            return
        else:
            print 'Retriing 5 sec'
            self.retry(exc=str(e))
            #test_task.apply_async(countdown=10)
    print "DDDD"
