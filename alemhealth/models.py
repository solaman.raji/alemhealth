# AlemHealth User models
# - UserProfile
# - Hospitals
# - Operator
# - Patient

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings

import uuid
from django_countries.fields import CountryField
import os
from alemhealth.jsonfield import JSONField


class UserProfile(models.Model):

    user = models.OneToOneField(User, related_name='profile')
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    access_key = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)

    phone = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=128, null=True, blank=True)

    class Meta:
        db_table = 'user_profiles'

    def __unicode__(self):
        return self.guid


# save add user profile while save user

def add_user_profile(sender, instance, **kwargs):
    UserProfile.objects.get_or_create(user=instance)


models.signals.post_save.connect(add_user_profile, sender=User)


def ah_files_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (str(instance.guid), ext)
    return os.path.join('files/', filename)


class AhFile(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    file = models.FileField(upload_to=ah_files_path)

    class Meta:
        db_table = 'files'

    def __unicode__(self):
        return str(self.guid)


def delete_files(sender, instance, **kwargs):

    file_path = settings.BASE_DIR + '/media/files/' + str(instance.file.name)

    try:
        if os.path.isfile(file_path):
            os.unlink(file_path)
    except Exception, e:
        print e


models.signals.pre_delete.connect(delete_files, sender=AhFile)


class Currency(models.Model):
    name = models.CharField(max_length=128)
    code = models.CharField(max_length=10)
    sign = models.CharField(max_length=5, help_text="Currency sign", default='$')
    is_left = models.BooleanField(
        default=True,
        help_text="Currency label appear in Left or Right")


    # http://www.exchange-rate.com/currency-list.html
    rate = models.DecimalField(max_digits=8, decimal_places=2, help_text="Today's currency rate")
    tax = models.DecimalField(max_digits=8, decimal_places=2, help_text="Hospital service tax", default=2.0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'currencies'

    def __unicode__(self):
        return self.name


class Hospital(models.Model):

    hospital_type_list = (
        (0, 'Primary'),
        (1, 'Secondary'),
        (2, 'Tertiary'),
        (3, 'Diagnostic'),
        (4, 'Community health clinic'),
    )

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.ManyToManyField(
        User, related_name='hospitals', blank=True)

    name = models.CharField(max_length=128)
    address = models.TextField(null=True, blank=True)
    phone = models.CharField(max_length=128, blank=True, null=True)
    general_email = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=128, null=True, blank=True)
    country = CountryField(null=True, blank=True)
    hospital_type = models.IntegerField(choices=hospital_type_list, default=0)

    primary_name = models.CharField(max_length=128, null=True, blank=True)
    primary_email = models.CharField(max_length=128, null=True, blank=True)
    primary_physician_contact = models.CharField(
        max_length=128, null=True, blank=True)
    logo = models.ForeignKey(
        AhFile, related_name='hospital', null=True, blank=True)

    long = models.CharField(max_length=128, null=True, blank=True)
    lat = models.CharField(max_length=128, null=True, blank=True)

    currency = models.ForeignKey(
        Currency,
        related_name='hospitals', blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    short_code = models.CharField(max_length=5, unique=True, blank=True, null=True)
    providers = JSONField(default=None, blank=True, null=True)

    class Meta:
        db_table = 'hospitals'

    def __unicode__(self):
        return self.name


class Operator(models.Model):
    user = models.OneToOneField(User, related_name='operator')
    hospitals = models.ManyToManyField(Hospital, related_name='operators')

    class Meta:
        db_table = 'operators'

    def __unicode__(self):
        return self.user.username


class Patient(models.Model):

    blood_group_list = (
        ('O+', 'O+'),
        ('O-', 'O-'),
        ('A+', 'A+'),
        ('A-', 'A-'),
        ('B+', 'B+'),
        ('B-', 'B-'),
        ('AB+', 'AB+'),
        ('AB-', 'AB-'),

    )

    user = models.OneToOneField(
        User, related_name='patients', null=True, blank=True)
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    patient_id = models.CharField(max_length=128, blank=True, null=True)
    name = models.CharField(max_length=128, blank=True, null=True)
    gender = models.CharField(max_length=10, blank=True, null=True)
    age = models.CharField(max_length=10, blank=True, null=True)
    phone = models.CharField(max_length=128, blank=True, null=True)

    blood_group = models.CharField(
        max_length=5, choices=blood_group_list, blank=True, null=True)
    date_of_birth = models.CharField(max_length=128, blank=True, null=True)

    city = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=128, null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'patients'

    def __unicode__(self):
        return self.name


class Provider(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.ManyToManyField(
        User, related_name='providers', blank=True)

    name = models.CharField(max_length=128)
    email = models.CharField(max_length=128, null=True, blank=True)
    phone = models.CharField(max_length=128, blank=True, null=True)
    address = models.TextField(blank=True, null=True)

    city = models.CharField(max_length=128, null=True, blank=True)
    country = CountryField(null=True, blank=True)

    primary_name = models.CharField(max_length=128, null=True, blank=True)
    # more then one email for providers
    primary_email = models.TextField(null=True, blank=True)
    primary_phone = models.CharField(max_length=128, null=True, blank=True)

    logo = models.ForeignKey(
        AhFile, related_name='provider', null=True, blank=True)

    is_csend = models.BooleanField(default=True)
    ae_title = models.CharField(max_length=128, null=True, blank=True)
    host = models.CharField(max_length=128, null=True, blank=True)
    port = models.CharField(max_length=10, null=True, blank=True)

    routine_turnaround = models.IntegerField(default=7)
    stat_turnaround = models.IntegerField(default=3)

    default_radiologist = models.IntegerField(default=0)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'providers'

    def __unicode__(self):
        return self.name


class Doctor(models.Model):

    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    user = models.OneToOneField(
        User, related_name='doctor', null=True, blank=True)
    referral_code = models.IntegerField(null=True, blank=True, unique=True)
    hospital = models.ForeignKey(
        Hospital, related_name='doctors',
        null=True, blank=True, on_delete=models.SET_NULL)

    provider = models.ForeignKey(
        Provider, related_name='doctors',
        null=True, blank=True, on_delete=models.SET_NULL)


    name = models.CharField(max_length=128)
    specialty = models.CharField(max_length=128, null=True, blank=True)

    email = models.CharField(max_length=128, null=True, blank=True)
    phone = models.CharField(max_length=128, null=True, blank=True)
    city = models.CharField(max_length=128, null=True, blank=True)
    country = models.CharField(max_length=128, null=True, blank=True)

    radiologist = models.BooleanField(default=False)

    signature = models.ForeignKey(
        AhFile, related_name='doctor', null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = 'doctors'

    def __unicode__(self):
        return self.name


class DoctorSpeciality(models.Model):
    name = models.CharField(max_length=225)

    class Meta:
        db_table = 'doctor_specialites'

    def __unicode__(self):
        return self.name
