import django
from django.conf.urls import include, url
from django.conf import settings
from django.contrib import admin
from alemhealth.views import *


from django.conf.urls.static import static
admin.autodiscover()

from django.contrib.staticfiles.views import serve
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
urlpatterns = [
    url(r'^media/(?P<path>.*)$', django.views.static.serve, {'document_root': settings.MEDIA_ROOT}),
    url(r'^$', dashboard, name='dashboard'),
    url(r'^pdf/?$', pdf),
    url(r'^dicom/?$', dicom),
    url(r'^receipt/?$', receipt),
    url(r'^dashboard/?$', dashboard),
    url(r'^health-check', health_check),
    url(r'^ah_admin/', include(admin.site.urls)),
    url(r'^', include('alemhealth.api_urls')),
    url(r'^', include('order.urls')),
    url(r'^', include('activity.urls')),
    url(r'^', include('report.api_url')),
]
