import base64
import contextlib
import csv
import datetime
import errno
import hashlib
import json
import re
import shutil
import socket
import tempfile
import time
import uuid

from django.conf import settings
from django.http import HttpResponse
from django.template.defaultfilters import slugify
from django.utils.encoding import smart_str


def get_uuid():
    return hashlib.md5(str(uuid.uuid1())).hexdigest()
    #return uuid.uuid1().hex


def app_storage_url(base_url='', value=''):
    if settings.USE_S3:
        bucket = settings.AWS_STORAGE_BUCKET_NAME
        media_url = settings.S3_URL + '/media/'
    else:
        media_url = base_url.rstrip(' /') + '/media/'
    return media_url + smart_str(value.lstrip(' /'))


def api_error_message(type, msg=""):

    error_message = {
        'DoesNotExist':{
            'code': '501',
            'message': msg  + ' does not Exist',
            'description':''
        },
        'DeleteSuccessfully':{
            'code':'202',
            'message': msg + ' delete Successfully',
            'description':''
        },
        'InvalidParameter': {
            'code': '400',
            'type': 'InvalidParameter',
            'message': 'Invalid Parameter Given' + (" '" + msg + "'" if msg else ''),
            'description': ''
        },
        'ParameterMissing': {
            'code': '400',
            'type': 'ParameterMissing',
            'message': 'Post Parameter Missing' + (" '" + msg + "'" if msg else ''),
            'description': ''
        },
        'IntegrityError': {
            'code': '500',
            'message': msg,
            'description': ''
        },
        "ResourceAlreadyExist":{
            'code': '409',
            'message': msg + ' already exist',
            'description': ''
        },
        "InvalidAccessKey":{
            'code': '403',
            'message': 'You provided an invalid api key',
            'description': 'You provided an invalid api key.'
        }

    }

    return HttpResponse(json.dumps(error_message[type]) , content_type="application/json", status= int(error_message[type]['code']))


def dict_to_json(data, status):
    return HttpResponse(json.dumps(data), content_type='application/json; charset=utf-8', status=status)



class DisableCSRF(object):
    def process_request(self, request):
        setattr(request, '_dont_enforce_csrf_checks', True)



class RedirectException(Exception):
    def __init__(self, url):
        self.url = url
    def __str__(self):
        return repr(self.url)


def get_full_path(request):
    full_path = ('http', ('', 's')[request.is_secure()], '://', request.META['HTTP_HOST'], request.path)
    return ''.join(full_path)

def get_country_list():
    shipping_file= settings.BASE_DIR + '/media/shipping-rates.csv'
    with open(shipping_file, 'r') as f:
        data = [[row[0], row[0]] for row in csv.reader(f.read().splitlines())]
    del data[0]
    return data



def truncate(text, length=100, suffix='...'):
    """Truncates `text`, on a word boundary, as close to
    the target length it can come.
    """

    slen = len(suffix)
    pattern = r'^(.{0,%d}\S)\s+\S+' % (length-slen-1)
    if len(text) > length:
        match = re.match(pattern, text)
        if match:
            length0 = match.end(0)
            length1 = match.end(1)
            if abs(length0+slen-length) < abs(length1+slen-length):
                return match.group(0) + suffix
            else:
                return match.group(1) + suffix
    return text


def handle_uploaded_file(request, source):
    FILE_UPLOAD_DIR= settings.BASE_DIR + '/media/' + request.business.sub_domain + '/images'

    fd, filepath = tempfile.mkstemp(prefix=source.name, dir=FILE_UPLOAD_DIR)
    with open(filepath, 'wb') as dest:
        shutil.copyfileobj(source, dest)
    return filepath


def save_file(file, path=''):
    filename = file._get_name()
    fd = open('%s/%s' % (settings.BASE_DIR, str(path) + str(filename)), 'wb')
    for chunk in file.chunks():
        fd.write(chunk)
    fd.close()


def unique_slugify(instance, value, slug_field_name='slug', queryset=None,
                   slug_separator='-'):
    """
    Calculates and stores a unique slug of ``value`` for an instance.

    ``slug_field_name`` should be a string matching the name of the field to
    store the slug in (and the field to check against for uniqueness).

    ``queryset`` usually doesn't need to be explicitly provided - it'll default
    to using the ``.all()`` queryset from the model's default manager.
    """
    slug_field = instance._meta.get_field(slug_field_name)

    slug = getattr(instance, slug_field.attname)
    slug_len = slug_field.max_length

    # Sort out the initial slug, limiting its length if necessary.
    slug = slugify(value)
    if slug_len:
        slug = slug[:slug_len]
    slug = _slug_strip(slug, slug_separator)
    original_slug = slug

    # Create the queryset if one wasn't explicitly provided and exclude the
    # current instance from the queryset.
    if queryset is None:
        queryset = instance.__class__._default_manager.all()
    if instance.pk:
        queryset = queryset.exclude(pk=instance.pk)

    # Find a unique slug. If one matches, at '-2' to the end and try again
    # (then '-3', etc).
    next = 2
    while not slug or queryset.filter(**{slug_field_name: slug}):
        slug = original_slug
        end = '%s%s' % (slug_separator, next)
        if slug_len and len(slug) + len(end) > slug_len:
            slug = slug[:slug_len-len(end)]
            slug = _slug_strip(slug, slug_separator)
        slug = '%s%s' % (slug, end)
        next += 1

    setattr(instance, slug_field.attname, slug)


def _slug_strip(value, separator='-'):
    """
    Cleans up a slug by removing slug separator characters that occur at the
    beginning or end of a slug.

    If an alternate separator is used, it will also replace any instances of
    the default '-' separator with the new separator.
    """
    separator = separator or ''
    if separator == '-' or not separator:
        re_sep = '-'
    else:
        re_sep = '(?:-|%s)' % re.escape(separator)
    # Remove multiple instances and if an alternate separator is provided,
    # replace the default '-' separator.
    if separator != re_sep:
        value = re.sub('%s+' % re_sep, separator, value)
    # Remove separator from the beginning and end of the slug.
    if separator:
        if separator != '-':
            re_sep = re.escape(separator)
        value = re.sub(r'^%s+|%s+$' % (re_sep, re_sep), '', value)
    return value


reserved_ports = set()


def get_open_port(lowest_port = 0, highest_port = None, bind_address = '', *socket_args, **socket_kwargs):
    if highest_port is None:
        highest_port = lowest_port + 100
    while lowest_port < highest_port:
        if lowest_port not in reserved_ports:
            try:
                with contextlib.closing(socket.socket(*socket_args, **socket_kwargs)) as my_socket:
                    my_socket.bind((bind_address, lowest_port))
                    this_port = my_socket.getsockname()[1]
                    reserved_ports.add(this_port)
                    my_socket.close()
                    return this_port
            except socket.error as error:
                if not error.errno == errno.EADDRINUSE:
                    raise
                assert not lowest_port == 0
                reserved_ports.add(lowest_port)
        lowest_port += 1
    raise Exception('Could not find open port')


def age(when, on=None):
    if on is None:
        on = datetime.date.today()
    on_unix = time.mktime(on.timetuple())
    when_unix = time.mktime(when.timetuple())
    return int((on_unix - when_unix) / 3.15569e7)


def generate_hash(expire_datetime, uri, query_param_file, remote_address, secret_key):
    """Generate hash for concatenation of params and a secret key"""
    ep = datetime.datetime(1970, 1, 1, 0, 0, 0)
    epoch_seconds = int((expire_datetime - ep).total_seconds())

    hash_input = ''.join([str(epoch_seconds), uri, query_param_file, remote_address, ' ', secret_key])

    # openssl md5 -binary | openssl base64 | tr +/ -_ | tr -d =`&expires=$DATE_HASH"
    temp_hash = hashlib.md5(hash_input)
    temp_hash = temp_hash.digest()
    temp_hash = str(base64.b64encode(temp_hash, '-_'))
    temp_hash = temp_hash.replace('=', '')
    return epoch_seconds, temp_hash


def validate_uuid(uuid_string):
    try:
        return uuid.UUID(uuid_string, version=4).hex
    except ValueError:
        return False
    except TypeError:
        return False
