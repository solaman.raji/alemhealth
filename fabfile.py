from fabric.api import *
from fabric.operations import *
from fabric.contrib.project import rsync_project
from fabric.contrib.files import exists

import sys, os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
os.environ['DJANGO_SETTINGS_MODULE'] = 'alemhealth.settings'
abspath = lambda filename: os.path.join(
    os.path.abspath(os.path.dirname(__file__)),
    filename
)
from alemhealth import settings

# --------------------------------------------
# AWS Machines
# --------------------------------------------

# fab -H "host1;host2" dev install_ah deploy

def dev():

    print "Connecting to alemhealth AWS dev server: %s " % env.hosts

    env.setup = True
    env.user = 'ubuntu'
    env.env_type = 'dev'

    env.home = '/home/ubuntu'
    env.serverpath = '%s/alemhealth' % env.home
    env.virtualenvpath = '%s/virtual-env/alemhealth' % env.home
    env.scriptpath = '%s/alemhealth/script/' % env.home

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'
    env.flower_config = 'dev-ops/supervisor/flower.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    # env.mysql_user = 'root'
    # env.mysql_password = '!qweqwe@#'
    # env.mysql_db = "alemhealth"
    # env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        'media',
        'node_modules',
        'alemhealth/local_settings.py',
        'storescp',
        '*.pyc',
        'log',
        'celerybeat-schedule.db',
        'celerybeat.pid',
        'celeryev.pid',
        'static/bower_components',
        'static/client/build',
        '.vagrant',
	    'Vagrantfile',
	    'erl_crash.dump',
    ]

    env.is_grunt = True
    check_ssh_access()

    return


def check_ssh_access():
    run('df -h')


def dev2():

    print "Connecting to alemhealth AWS dev server"

    env.setup = False
    env.user = 'ubuntu'
    env.key_filename = abspath('alemhealth.pem')
    env.env_type = 'dev'
    env.hosts = [
        '52.221.247.45',
        ]
    env.serverpath = '/home/ubuntu/alemhealth'
    env.virtualenvpath = '/home/ubuntu/virtual-env/alemhealth'
    env.scriptpath = '/home/ubuntu/alemhealth/script/'

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'
    env.flower_config = 'dev-ops/supervisor/flower.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    # env.mysql_user = 'root'
    # env.mysql_password = '!qweqwe@#'
    # env.mysql_db = "alemhealth"
    # env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        'media',
        'node_modules',
        'alemhealth/local_settings.py',
        'storescp',
        '*.pyc',
        'log',
        'celerybeat-schedule.db',
        'celerybeat.pid',
        'celeryev.pid',
        'static/bower_components',
        'static/client/build',
        '.vagrant',
	    'Vagrantfile',
        'vagrant/*',
        'erl_crash.dump',
    ]

    env.is_grunt = True

    return


def prod_nginx(host_key):

    print "Connecting to alemhealth AWS nginx Production server"
    print 'Need to install , bower. need to export dr CSV etc ..'
    env.setup = False
    env.user = 'ubuntu'
    # env.env_type = 'prod'
    env.env_type = 'azure'

    hosts = {
        'prod-platform-nginx-01': 'prod-platform-nginx-01.cloud.alemhealth.com.',
        'prod-platform-nginx-02': 'prod-platform-nginx-02.cloud.alemhealth.com',
        'prod-platform-nginx-03': 'prod-platform-nginx-03.cloud.alemhealth.com',
        'azure-platform-nginx-01': 'dev-platform-nginx-01.azr.alem.health',
    }
    env.hosts = [
        hosts[host_key],
        ]
    env.serverpath = '/home/ubuntu/alemhealth'
    env.virtualenvpath = '/home/ubuntu/virtual-env/alemhealth'
    env.scriptpath = '/home/ubuntu/alemhealth/script/'

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.nginx_site_config = 'dev-ops/dev/site.nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'
    env.flower_config = 'dev-ops/supervisor/flower.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    # env.mysql_user = 'root'
    # env.mysql_password = '!qweqwe@#'
    # env.mysql_db = "alemhealth"
    # env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        'media',
        'node_modules',
        'alemhealth/local_settings.py',
        'storescp',
        '*.pyc',
        'log',
        'celerybeat-schedule.db',
        'celerybeat.pid',
        'celeryev.pid',
        'static/bower_components',
        'static/client/build',
        '.vagrant',
	    'Vagrantfile',
        'vagrant/*'
	    'erl_crash.dump',
        'scripts/shell'
    ]


    env.is_grunt = True
    return


def prod_celery(host_key):

    print "Connecting to alemhealth AWS celery Production server"

    env.setup = False
    env.user = 'ubuntu'
    # env.env_type = 'prod'
    env.env_type = 'azure'

    hosts = {
        'prod-platform-celery-01': 'prod-platform-celery-01.cloud.alemhealth.com.',
        'prod-platform-celery-02': 'prod-platform-celery-02.cloud.alemhealth.com',
        'prod-platform-celery-03': 'prod-platform-celery-03.cloud.alemhealth.com',
        'azure-platform-celery-01': 'dev-platform-celery-01.azr.alem.health',
    }
    env.hosts = [
        hosts[host_key],
        ]
    env.serverpath = '/home/ubuntu/alemhealth'
    env.virtualenvpath = '/home/ubuntu/virtual-env/alemhealth'
    env.scriptpath = '/home/ubuntu/alemhealth/script/'

    # local config paths
    env.nginx_config = 'dev-ops/dev/nginx.conf'
    env.nginx_site_config = 'dev-ops/dev/site.nginx.conf'
    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'
    env.flower_config = 'dev-ops/supervisor/flower.conf'

    env.tmp_config = 'dev-ops/dev/celery-tmp.alemhealth.conf'

    env.graceful = False
    env.home = '/home/ubuntu'

    # mysql setting
    # env.mysql_user = 'root'
    # env.mysql_password = '!qweqwe@#'
    # env.mysql_db = "alemhealth"
    # env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        'media',
        'node_modules',
        'alemhealth/local_settings.py',
        'storescp',
        '*.pyc',
        'log',
        'celerybeat-schedule.db',
        'celerybeat.pid',
        'celeryev.pid',
        'static/bower_components',
        'static/client/build',
        '.vagrant',
	    'Vagrantfile',
        'vagrant/*',
	    'erl_crash.dump',
    ]


    env.is_grunt = False
    return

# --------------------------------------------
# Installing Alemhealth Platform
# --------------------------------------------


# install nginx instance
def install_nginx_instance():
    print "Connecting nginx instance " + env.host
    update_apt()
    create_app_directories()
    install_python_dependency()
    install_nodejs()
    install_zpaq()
    install_nginx()
    install_ruby()
    install_supervisor()
    create_uwsgi_supervisor()
    sync_code_base()
    pip_requirements()
    grunt()


# install nginx instance
def install_celery_instance():
    print "Connecting celery instance " + env.host
    update_apt()
    create_app_directories()
    install_python_dependency()
    install_dcmtk()
    install_rabbitmq()
    install_supervisor()
    sync_code_base()
    pip_requirements()
    create_celery_supervisor()


def install_ah():
    if env.setup:
        update_apt()
        create_app_directories()
        install_mysql()
        #create_celery_services()
        install_python_dependency()
        install_nodejs()
        install_nginx()
        install_ruby()
        install_rabbitmq()
        install_dcmtk()
        install_supervisor()
        create_supervisor_services()
        post_install()


def create_app_directories():
    directories = '/var/{log,cache,lib,backups,www}/alemhealth /opt/alemhealth'
    sudo('mkdir -vp %s' % directories)
    sudo('chown -R %s %s' % (env.user, directories))

    nested_directories = [
        '/var/lib/alemhealth/{data,media}',
        '/var/cache/alemhealth/{dicoms,tmp}',
        '/opt/alemhealth/{dcmtk,conf}',
        '/var/log/alemhealth/dcmtk',
        '/var/www/alemhealth/static',
    ]
    for directory in nested_directories:
        run('mkdir -vp %s' % directory)

    # FIXME: Is this needed?
    sudo('chown -R %s:www-data /var/www/alemhealth' % (env.user))
    sudo('usermod -a -G www-data %s' % (env.user))


def install_python_dependency():
    """
    install python dependency packages
    """
    print "Installing python dependency packages"
    sudo(
        'apt-get -y install '
        'python-pip python-setuptools libmysqlclient-dev '
        'python-mysqldb libmysqlclient-dev '
        'python-dev make automake gcc '
        'libxml2 libxml2-dev libxslt-dev '
        'python-dev libtidy-dev python-lxml '
        'htop iftop'
    )

    sudo(
        'apt-get install -y '
        'libjpeg62 zlib1g-dev libjpeg8-dev '
        'libjpeg-dev libfreetype6-dev '
        'libpng-dev libgif-dev'
    )
    sudo('apt-get -y install libcurl4-gnutls-dev librtmp-dev')
    sudo('apt-get install -y libjpeg-turbo8-dev')
    sudo('apt-get install -y libjpeg62-dev')

    # set up virtualenv
    print "Installing python virtualenv"
    #set up virtualenv
    sudo('pip install virtualenv virtualenvwrapper')
    print "Create alemhealth virtualenv"
    run('mkdir -p %s/virtual-env' % env.home)
    run('cd %s/virtual-env/; virtualenv  alemhealth' % env.home)
    #run('cd %s/'% env.home)



def update_apt():
    sudo('apt-get update')

def install_mysql():
    """
    install mysql with
    setup with passwords
    """
    print "Installing mysql"
    sudo('apt-get -y install debconf-utils')
    run('echo "mysql-server-5.6 mysql-server/root_password password !qweqwe@#" > ~/mysql.preseed')
    run('echo "mysql-server-5.6 mysql-server/root_password_again password !qweqwe@#" >> ~/mysql.preseed')
    run('echo "mysql-server-5.6 mysql-server/start_on_boot boolean true" >> ~/mysql.preseed')
    run('cat ~/mysql.preseed | sudo debconf-set-selections')
    sudo("apt-get install mysql-server -y")
    run('mysql -u root -h localhost -p"!qweqwe@#" -e "create database if not exists alemhealth CHARACTER SET utf8 COLLATE utf8_general_ci";')


def install_nodejs():
    #install nodejs with nvm
    # run('curl https://raw.githubusercontent.com/creationix/nvm/v0.10.0/install.sh | sh')
    #run('curl https://raw.githubusercontent.com/creationix/nvm/v0.5.1/install.sh | sh')
    # run ('echo ". ~/.nvm/nvm.sh" >> ~/.bash_profile')
    # run('nvm install 0.10')
    # run('nvm use 0.10')
    # run('nvm alias default 0.10')
    #run('cd %s/; npm install;npm install -g grunt-cli;grunt dev ' % env.serverpath)
    sudo('apt-get install -y python-software-properties')
    run('curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -')
    sudo('apt-get install -y build-essential libssl-dev nodejs')
    sudo('npm install -g grunt-cli')
    sudo('npm install -g bower')
    # run("npm install; npm install -g grunt-cli; grunt %s;" % (tasks))
    # run('cd %s/; npm install;npm install -g grunt-cli;grunt dev ' % env.serverpath)

def install_zpaq():
    print 'Installing zpaq compression library '
    sudo('apt-get -y install zpaq')

def install_nginx():

    print 'Installing uwsgi with PIP'
    sudo('pip install uwsgi')
    print 'Installing Nginx'
    sudo('apt-get -y install nginx')
    env.nginx_site_config = 'dev-ops/dev/site.nginx.conf'
    put('%s/%s' % (settings.BASE_DIR, env.nginx_config),
        '/etc/nginx/nginx.conf.alemhealth',
        use_sudo=True)

    put('%s/%s' % (settings.BASE_DIR, env.nginx_site_config),
        '/etc/nginx/sites-available/alemhealth',
        use_sudo=True)

    # sudo(
    #     'mv /etc/nginx/sites-enabled/site.nginx.conf '
    #     '/etc/nginx/sites-enabled/nginx.conf'
    # )

    sudo('ln -fs %s %s' % ('/etc/nginx/sites-available/alemhealth', '/etc/nginx/sites-enabled/alemhealth'))
    sudo('rm -f %s' % '/etc/nginx/sites-enabled/default')
    sudo("service nginx restart")
    # sudo nano /etc/nginx/nginx.conf  change the root


def install_rabbitmq():
    print 'Installing Rabbitmq'
    sudo('apt-get install -y rabbitmq-server')
    sudo('rabbitmqctl delete_user alemhealth', warn_only=True)
    sudo('rabbitmqctl add_user alemhealth alem2018health')
    sudo('rabbitmqctl set_permissions alemhealth ".*" ".*" ".*"')
    sudo('service rabbitmq-server restart')


def install_dcmtk():
    print 'Installing dcmtk'
    sudo('apt-get install -y dcmtk')


def install_supervisor():
    print 'Installing Supervisor'
    sudo('apt-get install -y supervisor')


def create_uwsgi_supervisor():
    print 'Store supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.uwsgi_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    sudo('supervisorctl reread all')

def create_celery_supervisor():

    print 'Store celery supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.celery_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    print 'Store celerybeat supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.celerybeat_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    print 'Store dcmtk listener supervisor config script'
    put('%s/%s' % (settings.BASE_DIR, env.storescp_config),
        '/etc/supervisor/conf.d/', use_sudo=True)

    print  'Store flower config script'
    put('%s/%s' % (settings.BASE_DIR, env.flower_config),
        '/etc/supervisor/conf.d/', use_sudo=True)


    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    sudo('supervisorctl reread all')

    print 'Copy tmp file config'
    put('%s/%s' % (settings.BASE_DIR, env.tmp_config), '/usr/lib/tmpfiles.d/', use_sudo=True)



def create_supervisor_services():
    tmp_dir = '/tmp/supervisor-conf'
    supervisor_conf_dir = '/etc/supervisor/conf.d/'
    run('mkdir -p %s' % tmp_dir)
    put('%s/%s' % (settings.BASE_DIR, env.uwsgi_config), tmp_dir)
    put('%s/%s' % (settings.BASE_DIR, env.celery_config), tmp_dir)
    put('%s/%s' % (settings.BASE_DIR, env.celerybeat_config), tmp_dir)
    put('%s/%s' % (settings.BASE_DIR, env.storescp_config), tmp_dir)

    sudo('cp -v %s/* %s' % (tmp_dir, supervisor_conf_dir))

    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    sudo('supervisorctl reread all')


def install_ruby():
    print 'Installing Ruby'
    # gem install was failing for similar issue:
    # https://github.com/hishamhm/htop/issues/439
    sudo('apt-get -y install autogen autoconf libtool')
    sudo('apt-get -y install ruby-full')
    sudo('gem install sass')
# --------------------------------------------
# Deploying Alemhealth Platform
# --------------------------------------------


def deploy():
    create_app_directories()
    sync_code_base()
    copy_settings(env.env_type)
    add_aws_config('ap-southeast-1')
    pip_requirements()
    migrate()

    if env.is_grunt:
        grunt()
        collect_static_files()

    restart_services()


def sync_code_base():
    print 'rsync Alemhealth code base'
    # local('rsync -rave "ssh -i %s" %s  %s/* %s@%s:%s' % (
    #     settings.BASE_DIR + '/alemhealth.pem',
    #     ' '.join(env.rsync_exclude),
    #     settings.BASE_DIR, env.user,
    #     env.hosts[0],
    #     env.serverpath
    #     )
    # )

    rsync_project(env.serverpath, abspath('') + '*',
                  exclude=env.rsync_exclude,
                  delete=True,
                  default_opts='-rvz',
                  ssh_opts='-o StrictHostKeyChecking=no')


def pip_requirements():
    run('source %s/bin/activate; cd %s; pip install -r alemhealth/requirements.txt;' % (env.virtualenvpath, env.serverpath))


def grunt(tasks='dev'):
    with cd(env.serverpath):
        run('npm install')
        run('cd static; bower install; cd ..')
        run("grunt %s;" % (tasks))


def collect_static_files():
    print 'Collect static files to web directory'
    with cd(env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        run('python manage.py collectstatic --noinput -i bower_components')


def restart_services():

    # print 'Restart nginx ...'
    # sudo("service nginx restart")

    print 'Restart all supervisor services ...'
    sudo('supervisorctl restart all')

    # print('Restart logstash-forwared')
    # sudo('service logstash-forwarder restart')

def migrate():
    with cd(env.serverpath), prefix('source %s/bin/activate' % env.virtualenvpath):
        print 'Migrate database'
        run('python manage.py showmigrations')
        run('python manage.py migrate')
        run('python manage.py showmigrations')


def post_install():
    sudo('echo [mysqld] > /etc/mysql/conf.d/disable_strict_mode.cnf')
    sudo('echo "sql_mode=IGNORE_SPACE,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" >> /etc/mysql/conf.d/disable_strict_mode.cnf')
    sudo('service mysql restart')


def copy_settings(env_type='dev'):
    assert env_type in ['prod', 'dev', 'azure']
    file_mapping = {
        'prod': 'aws_local_settings.py',
        'dev': 'sample.local_settings.py',
        'azure': 'azure_local_settings.py',
    }
    # src_file = 'aws_local_settings.py' if env_type == 'prod' else 'sample.local_settings.py'
    src_file = file_mapping[env_type]
    config_dest_path = os.path.join(env.serverpath, 'alemhealth', 'local_settings.py')
    config_src_path = os.path.join(settings.BASE_DIR, 'alemhealth', src_file)
    if not exists(config_dest_path, verbose=True):
        print 'Copying %s as config to %s' % (config_src_path, config_dest_path)
        put(config_src_path, config_dest_path)


def add_aws_config(region):
    aws_config_file = '~/.aws/config'
    if not exists(aws_config_file, verbose=True):
        run('mkdir -vp ~/.aws', warn_only=True)
        run('echo [default] > {}'.format(aws_config_file))
        run('echo region = {} >> {}'.format(region, aws_config_file))
