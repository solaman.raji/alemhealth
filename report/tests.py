# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json

from django.test import TestCase

from rest_framework.test import APIRequestFactory

# Using the standard RequestFactory API to create a form POST request
factory = APIRequestFactory()

report_json = '''{"order_guid":"23a6df074be04c88a93c66f57b9229dd", 
"report":"<div style='text-align: center;'><u><b>sssssss</b></u></div>
<div style='text-align: left;'><b>Clinical Information:</b></br>sss</br></div>
<div style='text-align: left;'><b>Comparison:</b></br>jjklj</br></div>
<div style='text-align: left;'><b>Findings:</b></br>saddsdsa</br></div>
<div style='text-align: left;'><b>Impression:</b></br>saddasd</br></div>"}'''
request = factory.put('/api/report/', json.loads(report_json), content_type='application/json')




