# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

from report.models import ReportModel, ReportTemplateModel

# Register your models here.

admin.site.register(ReportModel)
admin.site.register(ReportTemplateModel)
