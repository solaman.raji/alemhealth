# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-20 10:22
from __future__ import unicode_literals

from django.db import migrations, models
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('report', '0002_auto_20170831_0731'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='reportmodel',
            name='clinical_information',
        ),
        migrations.RemoveField(
            model_name='reportmodel',
            name='comparison',
        ),
        migrations.RemoveField(
            model_name='reportmodel',
            name='findings',
        ),
        migrations.RemoveField(
            model_name='reportmodel',
            name='impression',
        ),
        migrations.RemoveField(
            model_name='reportmodel',
            name='procedure',
        ),
        migrations.RemoveField(
            model_name='reportmodel',
            name='report_name',
        ),
        migrations.AddField(
            model_name='reportmodel',
            name='data_hash',
            field=models.CharField(default=None, max_length=32),
        ),
        migrations.AddField(
            model_name='reportmodel',
            name='data_html',
            field=jsonfield.fields.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='reportmodel',
            name='data_text',
            field=jsonfield.fields.JSONField(blank=True, default=None, null=True),
        ),
        migrations.AddField(
            model_name='reportmodel',
            name='order_guid',
            field=models.UUIDField(default=None, editable=False),
        ),
    ]
