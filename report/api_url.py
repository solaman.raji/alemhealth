from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

from report.views import ReportTemplateView, ReporterView, ReportDownloadView

urlpatterns = [
    #report
    url(r'^api/reporttemplate', ReportTemplateView.as_view()),

    # getting report
    url(r'^api/report/(?P<guid>[^/]+)$', ReporterView.as_view()),

    # setting report
    url(r'^api/report$', ReporterView.as_view()),

    # Report Download
    url(r'^api/reports$', ReportDownloadView.as_view()),

    #url(r'^docs/', include('rest_framework_docs.urls'))

]

urlpatterns = format_suffix_patterns(urlpatterns)
