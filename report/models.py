# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import uuid
from django.db import models
from alemhealth.models import Provider, Doctor
from jsonfield import JSONField


class ReportModel(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    order_guid = models.UUIDField(default=None, editable=False)
    data_html = JSONField(default=None, null=True, blank=True)
    data_text = JSONField(default=None, null=True, blank=True)
    data_hash = models.CharField(max_length=32, default=None)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return str(self.guid)

    class Meta:
        db_table = "reports"


class ReportTemplateModel(models.Model):
    guid = models.UUIDField(default=uuid.uuid4, editable=False, unique=True)
    provider = models.ForeignKey(Provider, null=True, blank=True, related_name='report_templates',
                                 on_delete=models.CASCADE)
    doctor = models.ForeignKey(Doctor, null=True, blank=True, related_name='report_templates',
                               on_delete=models.CASCADE)
    template_name = models.CharField(max_length=200, unique=True)
    procedure = models.TextField(null=True, blank=True)
    clinical_information = models.TextField(null=True, blank=True)
    comparison = models.TextField(null=True, blank=True)
    findings = models.TextField(null=True, blank=True)
    impression = models.TextField(null=True, blank=True)

    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __unicode__(self):
        return str(self.template_name)

    class Meta:
        db_table = "report_templates"
