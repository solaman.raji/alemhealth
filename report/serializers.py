from rest_framework import serializers
from report.models import ReportTemplateModel, ReportModel


class OrderReportSerializer(serializers.ModelSerializer):
    guid = serializers.CharField(read_only=True)

    class Meta:
        model = ReportModel
        fields = '__all__'


class ReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportTemplateModel
        fields = '__all__'


class ReportingTemplateSerializer(serializers.ModelSerializer):
    class Meta:
        model = ReportTemplateModel
        fields = '__all__'
