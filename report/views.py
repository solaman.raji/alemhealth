# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import json
import os

import logging
import xhtml2pdf.pisa as pisa
from django.db.models import Q
from django.core.files import File
from django.template import Context
from django.template import TemplateDoesNotExist
from django.template.loader import get_template
from django.utils.text import slugify
# from reportlab.lib import logger
from rest_framework.response import Response
from rest_framework.views import APIView
from django.http import HttpResponseBadRequest, HttpResponseNotFound, HttpResponseServerError
from django.core.exceptions import ObjectDoesNotExist, MultipleObjectsReturned

import alemhealth.settings as settings
from activity.models import create_activity
from order.models import Order
from report.models import ReportTemplateModel, ReportModel
from report.serializers import OrderReportSerializer, ReportingTemplateSerializer
from alemhealth.utils import dict_to_json
import uuid
from alemhealth.models import Provider, Hospital
import hashlib
from bs4 import BeautifulSoup

logger = logging.getLogger('alemhealth')


class ReporterView(APIView):
    def get(self, request, guid):
        data = {}

        try:
            report = ReportModel.objects.get(guid=guid)

            logger.info("get report >>> report: %s", report)
            # serializers = OrderReportSerializer(report)
            # logger.info("get report >>> serializers: %s", serializers)
            # logger.info("get report >>> serializers.data: %s", serializers.data)

            report_data = {
                "guid": report.guid,
                "order_guid": report.order_guid,
                "data_html": report.data_html,
            }

            logger.info("get report >>> report_data: %s", report_data)

            # data["success"] = serializers.data
            data["success"] = report_data
        except ObjectDoesNotExist:
            logger.exception("get report >>> report does not exist with guid %s", guid)
            data["error"] = "report does not exist with guid " + str(guid)
        except Exception as e:
            logger.exception("get report >>> exception occurs. reason: %s", e)
            data["error"] = "Invalid Request"

        return Response(data)

    def post(self, request, guid=None):
        compare_hash = False
        request_data = request.data
        order_guid = request_data["order_guid"]
        logger.info('Report create/update request for order guid: {} with data: {}'.format(order_guid, request_data))

        if guid is None:
            try:
                report = ReportModel.objects.get(order_guid=order_guid)
                logger.warning('No guid for report is provided. Report {} is found for order: {}.'.format(report.guid,
                                                                                                          order_guid))
            except ReportModel.DoesNotExist:
                report = ReportModel()
            except MultipleObjectsReturned:
                logger.exception('Multiple reports found for order: {}'.format(order_guid))
                return HttpResponseServerError('Mutliple reports found for order: {}'.format(order_guid))

            # if ReportModel.objects.filter(order_guid=order_guid).count() != 0:
            #     return HttpResponseBadRequest('A report already exists for order {}'.format(order_guid))
        else:
            compare_hash = True
            report = ReportModel.objects.get(guid=guid)

        data = self.edit_report(report, request_data, compare_hash)

        return Response(data)

    @classmethod
    def edit_report(cls, report, request_data, compare_hash):
        data = {}

        report_data_json = json.dumps(request_data["report_data"])  # convert report_data to json
        report_data_hash = hashlib.md5(report_data_json).hexdigest()  # convert report_data_json to md5 hash

        # Compare hash
        if compare_hash and report.data_hash == report_data_hash:
            order = Order.objects.get(guid=request_data["order_guid"])  # order object

            template = cls.get_template_for_pdf(order)  # get template for pdf
            cls.create_report_with_header(order, template)  # create report with header
            cls.create_report_without_header(order, template)  # create report without header

            data["success"] = "Report Successfully Submitted"
            return data

        # report object
        report.order_guid = request_data["order_guid"]
        report.data_html = request_data["report_data"]
        report.data_text = cls.get_plain_text(request_data["report_data"])
        report.data_hash = report_data_hash
        report.save()

        report_full_html = request_data["report_full_html"]
        order_guid = request_data["order_guid"]

        try:
            order = Order.objects.get(guid=order_guid)  # order object

            cls.update_order(order, report_full_html)  # update report in order table
            template = cls.get_template_for_pdf(order)  # get template for pdf
            cls.create_report_with_header(order, template)  # create report with header
            cls.create_report_without_header(order, template)  # create report without header

            data["success"] = "Report Successfully Submitted"
            cls._add_reporting_activity(order)
        except ObjectDoesNotExist:
            data["error"] = "Order " + str(order_guid) + ": not found (From Report API)"
            raise Exception("Order doesn't exist")
        except Exception as e:
            raise Exception(str(e))

        return data

    @classmethod
    def _add_reporting_activity(cls, order):
        actor = {
            'type': 'study',
            'name': order.accession_id,
            'guid': str(order.guid)
        }

        context = 'Report submitted from cloud'

        obj = {
            'type': 'report',
            'name': str(order.guid),
            'guid': str(order.guid)
        }

        listeners = [order.operator, order.hospital.user.all()[0]]

        create_activity(actor, context, listeners, obj=obj, order=order)

    @classmethod
    def get_report_full_html(cls, report_data):
        report_full_html = ""

        for report_info in report_data:
            if report_info == "procedure":
                report_full_html += "<div style='text-align: center;'><u><b>" + report_data[report_info] + "</b></u></div>";
            else:
                report_full_html += "<div style='text-align: left;'><b>Clinical Information:</b></br>" + report_data[report_info] + "</div>"

        return report_full_html

    @classmethod
    def get_report_header(cls):
        # header = ReportHeader.objects.get(pk=1)
        header = ""
        file_path = os.path.join(settings.MEDIA_ROOT, unicode(header.header_file))

        if not os.path.exists(file_path):
            raise Exception("Header image not found")
        return file_path

    @classmethod
    def get_plain_text(cls, report_data):
        plain_text = {}

        for data in report_data:
            soup = BeautifulSoup(report_data[data], 'html.parser')
            plain_text[data] = soup.get_text()

        return plain_text

    # Update 'report', 'report_file_updated_at', 'status' fields in 'order' table
    @classmethod
    def update_order(cls, order, report_full_html):
        order.report = report_full_html
        order.report_file_updated_at = datetime.datetime.now()
        order.status = 5  # Report ready
        order.save()
        return True

    # Get template for PDF
    @classmethod
    def get_template_for_pdf(cls, order):
        # Get report template folder name
        if settings.AH_ENV == 'dev':
            report_folder_name = 'dev'
        else:
            report_folder_name = 'pro'

        logger.info("report generate >>> report_folder_name: %s", report_folder_name)

        # Default template name
        template_name = "order.html"

        # Get order hospital short code
        # If short code exists then look for template named with that short code
        try:
            hospital_short_code = order.hospital.short_code.lower()

            # Check hospital_short_code
            if hospital_short_code:
                logger.info("report generate >>> hospital_short_code in lowercase: %s", hospital_short_code)

                template_name = hospital_short_code + ".html"
                logger.info("report generate >>> template_name from hospital_short_code: %s", template_name)

                template_path = os.path.join("report", report_folder_name, template_name)
                logger.info("report generate >>> template_path from hospital_short_code: %s", template_path)

                # Check if template_name with hospital_short_code does exist or not
                # If template does not exist then use default template
                try:
                    get_template(template_path)
                    logger.info("report generate pre >>> template_name %s does exist in template_path %", template_name,
                                template_path)
                except TemplateDoesNotExist:
                    logger.exception('report generate pre >>> template_name %s does not exist in template_path %s',
                                     template_name, template_path)
                    template_name = "order.html"
            else:
                logger.info("report generate >>> there is no hospital_short_code")
        except ObjectDoesNotExist:
            logger.exception("report generate >>> object does not exist")

        logger.info("report generate >>> template_name: %s", template_name)
        template_path = os.path.join("report", report_folder_name, template_name)
        logger.info("report generate >>> template_path: %s", template_path)

        # Get template
        try:
            logger.info("trying to get template")
            template = get_template(template_path)
            logger.info("report generate >>> template_name %s does exist in template_path %s", template_name,
                        template_path)
        except TemplateDoesNotExist:
            logger.exception("report generate >>> template_name %s does not exist in template_path %s", template_name,
                             template_path)
            raise Exception("report generate >>> template_name %s does not exist in template_path %s", template_name,
                            template_path)

        return template

    # create report with header
    @classmethod
    def create_report_with_header(cls, order, template):
        # import pdb; pdb.set_trace()
        report_data = {}
        report_data["order"] = order
        report_data["settings"] = settings
        report_data["header_file"] = ""
        report_data["created_date"] = str(datetime.date.today())

        # Get html
        html = template.render(report_data)

        filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
            str(datetime.date.today()))

        filename = filename[:120]
        filename = filename + '.pdf'
        temp_path = '/tmp/alemhealthhub/'

        if not os.path.exists(temp_path):
            os.makedirs(temp_path)

        tmp_filename = '/tmp/alemhealthhub/' + filename
        tmp_file = open(tmp_filename, "w+b")
        pisaStatus = pisa.CreatePDF(html, dest=tmp_file)

        if pisaStatus:
            order.report_filename.save(filename, File(tmp_file))
            if os.path.isfile(tmp_filename):
                os.unlink(tmp_filename)

        tmp_file.close()

    # create report without header
    @classmethod
    def create_report_without_header(cls, order, template):
        # no header_file
        no_header_report = {}
        no_header_report["order"] = order
        no_header_report["settings"] = settings
        no_header_report["created_date"] = str(datetime.date.today())

        no_header_html = template.render(no_header_report)

        no_header_filename = str(order.pk) + '_' + slugify(order.accession_id) + '__' + slugify(
            str(datetime.date.today()))

        no_header_filename = no_header_filename[:120]
        no_header_filename = no_header_filename + '.pdf'

        tmp_filename = '/tmp/alemhealthhub/' + no_header_filename
        tmp_file = open(tmp_filename, "w+b")
        pisaStatus = pisa.CreatePDF(no_header_html, dest=tmp_file)

        if pisaStatus:
            order.normal_report_filename.save(no_header_filename, File(tmp_file))
            if os.path.isfile(tmp_filename):
                os.unlink(tmp_filename)

        tmp_file.close()


class ReportTemplateView(APIView):
    def post(self, request):
        data = {}
        parser = json.loads(request.body)

        try:
            report = ReportTemplateModel()
            report.template_name = parser["template_name"]
            report.provider = Provider.objects.get(guid=parser["provider_guid"])

            if parser["procedure"] is not None:
                report.procedure = parser["procedure"]

            if parser["clinical_information"] is not None:
                report.clinical_information = parser["clinical_information"]

            if parser["comparison"] is not None:
                report.comparison = parser["comparison"]

            if parser["findings"] is not None:
                report.findings = parser["findings"]

            if parser["impression"] is not None:
                report.impression = parser["impression"]

            report.save()
            serializer = ReportingTemplateSerializer(report)
            data["report"] = serializer.data
            data["status"] = 200
        except Exception as e:
            data["error"] = "Cannot create report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def get(self, request):
        data = {}
        report_id = request.GET.get("report_id")

        if report_id:
            try:
                report = ReportTemplateModel.objects.get(pk=report_id)
                serializer = ReportingTemplateSerializer(report)
                data['report'] = serializer.data
                data["status"] = 200
            except ReportTemplateModel.DoesNotExist:
                data["error"] = "Cannot find report with id: " + str(report_id)
                data["status"] = 500
        else:
            try:
                page = request.GET.get('page')
                per_page = request.GET.get('per_page')

                if not per_page:
                    post_per_page = 25
                else:
                    post_per_page = int(per_page)

                if not page:
                    page = 1
                else:
                    page = int(page)

                offset = page * post_per_page - post_per_page
                pagesize = page * post_per_page
                reports = ReportTemplateModel.objects.all()

                if request.user.is_authenticated():
                    reports = self.report_template_filter_by_user_permission(reports, request.user)

                count = reports.count()

                if count > 0:
                    reports = reports[offset:pagesize]
                    serializer = ReportingTemplateSerializer(reports, many=True)
                    data["reports"] = serializer.data
                    data["count"] = count
                    data["status"] = 200
                else:
                    data["error"] = "There is no report here."
                    data["status"] = 400
            except Exception as err:
                data["error"] = "Cannot retrieve data. Reason: " + str(err)
                data["status"] = 500

        return Response(data)

    def put(self, request):
        data = {}
        parser = json.loads(request.body)

        try:
            report = ReportTemplateModel.objects.get(pk=parser["id"])

            if parser["procedure"] is not None:
                report.procedure = parser["procedure"]
            if parser["clinical_information"] is not None:
                report.clinical_information = parser["clinical_information"]
            if parser["comparison"] is not None:
                report.comparison = parser["comparison"]
            if parser["findings"] is not None:
                report.findings = parser["findings"]
            if parser["impression"] is not None:
                report.impression = parser["impression"]

            report.save()
            serializer = ReportingTemplateSerializer(report)
            data["report"] = serializer.data
            data["status"] = 200
        except ReportTemplateModel.DoesNotExist:
            data["error"] = "Cannot find report with id: " + str(parser["id"])
            data["status"] = 400
        except Exception as e:
            data["error"] = "Cannot update report. Reason: " + str(e)
            data["status"] = 500

        return Response(data)

    def delete(self, request):
        data = {}
        report_id = request.GET.get("id")

        try:
            report = ReportTemplateModel.objects.filter(pk=report_id).delete()
            data["success"] = "Successfully deleted the report"
            data["status"] = 200
        except Exception as err:
            data["error"] = "Cannot delete the report. Reason: " + str(err)
            data["status"] = 500
        return Response(data)

    def report_template_filter_by_user_permission(self, reports, user):
        group_name_list = [group.name for group in user.groups.all()]

        if 'doctor' in group_name_list:
            if user.doctor:
                if user.doctor.provider:
                    provider = user.doctor.provider

                    if user.doctor.radiologist:
                        doctor = user.doctor
                        reports = reports.filter(Q(provider=provider) | Q(doctor=doctor))
                    else:
                        reports = reports.filter(provider=provider)
                else:
                    if user.doctor.radiologist:
                        doctor = user.doctor
                        reports = reports.filter(doctor=doctor)
        elif 'provider' in group_name_list:
            if user.providers.all().count():
                provider = user.providers.all()[0]
                reports = reports.filter(provider=provider)

        return reports


class ReportDownloadView(APIView):
    def get(self, request):
        data = {}
        provider_guid = None
        facility_guid = None
        query_dict = request.GET
        logger.info("report download >>> query_dict: " + str(query_dict))

        # Check provider_guid parameter
        if "provider_guid" in query_dict:
            # Check provider_guid parameter validity
            try:
                provider_guid = uuid.UUID(query_dict["provider_guid"])
                logger.info("report download >>> provider_guid: " + str(provider_guid))

                # Check if provider exists with provider_guid
                try:
                    Provider.objects.get(guid=provider_guid)
                except ObjectDoesNotExist:
                    data['error'] = "provider does not exist"
                    logger.exception("report download >>> provider does not exist")
                    return HttpResponseNotFound(json.dumps(data), content_type="application/json")
            except ValueError:
                data['error'] = "provider_guid is not valid"
                logger.exception("report download >>> provider_guid is not valid")
                return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
            except Exception as e:
                data['error'] = "provider_guid is not valid"
                logger.exception("report download >>> exception occurs. Reason: " + str(e))
                return HttpResponseBadRequest(json.dumps(data), content_type="application/json")

        # Check facility_guid parameter
        if "facility_guid" in query_dict:
            # Check facility_guid parameter validity
            try:
                facility_guid = uuid.UUID(query_dict["facility_guid"])
                logger.info("report download >>> facility_guid: " + str(facility_guid))

                # Check if facility exists with facility_guid
                try:
                    Hospital.objects.get(guid=facility_guid)
                except ObjectDoesNotExist:
                    data['error'] = "facility does not exist"
                    logger.exception("report download >>> facility does not exist")
                    return HttpResponseNotFound(json.dumps(data), content_type="application/json")
            except ValueError:
                data['error'] = "facility_guid is not valid"
                logger.exception("report download >>> facility_guid is not valid")
                return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
            except Exception as e:
                data['error'] = "facility_guid is not valid"
                logger.exception("report download >>> exception occurs. Reason: " + str(e))
                return HttpResponseBadRequest(json.dumps(data), content_type="application/json")

        # Check last_download_time parameter
        if "last_download_time" in query_dict:
            last_download_time = query_dict["last_download_time"]

            if last_download_time:
                # Check provider_guid parameter validity
                try:
                    last_download_time = datetime.datetime.strptime(last_download_time, "%Y-%m-%d %H:%M:%S")
                    logger.info("report download >>> last_download_time: " + str(last_download_time))

                    if provider_guid and facility_guid:
                        data['error'] = "report download request is not valid"
                        logger.error("report download >>> report download request is not valid")
                        logger.error("report download >>> both provider_guid and facility_guid is available")
                        return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
                    elif provider_guid:
                        orders = Order.objects.filter(
                            provider__guid=provider_guid,
                            report_file_updated_at__gte=last_download_time
                        ).exclude(report__isnull=True).exclude(report__exact='')
                    elif facility_guid:
                        orders = Order.objects.filter(
                            hospital__guid=facility_guid,
                            report_file_updated_at__gte=last_download_time
                        ).exclude(report__isnull=True).exclude(report__exact='')
                    else:
                        data['error'] = "report download request is not valid"
                        logger.error("report download >>> report download request is not valid")
                        logger.error("report download >>> both provider_guid and facility_guid is not available")
                        return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
                except ValueError:
                    data['error'] = "last_download_time is not valid"
                    logger.exception("report download >>> last_download_time is not valid")
                    return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
                except Exception as e:
                    data['error'] = "last_download_time is not valid"
                    logger.exception("report download >>> exception occurs. Reason: " + str(e))
                    return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
            else:
                if provider_guid and facility_guid:
                    data['error'] = "report download request is not valid"
                    logger.error("report download >>> report download request is not valid")
                    logger.error("report download >>> both provider_guid and facility_guid is available")
                    return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
                elif provider_guid:
                    orders = Order.objects.filter(
                        provider__guid=provider_guid
                    ).exclude(report__isnull=True).exclude(report__exact='')
                elif facility_guid:
                    orders = Order.objects.filter(
                        hospital__guid=facility_guid
                    ).exclude(report__isnull=True).exclude(report__exact='')
                else:
                    data['error'] = "report download request is not valid"
                    logger.error("report download >>> report download request is not valid")
                    logger.error("report download >>> both provider_guid and facility_guid is not available")
                    return HttpResponseBadRequest(json.dumps(data), content_type="application/json")
        else:
            data['error'] = "last_download_time is not present in request"
            logger.exception("report download >>> last_download_time is not present in request")
            return HttpResponseBadRequest(json.dumps(data), content_type="application/json")

        logger.info("report download >>> modified report query_set: " + str(orders))

        data['reports'] = []
        for order in orders:
            report = {'order_guid': str(order.guid),
                      'report': order.report,
                      'radiologist': {'name': order.radiologist.name,
                                      'guid': str(order.radiologist.guid)
                                      } if order.radiologist else ''}
            data['reports'].append(report)

        logger.info("report download >>> json response: " + str(json.dumps(data)))

        return dict_to_json(data, 200)
