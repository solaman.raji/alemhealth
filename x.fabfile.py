from fabric.api import *
from fabric.operations import *
from fabric.contrib import django
from fabric.contrib.project import rsync_project
from fabric.contrib.console import confirm


import sys, os

sys.path.append(os.path.abspath(os.path.dirname(__file__)))
os.environ['DJANGO_SETTINGS_MODULE'] = 'alemhealth.settings'
abspath = lambda filename: os.path.join(os.path.abspath(os.path.dirname(__file__)),
                                        filename)
from alemhealth import settings

# --------------------------------------------
## Machines
# --------------------------------------------


def dev():

    env.setup = True
    env.user = 'root'
    env.hosts = [
        '128.199.98.171',
        ]
    env.serverpath = '/root/alemhealth'
    env.virtualenvpath = '/root/virtual-env/alemhealth'
    env.scriptpath = '/root/alemhealth/script/'
    env.nginx_config = 'dev-ops/dev/nginx.conf'

    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'

    env.graceful = False
    env.home = '/root'

    env.mysql_user = 'root'
    env.mysql_password = '!qweqwe@#'
    env.mysql_db = "alemhealth"
    env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        '--exclude alemhealth/migrations',
        '--exclude order/migrations',
        '--exclude media',
        '--exclude node_modules',
        '--exclude alemhealth/local_settings.py',
        '--exclude storescp',
        '--exclude *.pyc',
        '--exclude log',
        '--exclude celerybeat-schedule.db',
        '--exclude celerybeat.pid',
        '--exclude celeryev.pid',
        '--exclude static/bower_components',
    ]

    print "Connecting to alemhealth dev server"
    return


def prod():

    env.setup = False
    env.user = 'root'
    env.hosts = [
        '128.199.156.212',
        ]
    env.serverpath = '/root/alemhealth'
    env.virtualenvpath = '/root/virtual-env/alemhealth'
    env.scriptpath = '/root/alemhealth/script/'
    env.nginx_config = 'dev-ops/dev/nginx.conf'

    env.uwsgi_config = 'dev-ops/supervisor/uwsgi.conf'
    env.celery_config = 'dev-ops/supervisor/celeryd.conf'
    env.celerybeat_config = 'dev-ops/supervisor/celerybeat.conf'
    env.storescp_config = 'dev-ops/supervisor/dcmtk_listener.conf'


    env.graceful = False
    env.home = '/root'

    env.mysql_user = 'root'
    env.mysql_password = '!qweqwe@#'
    env.mysql_db = "alemhealth"
    env.mysql_host = "alemhealth"

    env.rsync_exclude = [
        '--exclude alemhealth/migrations',
        '--exclude order/migrations',
        '--exclude media',
        '--exclude node_modules',
        '--exclude alemhealth/local_settings.py',
        '--exclude storescp',
        '--exclude *.pyc',
        '--exclude log',
        '--exclude celerybeat-schedule.db',
        '--exclude celerybeat.pid',
        '--exclude celeryev.pid',
        '--exclude static/bower_components',
    ]

    print "Connecting to alemhealth prod server"
    return



# --------------------------------------------
## Installing machines
# --------------------------------------------


def install_machine():

    if env.setup:
        install_mysql()
        install_python_dependency()
        install_nodejs()
        install_nginx()
        #create_celery_services()
        install_rabbitmq()
        install_supervisor()
        create_supervisor_services()




def install_mysql():
    """
    install mysql with
    setup with passwords
    """
    print "Installing mysql"
    sudo('apt-get install debconf-utils')
    run('echo "mysql-server-5.1 mysql-server/root_password password !qweqwe@#" > ~/mysql.preseed')
    run('echo "mysql-server-5.1 mysql-server/root_password_again password !qweqwe@#" >> ~/mysql.preseed')
    run('echo "mysql-server-5.1 mysql-server/start_on_boot boolean true" >> ~/mysql.preseed')
    run('cat ~/mysql.preseed | sudo debconf-set-selections')
    sudo("apt-get install mysql-server -y")
    run('mysql -u root -h localhost -p"!qweqwe@#" -e "create database alemhealth CHARACTER SET utf8 COLLATE utf8_general_ci";')


def install_python_dependency():
    """
    install python dependency packages
    """
    print "Installing python dependency packages"
    sudo('apt-get -y install python-setuptools libmysqlclient-dev python-mysqldb libmysqlclient-dev python-dev make automake gcc libxml2 libxml2-dev libxslt-dev python-dev libtidy-dev python-lxml htop iftop')
    sudo('apt-get install -y libjpeg62 zlib1g-dev libjpeg8-dev libjpeg-dev libfreetype6-dev libpng-dev libgif-dev')
    sudo('apt-get -y install libcurl4-gnutls-dev librtmp-dev')
    run('apt-get install -y libjpeg-turbo8-dev')
    run('apt-get install -y libjpeg62-dev')

    #set up virtualenv
    sudo('apt-get -y install python-virtualenv')
    run('mkdir -p %s/virtual-env' % env.home)
    run('cd %s/virtual-env/; virtualenv  alemhealth' % env.home)
    #run('cd %s/'% env.home)


def install_nodejs():
    #install nodejs with nvm
    run('curl https://raw.githubusercontent.com/creationix/nvm/v0.10.0/install.sh | sh')
    #run('curl https://raw.githubusercontent.com/creationix/nvm/v0.5.1/install.sh | sh')
    run ('echo ". ~/.nvm/nvm.sh" >> ~/.bash_profile')
    run('nvm install 0.10')
    run('nvm use 0.10')
    run('nvm alias default 0.10')
    #run('cd %s/; npm install;npm install -g grunt-cli;grunt dev ' % env.serverpath)

def install_nginx():

    sudo('pip install uwsgi')
    sudo('apt-get -y install nginx')
    #sudo('touch /etc/nginx/sites-enabled/')
    put('%s/%s' % ( settings.BASE_DIR, env.nginx_config ), '/etc/nginx/sites-enabled/' )

    #sudo nano /etc/nginx/nginx.conf  change the root

def install_rabbitmq():
    sudo('apt-get install rabbitmq-server')

def install_supervisor():
    sudo('apt-get install supervisor')




def create_supervisor_services():
    put('%s/%s' % ( settings.BASE_DIR, env.uwsgi_config ), '/etc/supervisor/conf.d/')
    put('%s/%s' % ( settings.BASE_DIR, env.celery_config ), '/etc/supervisor/conf.d/')
    put('%s/%s' % ( settings.BASE_DIR, env.celerybeat_config ), '/etc/supervisor/conf.d/')
    put('%s/%s' % ( settings.BASE_DIR, env.storescp_config ), '/etc/supervisor/conf.d/')

    sudo('supervisorctl stop all')
    sudo('supervisorctl reread all')
    sudo('supervisorctl update all')
    sudo('supervisorctl start all')
    #sudo('supervisorctl reread all')





# --------------------------------------------
## deploying machines
# --------------------------------------------


def deploy():
    sync_file()
    pip_requirements()
    #syncdb()
    grunt()
    restart_services()


def sync_file():
    local('rsync -chavzP %s  %s/* root@%s:/root/alemhealth/' % ( ' '.join(env.rsync_exclude), settings.BASE_DIR, env.hosts[0] ))

def pip_requirements():
    run('source %s/bin/activate; cd %s; pip install -r alemhealth/requirements.txt;' % (env.virtualenvpath, env.serverpath))

def syncdb():
    run('source %s/bin/activate; cd %s; python manage.py syncdb;' % (env.virtualenvpath, env.serverpath))

def grunt(tasks='dev'):
    if env.setup:
        run("cd %s;nvm use 0.10; npm install; npm install -g grunt-cli; grunt %s;" % (env.serverpath, tasks))
    else:
        run("cd %s;nvm use 0.10;grunt %s;" % (env.serverpath, tasks))


def restart_services():

    print 'Restart nginx ...'
    sudo("service nginx restart")

    print 'Restart all supervisor services ...'
    sudo('supervisorctl restart all')

    print('Restart logstash-forwared')
    sudo('service logstash-forwarder restart')


def mysql_execute(sql):
    """ Executes passed sql command using mysql shell. """
    print 'executing sql :' + sql

    sql = sql.replace('"', r'\"')
    run('echo "%s" | mysql --user="%s" --password="%s"' % (sql, env.mysql_user , env.mysql_password))

