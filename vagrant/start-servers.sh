#!/bin/sh

SESSION_NAME="flif-middleware"
echo "Create tmux session for $SESSION_NAME"
pushd /flif-middleware

tmux new -s $SESSION_NAME -d

tmux rename-window -t $SESSION_NAME:0 "htop"
tmux send-keys -t $SESSION_NAME:0 "htop" C-m

tmux new-window -t $SESSION_NAME:1 -n "django-server"
tmux send-keys -t $SESSION_NAME:1 "workon virtualenv-flif-middleware" C-m
tmux send-keys -t $SESSION_NAME:1 "python manage.py runserver 192.168.22.22:8000" C-m

tmux new-window -t $SESSION_NAME:2 -n "middleware.log"
tmux send-keys -t $SESSION_NAME:2 "workon virtualenv-flif-middleware" C-m
tmux send-keys -t $SESSION_NAME:2 "tail -f /var/log/middleware.log" C-m

tmux new-window -t $SESSION_NAME:3 -n "sqlite"
tmux send-keys -t $SESSION_NAME:3 "workon virtualenv-flif-middleware" C-m

tmux new-window -t $SESSION_NAME:4 -n "media-dir"
tmux send-keys -t $SESSION_NAME:4 "workon virtualenv-flif-middleware" C-m
tmux send-keys -t $SESSION_NAME:4 "cd media" C-m

popd

SESSION_NAME="platform"
echo "Create tmux session for $SESSION_NAME"
pushd /alemhealth

tmux new -s $SESSION_NAME -d

tmux rename-window -t $SESSION_NAME:0 "django shell"
tmux send-keys -t $SESSION_NAME:0 "python manage.py shell_plus" C-m

tmux new-window -t $SESSION_NAME:1 -n "django-server"
tmux send-keys -t $SESSION_NAME:1 "python manage.py runserver 192.168.33.33:8000" C-m

tmux new-window -t $SESSION_NAME:2 -n "alemhealth.log"
tmux send-keys -t $SESSION_NAME:2 "tail -f /var/log/alemhealth/platform.log" C-m

tmux new-window -t $SESSION_NAME:3 -n "celery"
tmux split-window -h
tmux select-pane -t 0
tmux send-keys  "python manage.py celeryd" C-m
tmux select-pane -t 1
tmux send-keys "python manage.py celerybeat" C-m

tmux new-window -t $SESSION_NAME:7 -n "mysql"

tmux new-window -t $SESSION_NAME:8 -n "media-dir"
tmux send-keys -t $SESSION_NAME:8 "cd media" C-m

#tmux attach -t $SESSION_NAME

popd

tmux list-sessions
