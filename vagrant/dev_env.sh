export WORKON_HOME=$HOME/.virtualenvs
source /usr/local/bin/virtualenvwrapper.sh

export PROJECT=alemhealth
export PROJECT_HOME=/$PROJECT
cd $PROJECT_HOME

ENV_NAME=virtualenv-$PROJECT
mkvirtualenv $ENV_NAME
workon $ENV_NAME

pip install -r $PROJECT_HOME/$PROJECT/requirements.txt
#python setup.py develop
cp $PROJECT_HOME/$PROJECT/sample.local_settings.py $PROJECT_HOME/$PROJECT/local_settings.py

# Run migration to create tables
python manage.py migrate

export BASHRC_PATH=~/.bashrc
if ! grep -Fq "WORKON_HOME" $BASHRC_PATH; then
    echo 'export WORKON_HOME=$HOME/.virtualenvs' >> $BASHRC_PATH
    echo 'source /usr/local/bin/virtualenvwrapper.sh' >> $BASHRC_PATH
    echo ". $WORKON_HOME/$ENV_NAME/bin/activate" >> $BASHRC_PATH
    echo "export LANGUAGE=en_US.UTF-8" >> $BASHRC_PATH
    echo "export LC_ALL=en_US.UTF-8" >> $BASHRC_PATH
    echo "export RABBITMQ_HOME=/usr/lib/rabbitmq/bin" >> $BASHRC_PATH
fi

# Frontend packages
echo "Install frontend packages"

cd $PROJECT_HOME
echo "Run npm install"
npm install

echo "Run bower install"
cd $PROJECT_HOME/static
bower install
cd $PROJECT_HOME

echo "Install grunt dev"
grunt dev

# Initial Data Populate for Superuser and Groups
python manage.py shell < data_populate.py

# Middleware Setup
export PROJECT=flif-middleware
export PROJECT_HOME=/$PROJECT
export APP_NAME=middleware
cd $PROJECT_HOME

ENV_NAME=virtualenv-$PROJECT
mkvirtualenv $ENV_NAME
workon $ENV_NAME

pip install -r $PROJECT_HOME/requirements.txt
cp $PROJECT_HOME/$APP_NAME/sample_local_settings.py $PROJECT_HOME/$APP_NAME/local_settings.py
python manage.py migrate