#!/bin/bash
echo "Update aptitude packages"
apt-get update
echo "Install python and other packages"
apt-get install -y build-essential libssl-dev python2.7-dev python-pip ruby-full git nginx rabbitmq-server tmux curl dcmtk zpaq htop iftop sqlite3 swapspace

# MySQL installation
DBHOST=localhost
DBNAME=alemhealth
DBUSER=admin
DBPASSWD=!qweqwe@#

echo -e "\n--- Install MySQL specific packages and settings ---\n"
debconf-set-selections <<< "mysql-server mysql-server/root_password password $DBPASSWD"
debconf-set-selections <<< "mysql-server mysql-server/root_password_again password $DBPASSWD"

apt-get update
apt-get install -y mysql-server-5.7 libmysqlclient-dev libmysqlclient20

# Change MySQL bind-address
sudo sed -i '/bind-address/s/127.0.0.1/0.0.0.0/' /etc/mysql/mysql.conf.d/mysqld.cnf

# Restart MySQL for bind-address change
sudo service mysql restart

echo -e "\n--- Setting up our MySQL user and db ---\n"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE $DBNAME"
mysql -uroot -p$DBPASSWD -e "grant all privileges on $DBNAME.* to '$DBUSER'@'%' identified by '$DBPASSWD'"
mysql -uroot -p$DBPASSWD -e "CREATE DATABASE test_$DBNAME"
mysql -uroot -p$DBPASSWD -e "grant all privileges on test_$DBNAME.* to '$DBUSER'@'%' identified by '$DBPASSWD'"
mysql -uroot -p$DBPASSWD -e "SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''));"

#Create directories
mkdir -p /var/log/alemhealth
chown -R ubuntu /var/log/alemhealth
touch /var/log/middleware.log
chown -R ubuntu /var/log/middleware.log

# Python Setup
echo "Install python virtual environment"
pip install virtualenv virtualenvwrapper
pip install -U pip

# Node Installation
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
apt-get install -y nodejs
npm install -g grunt-cli
npm install -g bower
npm install -g grunt

gem install sass

#hostnamectl  set-hostname cloud-local.alemhealth.com

echo [mysqld] > /etc/mysql/conf.d/disable_strict_mode.cnf
echo "sql_mode=IGNORE_SPACE,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION" >> /etc/mysql/conf.d/disable_strict_mode.cnf
service mysql restart