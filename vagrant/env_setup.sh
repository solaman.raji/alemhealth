# RabbitMQ Plugins
echo "Enable RabbitMQ Plugins"
service rabbitmq-server stop
/usr/lib/rabbitmq/bin/rabbitmq-plugins enable rabbitmq_management
/usr/lib/rabbitmq/bin/rabbitmq-plugins enable rabbitmq_jsonrpc
service rabbitmq-server start

/usr/lib/rabbitmq/bin/rabbitmq-plugins list
