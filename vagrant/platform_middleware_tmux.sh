#!/bin/sh

SESSION_NAME="v_pf_mw"

tmux new -s $SESSION_NAME -d

tmux rename-window -t $SESSION_NAME:0 "htop"
tmux send-keys -t $SESSION_NAME:0 "htop" C-m

cd /flif-middleware

tmux new-window -t $SESSION_NAME:1 -n "mw-django-server"
tmux send-keys -t $SESSION_NAME:1 "workon virtualenv-flif-middleware" C-m
tmux send-keys -t $SESSION_NAME:1 "python manage.py runserver 192.168.22.22:8000" C-m

tmux new-window -t $SESSION_NAME:2 -n "middleware.log"
tmux send-keys -t $SESSION_NAME:2 "workon virtualenv-flif-middleware" C-m
tmux send-keys -t $SESSION_NAME:2 "tail -f /var/log/alemhealth/middleware.log" C-m

tmux new-window -t $SESSION_NAME:3 -n "mw-sqlite"
tmux send-keys -t $SESSION_NAME:3 "workon virtualenv-flif-middleware" C-m

tmux new-window -t $SESSION_NAME:4 -n "mw-media"
tmux send-keys -t $SESSION_NAME:4 "workon virtualenv-flif-middleware" C-m
tmux send-keys -t $SESSION_NAME:4 "cd media" C-m

cd /alemhealth

tmux new-window -t $SESSION_NAME:5 -n "pf-django-server"
tmux send-keys -t $SESSION_NAME:5 "python manage.py runserver 192.168.33.33:8000" C-m

tmux new-window -t $SESSION_NAME:6 -n "pf-celeryd"
tmux send-keys -t $SESSION_NAME:6 "python manage.py celeryd" C-m

tmux new-window -t $SESSION_NAME:7 -n "alemhealth.log"
tmux send-keys -t $SESSION_NAME:7 "tail -f /var/log/alemhealth/platform.log" C-m

tmux new-window -t $SESSION_NAME:8 -n "pf-mysql"

tmux new-window -t $SESSION_NAME:9 -n "pf-media"
tmux send-keys -t $SESSION_NAME:9 "cd media" C-m

tmux new-window -t $SESSION_NAME:10 -n "bash"

tmux attach -t $SESSION_NAME
